﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Helper.Client
{
    public  class MediaType
    {
        string _type = string.Empty;
        private MediaType(string type)
        {
            _type = type;
        }
        public string GetMediaType()
        {
            return _type;
        }
        public static MediaType Json = new MediaType("application/json");
    }
}
