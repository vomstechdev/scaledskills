﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace SCLD.Core.Helper.Client
{
   public class ClientContent
    {
         StringContent stringContent;

        private string baseUrl;
        internal StringContent GetContent()
        {
            return stringContent;
        }
        internal Uri GetUri()
        {
            return new Uri(baseUrl);
        }
        public void SetBaseUrl(string url)
        {
            baseUrl = url;
        }
        public void SetContent(string content)
        {
            stringContent = new StringContent(content);
        }
        public void SetContent(string content, Encoding encoding)
        {
            stringContent = new StringContent(content,encoding);
        }
        public void SetContent(string content, Encoding encoding, MediaType mediaType)
        {
            stringContent = new StringContent(content, encoding,mediaType.GetMediaType());
        }

    }
}
