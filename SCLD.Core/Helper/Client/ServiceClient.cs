﻿using SCLD.Core.Helper.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Helper
{
    public class ServiceClient:IDisposable
    {
        HttpClient client;
        HttpResponseMessage responce;
        public ServiceClient()
        {
            client = new HttpClient();
        }
        public ServiceClient(string baseAddress):this(new Uri(baseAddress))
        {
            
        }
        public ServiceClient(Uri uri)
        {
            client.BaseAddress = uri;

        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task PostExecuteAsync(Action<ClientContent> content)
        {
            var contentStr = new ClientContent();
            content?.Invoke(contentStr);
            client.BaseAddress = client.BaseAddress ?? contentStr.GetUri();
            responce = await client.PostAsync(client.BaseAddress, contentStr.GetContent());
        }
        public  Task<string> ReadAsStringAsync()
        {
            return responce?.Content.ReadAsStringAsync();
        }
        public Task<System.IO.Stream> ReadAsStreamAsync()
        {
            return responce?.Content.ReadAsStreamAsync();
        }
        public async Task<T> ReadAsAsync<T>()
        {
            StreamReader reader = new StreamReader(await responce.Content.ReadAsStreamAsync());
            string responseStringData = reader.ReadToEnd();
            T responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseStringData);
            return responseData;
        }
        public Task<byte[]> ReadAsByteArrayAsync()
        {
            return responce?.Content.ReadAsByteArrayAsync();
        }
        public void GetExecute()
        {

        }
        public void Run()
        {
            if (client == null)
            {
                throw new Exception("Client can not be null.");
            }
            if (client.BaseAddress?.ToString() == null)
            {
                throw new Exception("Address is not Valid.");
            }
        }
    }
}
