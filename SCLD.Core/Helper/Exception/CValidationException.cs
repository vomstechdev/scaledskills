﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SCLD.Core.Helper
{
    public class CValidationException : Exception
    {
        public CValidationException()
        {
            ValidationError = new Dictionary<string, string[]>();
        }
        public CValidationException(Dictionary<string, string[]> valuePairs)
        {
            ValidationError = valuePairs;
        }
        public CValidationException(string messege) : base(messege)
        {

        }
        public readonly IDictionary<string, string[]> ValidationError;

    }
    public class DataValidationException : CValidationException
    {
        public DataValidationException(Action<IDictionary<string, string[]>> action)
        {
            action?.Invoke(this.ValidationError);
        }
        public DataValidationException(Dictionary<string, string[]> valuePairs) : base(valuePairs)
        {
        }
        public override string Message => this.ValidationError.Count>0 ? string.Join(Environment.NewLine, this.ValidationError.Values.FirstOrDefault()) : base.Message;
    }
    public class PaymentRedirect : Exception
    {
        public PaymentRedirect(string messege) : base(messege)
        {

        }
    }

}
