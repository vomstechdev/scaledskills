﻿using Microsoft.AspNetCore.Http;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Helper.Document
{
    public class ImageWriter : IImageWriter
    {
        readonly IFileRepository fileRepository;
        public ImageWriter(IFileRepository _fileRepository)
        {
            fileRepository = _fileRepository;
        }
        public async Task<string> UploadImage(IFormFile file)
        {
            var format = CheckIfImageFile(file);
            if (format != WriterHelper.ImageFormat.unknown)
            {
                return await WriteFile(file, format);
            }

            return "Invalid image file";
        }


        /// <summary>
        /// Method to check if file is image file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private WriterHelper.ImageFormat CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return WriterHelper.GetImageFormat(fileBytes);
        }

        /// <summary>
        /// Method to write file onto the disk
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<string> WriteFile(IFormFile file, WriterHelper.ImageFormat imageFormat)
        {
            string fileName;
            DocumentModel documentModel = new DocumentModel();
            try
            {
                documentModel.FileName = documentModel.Caption = file.FileName;
                documentModel.DocumentId = documentModel.Group = Guid.NewGuid();
                documentModel.Filetype = documentModel.Type = imageFormat.ToString();
                documentModel.Ext = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = documentModel.DocumentId + documentModel.Ext; //Create a new Name 
                                                                         //for the file due to security reasons.
                documentModel.Directory = Path.Combine(Directory.GetCurrentDirectory(), "Documents\\images", fileName);

                using (var bits = new FileStream(documentModel.Directory, FileMode.Create))
                {
                    await file.CopyToAsync(bits);
                    documentModel = SaveIntoDatabase(documentModel);
                    return documentModel.DocumentId.ToString();
                }

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        private DocumentModel SaveIntoDatabase(DocumentModel doc)
        {
            //doc.DocumentId = Guid.NewGuid();
            doc.Group = doc.Group == Guid.Empty ? Guid.NewGuid() : doc.Group;
            doc.Order = 1;
            fileRepository.Add(doc);
            return doc;
        }
        public Task<Stream> GetStream(Guid id, out string fileName)
        {
            var model = fileRepository.GetFile(id);
            var dataBytes = System.IO.File.ReadAllBytes(model.Directory);
            fileName = model.FileName;
            var dataStream = new MemoryStream(dataBytes);
            return Task.FromResult<Stream>(dataStream);
        }
        public void DeleteImage(Guid id)
        {
            fileRepository.Delete(id);
        }
    }
}
