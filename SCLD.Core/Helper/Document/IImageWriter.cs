﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Helper.Document
{
    public interface IImageWriter
    {
        Task<string> UploadImage(IFormFile file);
        Task<System.IO.Stream>  GetStream(Guid id, out string fileName);
        void DeleteImage(Guid id);
    }
}
