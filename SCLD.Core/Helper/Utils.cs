﻿using SCLD.Core.Config;
using SCLD.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SCLD.Core.Helper
{
    public static class Util
    {
        public static string GetBase64StringForImage(string imgPath)
        {
            byte[] imageBytes = System.IO.File.ReadAllBytes(imgPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
        public static string PostApi(string imgPath)
        {
            byte[] imageBytes = System.IO.File.ReadAllBytes(imgPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
        public static string GetReferralId(string value)
        {
            value = value.ToUpper();
            if (value.Length > 4)
            {
                return (string.Concat(value.Substring(0, 4), (int)value[0], (int)value[1]));
            }
            return "12345678";
        }
        public static string GetDocPreview(string id, string siteUrl)
        {
            return string.IsNullOrWhiteSpace(id) ? id : GetDocPreview(Guid.Parse(id), siteUrl);
        }
        public static string GetDoc(string id)
        {
            var config = ConfigurationSection.Current.AppKeys;
            return GetDocPreview(id, config.ApiUrl);
        }
        public static string GetDocPreview(Guid? id,string siteUrl)
        {
            return  id.HasValue && id != Guid.Empty  ? siteUrl+ "api/Document/p/" + id.ToString() : null ;
        }
        public static string GetTrainingPreview(string id)
        {
            var siteUrl = ConfigurationSection.Current.AppKeys.WebUrl;
            if (!string.IsNullOrEmpty(id))
            {
                return "t/" + id.ToString();
            }
            return "/";
        }
        public static string GetTrainerPreview(string id)
        {
            var siteUrl = ConfigurationSection.Current.AppKeys.WebUrl;
            return  siteUrl + "p/" + id.ToString() ;
        }
        public static string GetOrganizationPreview(string id)
        {
            var siteUrl = ConfigurationSection.Current.AppKeys.WebUrl;
            return  siteUrl + "o/" + id.ToString() ;
        }
        public static string GetLoginUrl()
        {
            var siteUrl = ConfigurationSection.Current.AppKeys.WebUrl;
            return siteUrl + "auth/login/";
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static DateTime GetStanderTime()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime;
        }
        public static string GetUrlWithUrl(string url)
        {
          return  ConfigurationSection.Current.AppKeys.WebUrl + url;
        }
        //public static IEnumerable<SelectItem> GetEnumList<T>()
        //{
        //    Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectItem
        //    {
        //        Text = v.ToString(),
        //        Value = ((int)v).ToString()
        //    }).ToList();

        //    return data;
        //}
        public static string UserEmailConfirmUrl => ConfigurationSection.Current.AppKeys.WebUrl + "auth/userValidate/";
        public static string UserResetPasswordUrl => ConfigurationSection.Current.AppKeys.WebUrl + "auth/resetPassword/";
        public static string UserUnsubscibeUrl => ConfigurationSection.Current.AppKeys.WebUrl + "user/unsubscribe";
    }
}
