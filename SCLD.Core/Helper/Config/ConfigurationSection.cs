﻿using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Config
{
    public class ConfigurationSection
    {
        //private const string Geoogle = "GoogleConfigSection";
        private readonly IConfigManager configManager;
        public ConfigurationSection(IConfigManager _configManager)
        {
            configManager = _configManager;
            Current = this;
        }
        public GoogleConfig GoogleConfig { get => configManager.GetSection<GoogleConfig>(nameof(SCLD.Core.Config.GoogleConfig)); }
        public AppKeys AppKeys { get => configManager.GetSection<AppKeys>(nameof(AppKeys)); }
        public static ConfigurationSection Current { get; private set; }
    }
    public class GoogleConfig
    {
        public string Key { get; set; }
        public string VisionApiUrl { get; set; }
    }
    public class SMTPConfig
    {
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string Domain { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public bool IsHtml { get; set; }
        public bool UseDefaultCredentials { get; set; }
    }
}
