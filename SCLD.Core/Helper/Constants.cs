﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Helper
{
    public static class Constants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Rol = "rol", Id = "id",UserId = "UserId",Email = "Email";
            }
            public static class AuthType
            {
                public const string Token = "Token", Cookies = "id";
            }
            public static class JwtClaims
            {
                public const string ApiAccess = "api_access";
            }
        }
    }
}
