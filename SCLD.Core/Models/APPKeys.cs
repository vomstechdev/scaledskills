﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models
{
	public partial class AppKeys
	{
		public string WebUrl { get; set; }
        public string ApiUrl { get; set; }
    }
	public partial class AppKeys
	{
		public const string defaultSelectValue = "";
	}
    public partial class PaymentKeys
    {
        public string URL { get; set; }
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        public string ReturnUrl { get; set; }
        public string NotifyUrl { get; set; }
    }
}
