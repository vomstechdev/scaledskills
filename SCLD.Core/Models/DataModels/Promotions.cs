﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Promotion:BaseEntity
    {
        public const int EntityType_Image = 1;
        public const int EntityType_Training = 2;
        public long EntityId { get; set; }
        public int EntityType { get; set; }
        public string Description { get; set; }
        public int Order { get; set; } = 1;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
