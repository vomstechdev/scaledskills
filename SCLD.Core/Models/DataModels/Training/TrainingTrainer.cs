﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingTrainer:BaseEntity
    {
        public long TrainingId { get; set; }
        public long TrainerId { get; set; }
        public bool IsDelete { get; set; }
    }
}
