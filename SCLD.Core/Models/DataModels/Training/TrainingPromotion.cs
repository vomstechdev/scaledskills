﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingPromotion:BaseEntity
    {
        public long TrainingId { get; set; }
        public bool IsAdvertisement { get; set; }
        public decimal AdvertisementRate { get; set; }
        public bool IsDisplayBannerHomePage { get; set; }
        public decimal DisplayBannerHomePageRate { get; set; }
        public bool IsDisplayInCardHomePage { get; set; }
        public decimal DisplayCardHomePageRate { get; set; }
        public bool IsAffliliate { get; set; }
        public int AffliliateType { get; set; }
        public decimal AffliliateRate { get; set; }
    }
}
