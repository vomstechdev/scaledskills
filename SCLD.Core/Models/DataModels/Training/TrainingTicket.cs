﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingTicket:BaseEntity
    {
        public string Name { get; set; }
        public long TrainingId { get; set; }
        public int QTY { get; set; }
        public int AvailableQty { get; set; }
        public int MinBooking { get; set; }
        public int? MaxBooking { get; set; }
        public int TicketType { get; set; }
        public decimal PaymentCharge { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public string MSGForAtendee { get; set; }
    }
}
