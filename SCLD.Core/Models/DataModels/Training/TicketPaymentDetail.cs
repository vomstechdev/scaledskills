﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TicketPaymentDetail : BaseEntity
    {
        public long TicketId { get; set; }
        public Payment_FeeType FeeType { get; set; }
        public decimal Amount { get; set; }
        public bool IsPercentage { get; set; }
        public Payment_ModeType ModeType { get; set; }
    }
}
