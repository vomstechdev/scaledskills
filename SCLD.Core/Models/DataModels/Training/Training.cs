﻿using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Training : UserBaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public long HostedBy { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TimeZone { get; set; }
        public Training_Status TrainingStatus { get; set; } = Training_Status.UnPublish;
        public TrainingModeType ModeType { get; set; } = TrainingModeType.Offline;
        public long? AddressId { get; set; }
        public long? AboutId { get; set; }
        public string OnlineLocation { get; set; }
        public string LocationDetail { get; set; }
        public bool IsOnlineDetailsVisible { get; set; }
        public bool IsRecurrence { get; set; } = false;
        public bool IsUserAccept { get; set; } = false;
        public bool IsPromotion { get; set; } = false;

    }
}
