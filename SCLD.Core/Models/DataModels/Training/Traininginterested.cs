﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingInterested : UserBaseEntity
    {
        public long TrainingId { get; set; }
        public bool IsInterested { get; set; } = true;
    }
}
