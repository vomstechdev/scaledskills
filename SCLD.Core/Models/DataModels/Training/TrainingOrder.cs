﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingOrder : UserBaseEntity
    {
        public long TrainingId { get; set; }
        public long OrderId { get; set; }
    }
}
