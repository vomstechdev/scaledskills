﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingReview: UserBaseEntity
    {
        public long TrainingId { get; set; }
        public int RateOfSatisfied { get; set; }
        public string LikeAbout { get; set; }
        public string ImproveAbout { get; set; }
        public string NeedInFeature { get; set; }
    }
}
