﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingSetting : BaseEntity
    {
        public long TrainingId { get; set; }
        public bool IsPublicRepeat { get; set; } = true;
        public bool IsPublicCustomQuestion { get; set; } = true;
        public bool IsPublicRemainingTicket { get; set; } = true;
        public bool IsPublicSetReminder { get; set; } = true;
        public bool IsPublicAffiliatePromoterLink { get; set; } = true;
        public bool IsPublicTrainingId { get; set; } = true;
        public bool IsPublicPageViews { get; set; } = true;
        public bool IsPublicInterestedUser { get; set; } = true;
        public bool IsPublicRegisteredUser { get; set; } = true;
        public bool IsPublicAddToCalendar { get; set; } = true;
        public bool IsPublicXSpotsLeft { get; set; } = true;
        public bool IsPublicComments { get; set; } = true;
        public bool IsPublicFeedback { get; set; } = true;
        public bool IsPublicAboutOrganizer { get; set; } = true;
        public bool IsPublicMoreEventsFromSameOrganizer { get; set; } = true;
    }
}
