﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingImage : BaseEntity
    {
        public long TrainingId { get; set; }
        public string Header { get; set; }
        public string Card { get; set; }
    }
}
