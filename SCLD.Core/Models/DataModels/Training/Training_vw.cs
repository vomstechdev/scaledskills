﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Result_Trainings_view 
    {
        public long TrainingId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public Training_Status TrainingStatus { get; set; }
        public string OnlineLocation { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Mode { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int? InterestCount { get; set; }
        public decimal? Amount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public long? HostedBy { get; set; }
        public bool IsRecurrence { get; set; }
        public string Header { get; set; }
        public string Card { get; set; }
        public bool IsOnlineDetailsVisible { get; set; }
        public string Keywords { get; set; }
        [NotMapped]
        public bool CanEdit { get; set; } = false;
        [NotMapped]
        public bool IsFollow { get; set; } = false;
        [NotMapped]
        public bool RegisteredMember { get; set; } = false;
        public string ImageUrl { get => Core.Helper.Util.GetDoc(Card); }
        public string HeaderUrl { get => Core.Helper.Util.GetDoc(Header); }

        [NotMapped]
        public bool IsInterest { get; set; }
        [NotMapped]
        public int AvilableQtyTotal  { get; set; }
        [NotMapped]
        public int QtyTotal { get; set; }
        [NotMapped]
        public bool IsRegister { get; set; }
        [NotMapped]
        public bool IsDisplayFollow { get; set; }
        [NotMapped]
        public string UserUrl { get; set; }
        [NotMapped]
        [JsonIgnore]
        public long TrainnerId { get; set; }
        [NotMapped]
        public string UserEmail { get; set; }
        [NotMapped]
        public string UserPhoneNumber { get; set; }

    }
    public class Result_CardTraining_view
    {
        public long TrainingId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string OnlineLocation { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Mode { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int? InterestCount { get; set; }
        public decimal? Amount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string Image { get; set; }
        public Training_Status TrainingStatus { get; set; }
        public string ImageUrl { get => Core.Helper.Util.GetDoc(Image); }
        public string FullUrl { get => Core.Helper.Util.GetTrainingPreview(Url); }
        [NotMapped]
        public bool IsInterest { get; set; }

    }
}
