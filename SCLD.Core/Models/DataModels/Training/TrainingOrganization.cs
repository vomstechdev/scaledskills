﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class TrainingOrganization:BaseEntity
    {
        public long TrainingId { get; set; }
        public long OrganizationId { get; set; }
        public bool IsDelete { get; set; }
    }
}
