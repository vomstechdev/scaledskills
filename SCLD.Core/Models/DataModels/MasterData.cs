﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
	public class MasterData: BaseEntity
	{
		public string Key { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public long? ParentId { get; set; }
	}
}
