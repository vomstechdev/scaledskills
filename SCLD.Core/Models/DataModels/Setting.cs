﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Setting:BaseEntity
    {
        public string Key { get; set; }
        public string Values { get; set; }
        public int TypeOfValue { get; set; }
        public int Parent { get; set; }
    }
}
