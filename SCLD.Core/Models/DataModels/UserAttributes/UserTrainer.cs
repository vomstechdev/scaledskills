﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
	public partial class Trainer : UserBaseEntity
	{
        public string ProfileUrl { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string GST { get; set; }
		public string PanNumber { get; set; }
		public long? AddressId { get; set; }
		public long? AboutId { get; set; }
		public long? BankDetailId { get; set; }
		public long? SocialId { get; set; }
	}
}
