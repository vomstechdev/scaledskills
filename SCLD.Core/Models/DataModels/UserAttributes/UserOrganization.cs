﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
	public partial class Organization: UserBaseEntity
	{
		public string Name { get; set; }
		public string OwnerName { get; set; }
		public string Image { get; set; }
		public string ProfileUrl { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string GST { get; set; }
		public string PanNumber { get; set; }
		public long? AddressId { get; set; }
		public long? AboutId { get; set; }
		public long? BankDetailId { get; set; }
		public long? SocialId { get; set; }
	}
}
