﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class UserLanguage:UserBaseEntity
    {
        public int LanguageId { get; set; }
        public int Proficiency { get; set; }
    }
}
