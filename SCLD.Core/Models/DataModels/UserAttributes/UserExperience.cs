﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
   public class UserExperience : UserBaseEntity
    {
        public string Position { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
