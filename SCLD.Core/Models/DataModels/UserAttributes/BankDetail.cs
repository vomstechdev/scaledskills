﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
	public class BankDetail:UserBaseEntity
	{
		public string Name { get; set; }
		public string AccountNumber { get; set; }
		public int AccountType { get; set; }
		public string BankName { get; set; }
		public string BranchName { get; set; }
        public string IFSCCode { get; set; }
		public string GSTNum { get; set; }
		public string UPI { get; set; }
		public Guid? ExemptionDocId { get; set; }
        public Guid? PanCardDocId { get; set; }
        public Guid? AdharCardDocId { get; set; }
        public Guid? CancellationDocId { get; set; }
    }
}
