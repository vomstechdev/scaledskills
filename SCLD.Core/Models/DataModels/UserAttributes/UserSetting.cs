﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public partial class UserSetting: UserBaseEntity
    {
        public bool IsInterAffiliatePartner { get; set; }
        public string ReferralID { get; set; }
        public string ProfileUrl { get; set; }
        public bool IsPublishUrl { get; set; }
        public bool IsPublicCertifications { get; set; }
        public bool IsPublicSocialMedia { get; set; }
        public bool IsDefaultLanguage { get; set; }
        public string AboutText { get; set; }
    }
}
