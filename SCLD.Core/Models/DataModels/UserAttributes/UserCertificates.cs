﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class UserCertificate:UserBaseEntity
    {
        public string Name { get; set; }
        public string Authority { get; set; }
        public DateTime From  { get; set; }
        public DateTime? To { get; set; }
        public bool IsExpired { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
