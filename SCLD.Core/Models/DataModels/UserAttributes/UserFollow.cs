﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class UserFollow:UserBaseEntity
    {
        public UserFollowType Type { get; set; }
        public long TypeId { get; set; }
    }
}
