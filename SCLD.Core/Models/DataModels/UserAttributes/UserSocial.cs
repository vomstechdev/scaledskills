﻿namespace SCLD.Core.Models.DataModels
{
    public class UserSocial : UserBaseEntity
    {
        public string LinkedInUrls { get; set; }
        public string FacebookUrls { get; set; }
        public string TwitterUrls { get; set; }
        public string WebsiteUrls { get; set; }
    }
}
