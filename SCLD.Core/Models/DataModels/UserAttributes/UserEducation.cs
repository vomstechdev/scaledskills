﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class UserEducation: UserBaseEntity
    {
        public string Name { get; set; }
        public string FieldOfStudy { get; set; }
        public string College { get; set; }
        public string Grade { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
