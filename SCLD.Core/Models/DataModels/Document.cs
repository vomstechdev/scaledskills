﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Document : BaseEntity
    {
        public System.Guid DocumentId { get; set; }
        public System.Guid Group { get; set; }
        public string FileName { get; set; }
        public string Ext { get; set; }
        public string Caption { get; set; }
        public Nullable<int> Size { get; set; }
        public string Type { get; set; }
        public string Filetype { get; set; }
        public string Directory { get; set; }
        public int? Order { get; set; }
        public bool? IsDelete { get; set; }
    }
}
