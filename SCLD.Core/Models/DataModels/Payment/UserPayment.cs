﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
   public class UserPayment : UserBaseEntity
    {
        public Guid PaymentId { get; set; }
        public string OrderId { get; set; }
        public decimal ServiceFee { get; set; }
        public decimal GST { get; set; }
        public decimal GetwayFee { get; set; }
        public decimal OtherFee { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public bool IsPaid { get; set; }
        public string PaymentType { get; set; }
        public UserPayment_Status PaymentStatus { get; set; }
        public string Messege { get; set; }
        public string ReturnUrl { get; set; }
        public string NotifyUrl { get; set; }
        public string TransactionId { get; set; }
        public string OrderCurrency { get; set; } = "INR";
        public string OrderNote { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
    }
}
