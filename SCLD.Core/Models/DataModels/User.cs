﻿using Microsoft.AspNetCore.Identity;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class User : IdentityUser,IEntity
    {
        public long RecId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender Gender { get; set; } = Gender.Male;
        public string Image { get; set; }
        public DateTime LastLoginTime { get; set; }
        public bool IsSubscribe { get; set; }
    }
}
