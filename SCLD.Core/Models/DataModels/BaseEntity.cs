﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public interface IEntity
    {
    }
    public class BaseEntity: IEntity
    {
        public virtual long Id { get; set; }
        public bool Status { get; set; } = true;
        public DateTime CreateDate { get; set; } = DateTime.Now.NowDate();
        public DateTime? ModifiedDate { get; set; }
    }

    public class UserBaseEntity : BaseEntity
    {
        public virtual long UserId { get; set; }
        
    }

}
