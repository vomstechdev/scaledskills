﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Contact: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Msg { get; set; }
        public ContactStatus ContactStatus { get; set; }
        public string Remarks { get; set; }
    }
}
