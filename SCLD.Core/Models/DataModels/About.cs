﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class About : BaseEntity
    {
		public string Name { get; set; }
        public string VedioUrl { get; set; }
        public string AboutText { get; set; }
		public string CoursesOfferedText { get; set; }
        public long EntityId { get; set; }
        public string EntityType { get; set; }
    }
}
