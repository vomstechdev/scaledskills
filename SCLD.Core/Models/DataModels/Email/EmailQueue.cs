﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class EmailQueue : UserBaseEntity
    {
        public static Int32 STATUS_DO_NOT_SEND = 0;
        public static Int32 STATUS_PENDING = 1;
        public static Int32 STATUS_InProgress = 2;
        public static Int32 STATUS_SENT = 3;
        public static Int32 STATUS_PUBLIC = 4;

        public string ToEmail { get; set; }

        public string CCEmail { get; set; }

        public string BCCEmail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public DateTime? QueueTime { get; set; }

        public int? EmailStatus { get; set; }

        public DateTime? SentTime { get; set; }

        public string SmtpServer { get; set; }
        public int? SmtpPort { get; set; }
        public string FromAddress { get; set; }
        public string UserName { get; set; }
        public int Retries { get; set; }

        public string Password { get; set; }

        public string DisplayName
        {
            get; set;
        }
        public string Seperator { get; set; }

        public string ErrorDescription { get; set; }
     }
}


