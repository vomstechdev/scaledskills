﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class EmailQueueAttachment : BaseEntity
    {
        public string FileName { get; set; }
    }
}
