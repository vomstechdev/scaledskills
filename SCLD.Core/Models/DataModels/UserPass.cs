﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
   public class UserPass : BaseEntity
    {
        public string Password { get; set; }
    }
}
