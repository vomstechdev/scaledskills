﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class OrderItem:BaseEntity
    {
        public long OrderId { get; set; }
        public long ItemId { get; set; }
        public string Reference { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
