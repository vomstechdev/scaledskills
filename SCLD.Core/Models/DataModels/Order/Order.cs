﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SCLD.Core.Models.DataModels
{
    public class Order:UserBaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string OrderAppId { get; set; }
        public string Reference { get; set; }
        public string ReferralCode { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }

    }
}
