﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.DataModels.Query
{
    public class MasterResult
    {
        ICollection<MasterQ> MasterQs { get; set; }
        ICollection<MasterQ> MastersQs { get; set; }
    }
    public class MasterQ
    {
        public long UserId { get; set; }
        public string Name { get; set; }
    }
}
