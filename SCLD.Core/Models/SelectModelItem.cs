﻿using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models
{
	public class SelectModelItem: SelectItem,IModel
    {
	}
    public class SelectItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool IsSelect { get; set; }
    }
    public class MultiSelectItems
	{
		public string Key { get; set; }
		public IEnumerable<SelectModelItem> Items { get; set; }
	}
}
