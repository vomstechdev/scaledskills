﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public interface IModel 
    {
    }
    public class Model: IModel
    {
        public virtual long Id { get; set; }
        [JsonIgnore]
        public virtual long UserId { get; set; }
    }
}
