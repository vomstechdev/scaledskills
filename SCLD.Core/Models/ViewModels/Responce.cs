﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace SCLD.Core.Models.ViewModels
{
	public class BaseResponse
	{
		public BaseResponse()
		{
			ResponseCode = ResponseCode.OK;
			ResponseMessege = "Request Successfully Proceed";
		}
		[JsonProperty("responseMessege")]
		public string ResponseMessege { get; set; }
		[JsonProperty("responseCode")]
		public ResponseCode ResponseCode { get; set; }
	}
	public class Response<T> : BaseResponse
	{
		[JsonProperty("result")]
		public T Result { get; set; }

	}
}
