﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
	public class OrganizationModel: Model
	{
		public string Name { get; set; }
		public string OwnerName { get; set; }
		public string Image { get; set; }
		public string ProfileUrl { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string GST { get; set; }
		public string PanNumber { get; set; }
		public AddressModel Address { get; set; }
		[JsonIgnore]
		public long? AddressId { get; set; }
		[JsonIgnore]
		public long? AboutId { get; set; }
		[JsonIgnore]
		public long? BankDetailId { get; set; }
		[JsonIgnore]
		public long? SocialId { get; set; }
    }
    public class OrganizationResult:Model
    {
        public string Name { get; set; }
		public string URL { get; set; }
	}
	public class OrganizationattrResult : OrganizationResult
	{
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
	}
}
