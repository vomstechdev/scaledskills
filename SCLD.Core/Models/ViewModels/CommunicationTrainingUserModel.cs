﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class CommunicationTrainingUserModel
    {
        public List<int> Trainings { get; set; }
        public List<Training_Users> userTypes { get; set; }
    }
}
