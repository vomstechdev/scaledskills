﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserAbout
    {
       public string ProfileUrl { get; set; }
       public string AboutText { get; set; }
       public IEnumerable<UserLanguageModel> Languages { get; set; }
    }
}
