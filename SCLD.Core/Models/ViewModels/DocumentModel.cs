﻿using Newtonsoft.Json;
using SCLD.Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class DocumentModel : Document, IModel
    {
        public string GetEncriptFile()
        {
            return string.Concat(Id, Ext);
        }
    }
}
