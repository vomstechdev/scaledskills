﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{

    public class SearchModel : SearchBase
    {
        public virtual long? UserId { get; set; }
        [JsonIgnore]
        public long? LoginUserId { get; set; }
    }
    public class SearchBase
    {
        public const string PageType_Training = "T";
        public const string PageType_Trainer = "P";
        public const string PageType_Organization = "O";
        public string PageType { get; set; }
        public string SearchText { get; set; }
        public int PageSize { get; set; } = 20;
        public int Page { get; set; }
    }
    public class SearchPageResult<T>
    {
        public IEnumerable<T> Results { get; set; }
        public int TotalCount { get; set; }
    }
    public class ReviewSearchPageResult : SearchPageResult<TrainingReviewModel>
    {
        public double AvgRating { get; set; }
    }
    public class HomeHeaderResult<T>
    {
        public IEnumerable<T> Trainings { get; set; }
        public IEnumerable<TopHeader> TopHeaders { get; set; }

    }
    public class TopHeader
    {
        public string Image { get; set; }
        public int ImageType { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get => Core.Helper.Util.GetDoc(Image); }
        public string FullUrl { get => Core.Helper.Util.GetTrainingPreview(Url); }
        public string Url { get; set; }
    }

    #region Foolow_Search
    public class FollowSearchModel : SearchBase
    {
        public long Id { get; set; }
        [JsonIgnore]
        public long? LoginUserId { get; set; }
        [JsonIgnore]
        public UserFollowType Type { get; set; }
    }
    #endregion
    #region Register_Search
    public class TrainingUserSearchModel : SearchBase
    {
        public long TrainingId { get; set; }
    }
    #endregion

}
