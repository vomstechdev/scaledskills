﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
   public class PublishTrainingModel : Model
    {
        [JsonIgnore]
        public override long Id { get; set; }
        public long TrainingId { get; set; }
        public bool IsPromotion { get; set; }
        public bool IsAccept { get; set; }
        public Training_Status Status { get; set; }
    }
}
