﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TicketPaymentDetailModel:Model
    {
        public long TicketId { get; set; }
        public string FeeText { get; set; }
        public Payment_FeeType FeeType { get; set; }
        public decimal Amount { get; set; }
        public bool IsPercentage { get; set; }
        public Payment_ModeType ModeType { get; set; }
    }
    public class TicketDetailData
    {
        public decimal TotalAmount { get; set; }
        public IEnumerable<TicketPaymentDetailModel> PaymentDetails{ get; set; }
    }
}
