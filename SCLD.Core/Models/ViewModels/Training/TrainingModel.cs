﻿using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TrainingModel : Model
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TimeZone { get; set; }
        public long HostedBy { get; set; } = 1;
        public Training_Status TrainingStatus { get; set; } = Training_Status.UnPublish;
        public IEnumerable<long> OrganizationList { get; set; }
        public IEnumerable<SelectItem> HostedByListObj { get; set; }
        public IEnumerable<SelectItem> OrganizationListObj { get; set; }
        public int ModeType { get; set; }
        public long AddressId { get; set; }
        public long TrainerId { get; set; }

    }
}
