﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TrainingLocation
    {
        public TrainingModeType ModeType { get; set; } = TrainingModeType.Offline;
        public string OnlineLocation { get; set; }
        public string LocationDetail { get; set; }
        public bool IsOnlineDetailsVisible { get; set; }
        public AddressModel AddressModel { get; set; }
    }
    public enum TrainingModeType
    {
        Online = 2,
        Offline = 1
    }
}
