﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TrainingReviewModel: Model
    {
        [JsonIgnore]
        public long TrainingId { get; set; }
        public int RateOfSatisfied { get; set; }
        public string LikeAbout { get; set; }
        public string ImproveAbout { get; set; }
        public string NeedInFeature { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class TrainingIsInterestModel
    {
        public bool IsInterest { get; set; } = true;
    }
}
