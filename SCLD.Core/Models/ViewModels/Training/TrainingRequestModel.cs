﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TrainingRequestModel : Model
    {
		public int NeedType { get; set; }
		public int TrainingType { get; set; }
		public string CompanyName { get; set; }
		public int NumberOfParticipant { get; set; }
		public string Summery { get; set; }
		public string Description { get; set; }
		public string Budget { get; set; }
		public string Keywords { get; set; }
		public AddressModel Address { get; set; }
	}
}
