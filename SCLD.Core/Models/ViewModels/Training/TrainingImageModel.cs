﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class TrainingImageModel :Model
    {
        [JsonIgnore]
        public long TrainingId { get; set; }
        public string Header { get; set; }
        public string Card { get; set; }
        public string CardUrl { get; set; }
        public string HeaderUrl { get; set; }
    }
}
