﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
   public class PaymentModel
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        public string ApiKey { get; set; } = "11514956fa50e1ef34dcf44c841511";
        public string SecretKey { get; set; } = "1544359f19eb29b5525abdd2f8e85a42d5799c04";
        [JsonProperty("orderAmount")]
        public double Amount { get; set; }
        [JsonProperty("referenceId")]
        public string TransactionId { get; set; }
        public string OrderCurrency { get; set; } = "INR";
        public string OrderNote { get; set; }
        [JsonProperty("txStatus")]
        public UserPayment_Status PaymentStatus { get; set; }
        [JsonProperty("txStatus")]
        public string Messege { get; set; }
        [JsonProperty("paymentMode")]
        public string PaymentMode { get; set; }
        [JsonProperty("txTime")]
        public DateTime TransactionTime { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string ReturnUrl { get; set; } = "http://localhost:53379/payment";
        public string NotifyUrl { get; set; } = "http://localhost:53379/payment";
        [JsonProperty("signature")]
        public string Signature { get; set; } 
            
    }
}
