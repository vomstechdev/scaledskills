﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class CommunicationViewModel
    {
        public string[] Emails { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        [JsonIgnore]
        public string Email { get; set; }
        [JsonIgnore]
        public long? UserId { get; set; }
    }
}
