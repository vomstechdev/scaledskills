﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserModel : RegisterUserRequest
    {
        public Gender Gender { get; set; }
        public string Image { get; set; }
        public string ReferralID { get; set; }
        public bool IsInterAffiliatePartner { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public AddressModel Address { get; set; }
        public bool IsSubscribe { get; set; }
    }
    public class TrainerUserModel : UserModel
    {
        public string ProfileUrl { get; set; }
    }
}
