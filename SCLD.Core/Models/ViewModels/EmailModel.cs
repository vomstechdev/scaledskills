﻿using Newtonsoft.Json;
using SCLD.Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class EmailModel : Model
    {
        public EmailModel()
        {
            Seperator = ",";
        }
        public string ToEmail { get; set; }
        [JsonIgnore]
        public string CCEmail { get; set; }
        [JsonIgnore]
        public string BCCEmail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
        [JsonIgnore]
        public int? EmailStatus { get; set; } = EmailQueue.STATUS_PENDING;
        [JsonIgnore]
        public DateTime? SentTime { get; set; }
        [JsonIgnore]
        public string DisplayName
        {
            get; set;
        }
        public string Seperator { get; set; }

    }
}
