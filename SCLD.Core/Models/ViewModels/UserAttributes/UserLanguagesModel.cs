﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserLanguageModel : Model
    {
        public int LanguageId { get; set; }
        public int Proficiency { get; set; }
    }
}
