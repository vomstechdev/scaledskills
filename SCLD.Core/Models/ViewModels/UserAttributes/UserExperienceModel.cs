﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
   public class UserExperienceModel : Model
    {
        public string Position { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
