﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserFollowModel : Model
    {
        [JsonIgnore]
        public UserFollowType Type { get; set; }
        [JsonIgnore]
        public override long Id { get; set; }
        public long TypeId { get; set; }
    }
    public class UserFollowUserModel
    {
        public long Id { get; set; }
        public long TypeId { get; set; }

        public UserModel User { get; set; }
    }
}
