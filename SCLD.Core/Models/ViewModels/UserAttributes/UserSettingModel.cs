﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserSettingModel : Model
    {
		[JsonIgnore]
        public string ReferralID { get; set; }
        public string ProfileUrl { get; set; }
		public string PublicUrl { get; set; }
        public bool IsPublishUrl { get; set; }
        [JsonIgnore]
        public bool IsInterAffiliatePartner { get; set; }
        public bool IsPublicCertifications { get; set; }
        public bool IsPublicSocialMedia { get; set; }
        public bool IsDefaultLanguage { get; set; }
        public string AboutText { get; set; }
    }
	public class UserProfileUserSetting : UserSettingModel
	{
		
	}
}
