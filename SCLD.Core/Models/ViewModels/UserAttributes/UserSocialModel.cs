﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserSocialModel : Model
    {
        public string LinkedInUrls { get; set; }
        public string FacebookUrls { get; set; }
        public string TwitterUrls { get; set; }
        public string WebsiteUrls { get; set; }
    }
}
