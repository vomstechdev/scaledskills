﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class BankDetailModel : Model
    {
		public string Name { get; set; }
		public string AccountNumber { get; set; }
		public int AccountType { get; set; }
		public string BankName { get; set; }
		public string BranchName { get; set; }
		public string IFSCCode { get; set; }
		public string GSTNum { get; set; }
        public string UPI { get; set; }
        public Guid? ExemptionDocId { get; set; }
        public Guid? PanCardDocId { get; set; }
        public Guid? AdharCardDocId { get; set; }
        public Guid? CancellationDocId { get; set; }

        public string ExemptionDocUrl { get; set; }
        public string PanCardDocUrl { get; set; }
        public string AdharCardDocUrl { get; set; }
        public string CancellationDocUrl { get; set; }

    }
}
