﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class OrderItemModel<T> : OrderAttrItemModel
    {
        public virtual T Item { get; set; }
        [JsonIgnore]
        public override long OrderId { get; set; }
        [JsonIgnore]
        public override string Reference { get; set; }
        [JsonIgnore]
        public override long Id { get; set; }

    }
    public class OrderAttrItemModel : Model
    {
        public virtual long OrderId { get; set; }
        public virtual long ItemId { get; set; }
        public virtual string Reference { get; set; }
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
