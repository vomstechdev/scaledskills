﻿using Newtonsoft.Json;
using SCLD.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class OrderModel : Model
    {
        [JsonIgnore]
        public override long Id { get; set; }
        public string OrderAppId { get; set; }
        [JsonIgnore]
        public string Reference { get; set; }
        [JsonIgnore]
        public OrderStatus OrderStatus { get; set; }
        public string OrderStatusName { get {return  OrderStatus.ToString();  } }
        public string Address { get; set; }
        public string Description { get; set; }
        public decimal TotalAmount { get; set; }
        public string ReferralCode { get; set; }
        public IEnumerable<OrderItemModel<object>> Items { get; set; }
    }
}
