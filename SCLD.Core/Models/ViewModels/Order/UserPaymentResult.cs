﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class UserPaymentResult
    {
        public decimal ServiceTax { get; set; }
        public decimal GST { get; set; }
        public decimal Getwayfee { get; set; }
        public decimal Affilation { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public decimal BuyerTotal { get; set; }
        public decimal OrganizerTotal { get; set; }
    }
}
