﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
    public class RegisterUserRequest : CredentialsViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
		public string PhoneNumber { get; set; }
        public bool? IsTrainer { get; set; } = true;
    }
}
