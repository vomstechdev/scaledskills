﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.ViewModels
{
	public class AboutModel : Model
	{
		public string Name { get; set; }
		public string VideoUrl { get; set; }
		public string AboutText { get; set; }
		public string CoursesOfferedText { get; set; }
		[JsonIgnore]
		public long EntityId { get; set; }
		[JsonIgnore]
		public string EntityType { get; set; }
	}
}
