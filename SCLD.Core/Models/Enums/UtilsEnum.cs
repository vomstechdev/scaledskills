﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Models.Enums
{
    public enum Gender
    {
        UnKnown = 0,
        Male = 1,
        FeMale = 2,
        Other = 3
    }
    public enum MasterKey
    {
        COUN,
        STATE,
        CITY,
        LANG,
        TRNGNEEDTYPE,
        ZONE,
        TRNGTYPE
    }
    public enum UserFollowType
    {
        Training = 3 ,
        Trainer = 1,
        Organization = 2
    }
    public enum OrderStatus
    {
        Open = 1,
        Pending = 2,
        Complete = 3,
        Cancel = 4
    }
    public enum ContactStatus
    {
        Open = 1,
        Pending = 2,
        OnHold = 3,
        Complete = 4
    }
    public enum Payment_ModeType
    {
        Organizer = 1,
        Buyer = 2
    }
    public enum Training_Status
    {
        UnPublish = 0,
        Publish = 1
    }
    public enum Training_Hosted
    {
        Individual = 1,
        Organization = 2
    }
    public enum Training_Users
    {
        Registered = 1,
        Interested = 2,
        Member = 3,
        PendingRegistration = 4,
        FeedbackReceived = 5
    }
    public enum Payment_FeeType
    {
        ServiceTax = 777,
        GateWay = 778,
        AffilationMarketing = 776,
        GST = 779
    }
    public enum UserPayment_Status
    {
        SUCCESS,
        FAILED,
        PENDING,
        CANCELLED,
        FLAGGED
    }
}
