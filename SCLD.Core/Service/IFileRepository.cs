﻿using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IFileRepository
    {
        DocumentModel Add(DocumentModel model);
        DocumentModel Update(DocumentModel model);
        void SetOrderFile(Guid Id, int order);
        DocumentModel GetFile(Guid Id);
        IEnumerable<DocumentModel> GetAllFile(Guid Group);
        void Delete(Guid Id);
    }
}
