﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Service
{
    public interface IGoogleService
    {
        Task<object> GetInfoForImage(string path);
    }
}
