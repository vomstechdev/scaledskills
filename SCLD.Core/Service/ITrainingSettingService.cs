﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface ITrainingSettingService
    {
        TrainingSettingModel GetTrainingSettingById(object id);
        IEnumerable<TrainingSetting> GetTrainingSettingByTrainingId(long id);
        long InsertTrainingSetting(TrainingSettingModel model);
        long UpdateTrainingSetting(TrainingSettingModel model);
    }
}
