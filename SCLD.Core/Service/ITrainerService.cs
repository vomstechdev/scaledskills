﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface ITrainerService
	{
		TrainerModel GetTrainerById(object id);
        TrainerModel GetByUrl(string url);
        TrainerModel GetTrainerByUserId(long userId);
		long InsertTrainer(TrainerModel model);
		long UpdateTrainer(TrainerModel model);
		long UpdateAbout(AboutModel model, long id);
		long UpdateUserSocial(UserSocialModel model, long id);
		long UpdateBankDetail(BankDetailModel model, long id);
        bool ValidateUrl(string url, long? userId);

    }
}
