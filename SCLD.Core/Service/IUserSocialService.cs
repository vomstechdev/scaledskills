﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IUserSocialService
    {
        UserSocialModel GetUserSocialById(object id);
        IEnumerable<UserSocial> GetUserSocialByUserId(long id);
        long InsertUserSocial(UserSocialModel model);
        long UpdateUserSocial(UserSocialModel model);
		long Save(UserSocialModel model);

	}
}
