﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IUserCertificateService
    {
        UserCertificateModel GetUserCertificateById(object id);
        IEnumerable<UserCertificate> GetUserCertificateByUserId(long id);
        long InsertUserCertificate(UserCertificateModel model);
        long UpdateUserCertificate(UserCertificateModel model);
    }
}
