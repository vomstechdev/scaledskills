﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface ITicketService
    {
        IEnumerable<TicketPaymentDetailModel> GetTicketPaymentDetails(long? ticketId);
        void SaveTicketPaymentDetail(IEnumerable<TicketPaymentDetailModel> ticketPaymentDetails);
        UserPaymentResult GetTicketPaymentPreview(TicketDetailData ticketDetailData);
        UserPaymentResult GetTicketPaymentPreview(long? ticketId);
    }
}
