﻿using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IOrderService
    {
        string CreateAsOpenOrder(OrderModel orderModel);
        string SaveAsPendingOrder(string OrderUniqId);
        OrderModel GetOrder(string OrderUniqId);
        string SaveAsCompleteOrder(string OrderUniqId);
        string SaveAsCancleOrder(string OrderUniqId);
        object OrderSummary(string OrderUniqId);
    }
}
