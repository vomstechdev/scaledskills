﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IPurchasable
    {
       OrderModel GetOrder(string OrderUniqId);
       string CreateOrder(OrderModel model);
       string ProcessOrder(string OrderUniqId);
        object OrderSummary(string OrderUniqId);
        string CancleOrder(string OrderUniqId);
        string PaymentAndProcessOrder(string OrderUniqId);
        string SaveOrderAndMarkAsDone(PaymentModel paymentModel);
        UserPayment GetPayment(string paymentId);
       IOrderService GetOrderServiceByOrderId(string OrderUniqId);
    }
    public interface IServicePurchasable
    {
        void CancleAllPendingOrder();
    }
}
