﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Service
{
    public interface ITrainingService
    {
        TrainingModel GetTrainingById(object id);
        IEnumerable<Training> GetTrainingByUserId(long id);
        long InsertTraining(TrainingModel model);
        long UpdateTraining(TrainingModel model);
        TrainingLocation GetTrainingAddress(long id);
        long UpdateAddress(TrainingLocation model, long id);
        TrainingTicketModel GetTicketById(object id);
        IEnumerable<TrainingTicketModel> GetTrainingTicketByTicketId(long id);
        long InsertTrainingTicket(TrainingTicketModel model);
        long UpdateTrainingTicket(TrainingTicketModel model);
        TrainingReviewModel GetReviewById(object id);
        TrainingReviewModel GetReviewByUserIdAndTrainingId(long trainingId, long userId);
        IEnumerable<TrainingReviewModel> GetTrainingReviewByTrainingId(long id);
        long InsertTrainingReview(TrainingReviewModel model);
        long UpdateTrainingReview(TrainingReviewModel model);
        long UpdateAbout(AboutModel model, long id);
        AboutModel GetTicketAbout(long id);
        IEnumerable<TrainingTrainer> GetTrainingTrainersTicket(long TrainingId);
        void DeleteTrainingTrainer(long TrainersIds, long TrainingId);
        void AddTrainingTrainer(long TrainersId, long TrainingId);
        long SaveTrainingPromotion(TrainingPromotionModel model);
        TrainingPromotionModel GetTrainingPromotion(long id);
        long SaveTrainingSetting(TrainingSettingModel model);
        TrainingSettingModel GetTrainingSetting(long id);
        long SaveTrainingOrganization(TrainingModel model);
        long SaveTrainingImage(TrainingImageModel model);
        TrainingImageModel GetTrainingImage(long id);
        int SetTrainingInterast(TrainingInterested trainingInterested);
        bool ValidateUrl(string url, long? userId);
        IEnumerable<OrganizationattrResult> GetOrganizationList(long TrainingId);
        Task PublishTrainingAsync(PublishTrainingModel publishTrainingModel);
    }

    public interface ITrainingListService
    {
        SearchPageResult<Result_CardTraining_view> GetRecentTraining(SearchModel model);
        SearchPageResult<Result_CardTraining_view> GetPastTraining(SearchModel model);
        SearchPageResult<Result_CardTraining_view> GetUpCommingTraining(SearchModel model);
        Result_Trainings_view GetTrainingByUrl(string url, long userId);
        void SetTrainingHosted(ref Result_Trainings_view training);
        HomeHeaderResult<Result_CardTraining_view> GetHomeHeader();
        IEnumerable<TrainingTicketModel> GetTrainingTicketsByUrl(string url);
        SearchPageResult<Result_CardTraining_view> GetRelatedTraining(SearchModel model);
        SearchPageResult<Result_CardTraining_view> GetAttendiesUpCommingTraining(SearchModel model);
        SearchPageResult<Result_CardTraining_view> GetAttendiesPastTraining(SearchModel model);
        ReviewSearchPageResult GetTrainingReview(SearchModel model);
    }
}
