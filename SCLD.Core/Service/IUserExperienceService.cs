﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IUserExperienceService
    {
        UserExperienceModel GetUserExperienceById(object id);
        IEnumerable<UserExperience> GetUserExperienceByUserId(long id);
        long InsertUserExperience(UserExperienceModel model);
        long UpdateUserExperience(UserExperienceModel model);
    }
}
