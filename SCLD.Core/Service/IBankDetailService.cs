﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface IBankDetailService
	{
		BankDetailModel GetBankDetailById(object id);
        BankDetailModel GetBankDetailByUserId(long id);
		long InsertBankDetail(BankDetailModel model);
		long UpdateBankDetail(BankDetailModel model);
		long Save(BankDetailModel model);
	}
}
