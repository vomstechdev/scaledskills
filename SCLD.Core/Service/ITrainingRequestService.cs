﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface ITrainingRequestService
	{
		TrainingRequestModel GetTrainingRequestById(object id);
        IEnumerable<TrainingRequest> GetTrainingRequestByUserId(long id);
        long InsertTrainingRequest(TrainingRequestModel model);
        long UpdateTrainingRequest(TrainingRequestModel model);
    }
}
