﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IAddressService
    {
        AddressModel GetAddressById(object id);
        IEnumerable<Address> GetAddressByUserId(long id);
        long InsertAddress(AddressModel model);
        long UpdateAddress(AddressModel model);
        long Save(AddressModel model);
		long SaveUserAddress(AddressModel model);

	}
}
