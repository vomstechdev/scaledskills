﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface IUserSettingService
	{
		UserSettingModel GetUserSettingById(object id);
		UserSettingModel GetUserSettingByUserId(long id);
		long InsertUserSetting(UserSettingModel model);
		long UpdateUserSetting(UserSettingModel model);
		long UpdateInterAffiliatePartner(long userId, bool isInterAffiliatePartner, string referralID);
		long UpdateProfileUserSetting(UserProfileUserSetting model);
	}
}
