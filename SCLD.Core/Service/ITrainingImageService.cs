﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface ITrainingImageService
    {
        TrainingImageModel GetTrainingImageById(object id);
        IEnumerable<TrainingImage> GetTrainingImageByTrainingId(long id);
        long InsertTrainingImage(TrainingImageModel model);
        long UpdateTrainingImage(TrainingImageModel model);
    }
}
