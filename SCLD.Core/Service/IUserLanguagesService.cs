﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IUserLanguagesService
    {
        UserLanguageModel GetUserLanguagesById(object id);
        IEnumerable<UserLanguage> GetUserLanguagesByUserId(long id);
        long InsertUserLanguages(UserLanguageModel model);
        long UpdateUserLanguages(UserLanguageModel model);
    }
}
