﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface IAboutService
	{
		AboutModel GetTagById(object id);
		IEnumerable<About> GetTagByEntityId(long id);
		long InsertAbout(AboutModel model);
		long UpdateAbout(AboutModel model);
		long Save(AboutModel model);
	}
}
