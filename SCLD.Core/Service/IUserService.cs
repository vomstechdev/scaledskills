﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Service
{
    public interface IUserService
    {
        Task<UserModel> GetUserById(object id);
        Task<bool> Create(RegisterUserRequest model);
        Task<bool> Update(UserModel model);
        Task<User> GetUserByPasswordAsync(string userName, string password);
        Task<bool> ChangePassword(string userId, string currentPassword, string newPassword);
        Task<bool> UpdateWithAttributeDetails(UserModel model);
        Task<bool> CreateAsTrainner(RegisterUserRequest model);
        Task<string> GeneratePasswordResetTokenByEmailAsync(string email);
        Task<bool> ResetPassword(string userId, string code, string newPassword);
        Task SendConfirmEmailAsync(string email);
        Task<bool> ConfirmEmail(string userId, string code);
        Task<bool> UnsubscribeUser(string id);
        Task<bool> SendMailToSubscriber(TrainingModel trainingModel, TrainingImageModel trainingImage);
    }
}
