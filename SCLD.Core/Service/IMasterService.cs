﻿using SCLD.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface IMasterService
	{
		IEnumerable<SelectModelItem> GetMasterData(string key);
		IEnumerable<MultiSelectItems> GetMasterData(params string[] keys);
		IEnumerable<SelectModelItem> GetMasterData(string key, string preSelect);
		IEnumerable<SelectModelItem> GetMasterData(string key, long? parentId);
		IEnumerable<SelectModelItem> GetMasterData(string key, long? parentId, string preSelect);
        void SetEmail(Core.Models.ViewModels.EmailModel emailModel);
        void AddContact(Core.Models.ViewModels.ContactModel contactModel);
    }
}
