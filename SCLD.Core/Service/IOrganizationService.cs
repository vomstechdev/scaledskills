﻿using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IOrganizationService
    {
        OrganizationModel GetOrganizationById(object id);
        OrganizationModel GetByUrl(string url);
        OrganizationModel GetOrganizationByUserId(long userId);
        IEnumerable<OrganizationResult> GetOrganizationList();
        long InsertOrganization(OrganizationModel model);
        long UpdateOrganization(OrganizationModel model);
        long UpdateAbout(AboutModel model, long id);
        long UpdateUserSocial(UserSocialModel model, long id);
        long UpdateBankDetail(BankDetailModel model, long id);
        IEnumerable<SelectItem> GetListByOrganizer(long userId, Training_Hosted training_Hosted);
        IEnumerable<SelectItem> GetUserListByTrainings(List<int> trainings, IEnumerable<Training_Users> users);
        void SendCommunicationEmail(CommunicationViewModel communicationViewModel);
        IEnumerable<SelectItem> GetTrainingListByOrganizations(long userId, List<long> organizations);
        IEnumerable<SelectItem> GetUserOrganizations(long loginId);
    }
}
