﻿using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IHomeService
    {
        void UserFollow(UserFollowModel model);
        bool IsFollow(UserFollowModel model);
        SearchPageResult<UserFollowUserModel> UserMemberFollowList(FollowSearchModel model);
        SearchPageResult<TrainerUserModel> RegisterTrainerUsersList(TrainingUserSearchModel model);
        SearchPageResult<UserModel> RegisterTrainingUsersList(TrainingUserSearchModel model);
    }
}
