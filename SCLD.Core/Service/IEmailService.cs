﻿using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
	public interface IEmailService
	{
        long Save(EmailModel model);
        string GetBody(Body body, object model);
        string BindProperty(Body body, object model);
        string MainBody(string template);
    }
    public interface IEmailSendingService
    {
        void SendEmailsAsync();
    }
    public enum Body
    {
        ForgotPassword ,
        ConfirmEmail,
        TrainingRegister,
        Ticket_TrainingRegister,
        TrainingNotification
    }
}
