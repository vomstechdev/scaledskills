﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Service
{
    public interface IUserEducationService
    {
        UserEducationModel GetUserEducationById(object id);
        IEnumerable<UserEducation> GetUserEducationByUserId(long id);
        long InsertUserEducation(UserEducationModel model);
        long UpdateUserEducation(UserEducationModel model);
    }
}
