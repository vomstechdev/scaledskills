﻿using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Core.Data
{
    public interface IUserRepository
    {
        Task<bool> CreateAsync(RegisterUserRequest model);
        Task<User> GetUserByPasswordAsync(string userName, string password);
        Task<bool> UpdateAsync(UserModel entity);
        Task<User> FindByNameAsync(string userName);
        Task<User> GetByUserId(string userId);
        Task<bool> ChangePassword(string userId, string currentPassword, string newPassword);
        Task<string> GeneratePasswordResetTokenAsync(string email, Action<User,string> action = null);
        Task<string> GenerateEmailConfirmTokenAsync(string email, Action<User, string> action = null);
        Task<bool> ResetPassword(string userId, string code, string newPassword);
        Task<bool> ConfirmEmail(string userId, string code);
        IQueryable<User> Users { get; }
        Task<bool> UnsubscribeUser(string id);
        List<User> GetAllSubscribers();
    }
}
