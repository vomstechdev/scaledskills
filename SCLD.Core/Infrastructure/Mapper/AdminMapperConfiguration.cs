﻿using AutoMapper;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;

namespace SCLD.Core.Infrastructure.Mapper
{
    class MapperConfiguration : Profile, IOrderedMapperProfile
    {
        public int Order => 1;
        public MapperConfiguration()
        {
            CreateEntityMaps();
        }
        protected virtual void CreateEntityMaps()
        {
            CreateMap<RegisterUserRequest, User>().ReverseMap();
            CreateMap<UserProfileUserSetting, UserSetting>()
            .ForMember(x => x.UserId, options => options.Ignore())
            .ForMember(x => x.ReferralID, options => options.Ignore())
            .ForMember(x => x.IsInterAffiliatePartner, options => options.Ignore());
            CreateMap<UserSettingModel, UserSetting>().ReverseMap();
            CreateMap<UserSocialModel, UserSocial>().ReverseMap();
            CreateMap<UserLanguage, UserLanguageModel>().ReverseMap();
            CreateMap<UserCertificateModel, UserCertificate>().ReverseMap();
            CreateMap<UserEducationModel, UserEducation>().ReverseMap();
            CreateMap<UserExperienceModel, UserExperience>().ReverseMap();
            CreateMap<TrainingModel, Training>()
                .ForMember(e => e.IsUserAccept, options => options.Ignore())
                .ForMember(e => e.OnlineLocation, options => options.Ignore())
                .ForMember(e => e.AddressId, options => options.Ignore())
                .ForMember(e => e.AboutId, options => options.Ignore())
                .ForMember(e => e.IsRecurrence, options => options.Ignore())
                .ForMember(e => e.IsUserAccept, options => options.Ignore())
                .ForMember(e => e.IsPromotion, options => options.Ignore())
                .ForMember(e => e.ModeType, options => options.Ignore())
                .ForMember(e => e.TrainingStatus, options => options.Ignore())
                .ReverseMap();
            CreateMap<TrainingImageModel, TrainingImage>().ReverseMap();
            CreateMap<TrainingSettingModel, TrainingSetting>().ReverseMap();
            CreateMap<AddressModel, Address>().ReverseMap();
            CreateMap<BankDetailModel, BankDetail>().ReverseMap();
            CreateMap<TrainingRequestModel, TrainingRequest>()
                .ForMember(e => e.City, options => options.MapFrom(z => z.Address.City))
                .ForMember(e => e.StateId, options => options.MapFrom(z => z.Address.StateId))
                .ForMember(e => e.CountryId, options => options.MapFrom(z => z.Address.CountryId))
                .ForMember(e => e.ZipCode, options => options.MapFrom(z => z.Address.ZipCode)).ReverseMap();
            CreateMap<AboutModel, About>().ForMember(e => e.VedioUrl, option => option.MapFrom(z => z.VideoUrl));
            CreateMap<About, AboutModel>().ForMember(e => e.VideoUrl, option => option.MapFrom(z => z.VedioUrl));
            CreateMap<UserModel, User>()
                .ForMember(e => e.PasswordHash, options => options.Ignore())
                .ForMember(e => e.AccessFailedCount, options => options.Ignore())
                .ForMember(e => e.ConcurrencyStamp, options => options.Ignore())
                .ForMember(e => e.EmailConfirmed, options => options.Ignore())
                .ForMember(e => e.Id, options => options.Ignore())
                .ForMember(e => e.LockoutEnabled, options => options.Ignore())
                .ForMember(e => e.LockoutEnd, options => options.Ignore())
                .ForMember(e => e.NormalizedEmail, options => options.Ignore())
                .ForMember(e => e.NormalizedUserName, options => options.Ignore())
                .ForMember(e => e.PhoneNumberConfirmed, options => options.Ignore())
                .ForMember(e => e.RecId, options => options.Ignore())
                .ForMember(e => e.SecurityStamp, options => options.Ignore())
                .ForMember(e => e.TwoFactorEnabled, options => options.Ignore())
                .ForMember(e => e.UserName, options => options.Ignore());
            CreateMap<User, UserModel>().ForMember(x => x.UserId, options => options.Ignore())
                .ForMember(x => x.Id, options => options.Ignore());
            CreateMap<MasterData, SelectModelItem>().ForMember(x => x.Text, y => y.MapFrom(z => z.Name))
                .ForMember(x => x.Value, y => y.MapFrom(z => z.Id.ToString()));
            CreateMap<TrainerModel, Trainer>().ForMember(e => e.AboutId, options => options.Ignore())
                .ForMember(e => e.AddressId, options => options.Ignore())
                .ForMember(e => e.BankDetailId, options => options.Ignore())
            .ForMember(e => e.SocialId, options => options.Ignore());
            CreateMap<Trainer, TrainerModel>();
            CreateMap<OrganizationModel, Organization>().ForMember(e => e.AboutId, options => options.Ignore())
                .ForMember(e => e.AddressId, options => options.Ignore())
                .ForMember(e => e.BankDetailId, options => options.Ignore())
                .ForMember(e => e.SocialId, options => options.Ignore());
            CreateMap<Organization, OrganizationModel>();
            CreateMap<Organization, OrganizationResult>();
            CreateMap<EmailModel, EmailQueue>().ReverseMap();
            CreateMap<TrainingImageModel, TrainingImage>()
                .ForMember(e => e.Id, options => options.Ignore()).ReverseMap();
            CreateMap<TrainingTicketModel, TrainingTicket>()
                .ForMember(e => e.AvailableQty, options => options.Ignore()).ReverseMap();
            CreateMap<TrainingReviewModel, TrainingReview>().ReverseMap();
            CreateMap<Core.Models.ViewModels.TrainingPromotionModel, TrainingPromotion>().ReverseMap();
            CreateMap<Core.Models.ViewModels.DocumentModel, Document>()
                .ForMember(e => e.Id, options => options.Ignore())
                .ReverseMap();
            CreateMap<UserFollowModel, UserFollow>()
                .ForMember(e => e.Id, options => options.Ignore())
                .ReverseMap();
            CreateMap<OrderModel, Order>()
                .ForMember(e => e.Id, options => options.Ignore())
                .ForMember(e => e.OrderAppId, options => options.Ignore())
                .ReverseMap();
            CreateMap<OrderItemViewModel, OrderItem>()
                .ForMember(e => e.Id, options => options.Ignore())
                .ReverseMap();
            CreateMap<OrderItemModel<object>, OrderItem>()
               .ForMember(e => e.Id, options => options.Ignore())
               .ReverseMap();
            CreateMap<ContactModel, Contact>()
               .ForMember(e => e.Id, options => options.Ignore())
               .ReverseMap();
            CreateMap<TicketPaymentDetailModel, TicketPaymentDetail>()
               .ForMember(e => e.Id, options => options.Ignore())
               .ReverseMap();

        }
    }
}
