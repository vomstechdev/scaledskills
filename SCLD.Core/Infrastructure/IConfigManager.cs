﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Core.Infrastructure
{
    public interface IConfigManager
    {
        T GetSection<T>();
        T GetSection<T>(string key);
        T GetService<T>();
        object GetService(Type serviceType);
    }
    public enum ObjectType
    {
        Singleton,
         Factory,
         Scope
    }
}
