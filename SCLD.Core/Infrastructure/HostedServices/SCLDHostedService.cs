﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SCLD.Core.Infrastructure.HostedServices
{
	public class SCLDHostedService : IHostedService, IDisposable
	{
		private Timer _timer;
		private Timer _timer1;
		readonly ILogger _logger;
		public SCLDHostedService(IServiceProvider services, ILogger<SCLDHostedService> logger)
		{
			Services = services;
			_logger = logger;
		}
		public IServiceProvider Services { get; }
		public Task StartAsync(CancellationToken cancellationToken)
		{
			_timer = new Timer(DoWork, null, TimeSpan.Zero,
			TimeSpan.FromSeconds(30));
			_timer1 = new Timer(DoOrderCancleService, null, TimeSpan.Zero,
			TimeSpan.FromMinutes(1));
			return Task.CompletedTask;
		}
		private void DoWork(object state)
		{
			using (
                var scope = Services.CreateScope())
			{
				var scopedProcessingService =
					scope.ServiceProvider
						.GetRequiredService<IEmailSendingService>();

				scopedProcessingService.SendEmailsAsync() ;
			}
		}
		private void DoOrderCancleService(object state)
		{
			using (
				var scope = Services.CreateScope())
			{
				var scopedProcessingService =
					scope.ServiceProvider
						.GetRequiredService<IServicePurchasable>();

				scopedProcessingService.CancleAllPendingOrder();
			}
		}
		public Task StopAsync(CancellationToken cancellationToken)
		{
			_logger.LogInformation("Timed Background Service is stopping.");

			_timer?.Change(Timeout.Infinite, 0);
			_timer1?.Change(Timeout.Infinite, 0);

			return Task.CompletedTask;
		}
		public void Dispose()
		{
			_timer?.Dispose();
			_timer1?.Dispose();
		}
	}
}
