﻿using AutoMapper;
using SCLD.Core.Helper.TypeFinder;
using SCLD.Core.Infrastructure;
using SCLD.Core.Infrastructure.DependencyManagement;
using SCLD.Core.Infrastructure.Mapper;
using SCLD.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCLD.Mapping
{
    public partial class DependencyConfig
    {
        static DependencyConfig()
        {
            Current = new DependencyConfig();
        }
        public static DependencyConfig Current { get; set; }
    }
    public partial class DependencyConfig
    {
        private DependencyConfig()
        {

        }
        public void Build(Action<Type, Type, int> registerAction)
        {
            var dependency = new Dictionary<Type, Type>();
            ITypeFinder typeFinder = new TypeFinder(AppDomain.CurrentDomain.GetAssemblies());
            var dependencyRegistrars = typeFinder.FindClassesOfType<IDependencyRegistrar>();
            var instances = dependencyRegistrars
                .Select(dependencyRegistrar => (IDependencyRegistrar)Activator.CreateInstance(dependencyRegistrar))
                .OrderBy(dependencyRegistrar => dependencyRegistrar.Order);
            foreach (var dependencyRegistrar in instances)
                dependencyRegistrar.Register(ref dependency);
            var dep = new SCLD.Service.DependencyRegister();
            //dep.Register(ref dependency);
            foreach (var service in dependency)
                registerAction?.Invoke(service.Key, service.Value, (int)ObjectType.Factory);

        }
        public void BuildPreObjectService(Action<object, int> registerAction)
        {
            ITypeFinder typeFinder = new TypeFinder(AppDomain.CurrentDomain.GetAssemblies());
            var dependencyRegistrars = typeFinder.FindClassesOfType<IOrderedMapperProfile>();
            var mappers = dependencyRegistrars
               .Select(dependencyRegistrar => (IOrderedMapperProfile)Activator.CreateInstance(dependencyRegistrar))
               .OrderBy(dependencyRegistrar => dependencyRegistrar.Order);
            var config = new MapperConfiguration(cfg =>
            {
                foreach (var map in mappers)
                {
                    cfg.AddProfile(map.GetType());
                }
            });
            AutoMapperConfiguration.Init(config);
            registerAction.Invoke(AutoMapperConfiguration.Mapper, (int)ObjectType.Singleton);
            //register
        }
    }
}
