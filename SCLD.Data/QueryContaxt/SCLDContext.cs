﻿using Microsoft.EntityFrameworkCore;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.DataModels.Query;
using SCLD.Core.Models.ViewModels;

namespace SCLD.Data
{
    public partial class SCLDContext 
    {
       public DbQuery<MasterQ> MasterQs { get; set; }
        public DbQuery<Result_Trainings_view> Trainings_vw { get; set; }
        public DbQuery<Result_CardTraining_view> CardTraining_vw { get; set; }
        public DbQuery<TicketPaymentDetailModel> TicketPaymentDetailModel_vw { get; set; }
        public DbQuery<SelectItem> SelectItem { get; set; }
    }
}
