﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure.DependencyManagement;
using SCLD.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Data
{
    class DependencyRegister : IDependencyRegistrar
    {
        public int Order => 1;

        public void Register(ref Dictionary<Type, Type> dependencies)
        {
            dependencies.Add(typeof(IDbContext), typeof(SCLDContext));
            dependencies.Add(typeof(IRepository<>), typeof(DataRepository<>));
            dependencies.Add(typeof(IUserRepository), typeof(UserRepository));
        }
    }
}
