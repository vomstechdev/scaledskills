﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SCLD.Data.Migrations
{
    public partial class Column_N_UserPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Messege",
                table: "UserPayments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotifyUrl",
                table: "UserPayments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentStatus",
                table: "UserPayments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ReturnUrl",
                table: "UserPayments",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "UserPayments",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Messege",
                table: "UserPayments");

            migrationBuilder.DropColumn(
                name: "NotifyUrl",
                table: "UserPayments");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "UserPayments");

            migrationBuilder.DropColumn(
                name: "ReturnUrl",
                table: "UserPayments");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserPayments");
        }
    }
}
