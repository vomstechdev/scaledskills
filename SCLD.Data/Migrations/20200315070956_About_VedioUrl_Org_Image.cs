﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SCLD.Data.Migrations
{
    public partial class About_VedioUrl_Org_Image : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Organizations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VedioUrl",
                table: "Abouts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "VedioUrl",
                table: "Abouts");
        }
    }
}
