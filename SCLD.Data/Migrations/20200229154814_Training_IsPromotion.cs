﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SCLD.Data.Migrations
{
    public partial class Training_IsPromotion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPromotion",
                table: "Trainings",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPromotion",
                table: "Trainings");
        }
    }
}
