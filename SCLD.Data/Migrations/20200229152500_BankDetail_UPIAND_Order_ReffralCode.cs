﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SCLD.Data.Migrations
{
    public partial class BankDetail_UPIAND_Order_ReffralCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ReferralCode",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UPI",
                table: "BankDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferralCode",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "UPI",
                table: "BankDetails");
        }
    }
}
