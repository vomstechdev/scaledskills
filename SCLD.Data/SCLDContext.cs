﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SCLD.Core.Models.DataModels;
using SCLD.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCLD.Data
{
    public partial class SCLDContext : IdentityDbContext<User>, IDbContext
    {
        public SCLDContext(DbContextOptions<SCLDContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Query<Result_CardTraining_view>().ToView("CardTraining_view");
            builder.Query<Result_Trainings_view>().ToView("Trainings_view");
            builder.Entity<User>(entity =>
            {
                entity.ToTable(name: "Users");
                entity.Property(p => p.Id).HasColumnName("Id");
                entity.Property(x => x.RecId).ValueGeneratedOnAdd();
                entity.Property(x => x.RecId).Metadata.AfterSaveBehavior = Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Ignore;
            });
            builder.Entity<Trainer>(entity =>
            {
                entity.HasIndex(x => x.ProfileUrl).IsUnique()
                        .HasName("TrainerProfileUrlIndex")
                        .HasFilter("[ProfileUrl] IS NOT NULL");

            });
            builder.Entity<Training>(entity =>
            {
                entity.HasIndex(x => x.Url).IsUnique()
                        .HasName("TrainingUrlIndex")
                        .HasFilter("[Url] IS NOT NULL");

            });
            builder.Entity<TrainingPromotion>(entity =>
            {
                entity.Property(p => p.AffliliateRate).HasColumnType("decimal(18,2)");
                entity.Property(p => p.DisplayBannerHomePageRate).HasColumnType("decimal(18,2)");
                entity.Property(p => p.AdvertisementRate).HasColumnType("decimal(18,2)");
                entity.Property(p => p.DisplayCardHomePageRate).HasColumnType("decimal(18,2)");
            });
            builder.Entity<TrainingTicket>(entity =>
            {
                entity.Property(p => p.PaymentCharge).HasColumnType("decimal(18,2)");
            });
            builder.Entity<Organization>(entity =>
            {
                entity.HasIndex(x => x.ProfileUrl).IsUnique()
                        .HasName("OrganizationProfileUrlIndex")
                        .HasFilter("[ProfileUrl] IS NOT NULL");
            });
            builder.Entity<About>(entity =>
            {
                entity.Property(x => x.AboutText).HasColumnType("NVARCHAR(MAX)");
                entity.Property(x => x.CoursesOfferedText).HasColumnType("NVARCHAR(MAX)");
            });
            builder.Entity<EmailQueue>(entity =>
            {
                entity.Property(x => x.Body).HasColumnType("NVARCHAR(MAX)");
            });
            builder.Entity<IdentityRole>(entity =>
            {
                entity.ToTable(name: "Role");
            });
            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRoles");
                //in case you chagned the TKey type
                //  entity.HasKey(key => new { key.UserId, key.RoleId });
            });
            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaims");
            });

            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogins");
                //in case you chagned the TKey type
                //  entity.HasKey(key => new { key.ProviderKey, key.LoginProvider });       
            });

            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaims");

            });

            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserTokens");
                //in case you chagned the TKey type
                // entity.HasKey(key => new { key.UserId, key.LoginProvider, key.Name });

            });
            builder.Entity<Setting>(entity =>
            {
                entity.HasIndex(x => x.Key).IsUnique()
                        .HasName("KeyIndex")
                        .HasFilter("[Key] IS NOT NULL");

            });
            builder.Entity<Order>(entity =>
            {
                entity.Property(x=>x.OrderAppId)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasComputedColumnSql("Upper(('Ord'+'-'+right(CONVERT(nvarchar(26), CreateDate,112),4)+'-'+left(Reference,1)+RIGHT(Reference,1)+'-'+right(replicate('0',(9))+CONVERT([varchar],Id),(9)))) PERSISTED");

            });

            //var user = builder.Entity<User>().ToTable("Users");
            //user.Property(p => p.Id).HasColumnName("Id");
            //user.Property(p => p.RecId).ValueGeneratedOnAdd();
            //user.HasMany(u => u.).WithRequired().HasForeignKey(ur => ur.UserId);
            //user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            //user.HasMany(u => u.Logins).WithRequired().HasForeignKey(ul => ul.UserId);
            //user.Property(u => u.UserName).IsRequired();
            //builder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles").HasKey(r => new { r.UserId, r.RoleId });
            //builder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogins").HasKey(l => new { l.UserId, l.LoginProvider, l.ProviderKey });
            //builder.Entity<IdentityUserClaim<Guid>>().ToTable("UserClaims");
            //builder.Entity<IdentityRole>().ToTable("Roles");

        }
        public new virtual DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }
        public void Detach<TEntity>(TEntity entity) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> EntityFromSql<TEntity>(string sql, params object[] parameters) where TEntity : class
        {
            return this.Query<TEntity>().FromSql(sql, parameters);
        }
        public IQueryable<TEntity> EntityFromSql<TEntity>(string sql) where TEntity : class
        {
            return this.Query<TEntity>().FromSql(sql);
        }

        public string GenerateCreateScript()
        {
            throw new NotImplementedException();
        }

        public IQueryable<TQuery> QueryFromSql<TQuery>(string sql, params object[] parameters) where TQuery : class
        {
            throw new NotImplementedException();
        }
    }
    public partial class SCLDContext
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<UserCertificate> UserCertificates { get; set; }
        public DbSet<UserLanguage> UserLanguages { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }
        public DbSet<UserSocial> UserSocials { get; set; }
        public DbSet<UserEducation> UserEducations { get; set; }
        public DbSet<UserExperience> UserExperiences { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<TrainingOrganization> TrainingOrganizations { get; set; }
        public DbSet<TrainingTrainer> TrainingTrainers { get; set; }
        public DbSet<TrainingTicket> TrainingTickets { get; set; }
        public DbSet<TrainingPromotion> TrainingPromotions { get; set; }
        public DbSet<TrainingRequest> TrainingRequests { get; set; }
        public DbSet<TrainingImage> TrainingImages { get; set; }
        public DbSet<TrainingSetting> TrainingSettings { get; set; }
        public DbSet<About> Abouts { get; set; }
        public DbSet<MasterData> MasterDatas { get; set; }
        public DbSet<BankDetail> BankDetails { get; set; }
        public DbSet<Trainer> Trainers { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<EmailQueue> EmailQueues { get; set; }
        public DbSet<EmailQueueAttachment> EmailQueueAttachments { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<TrainingReview> Reviews { get; set; }
        public DbSet<TrainingInterested> TrainingInteresteds { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<UserFollow> UserFollows { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<TrainingOrder> TrainingOrders { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<TicketPaymentDetail> TicketPaymentDetails { get; set; }
        public DbSet<UserPayment> UserPayments { get; set; }
        public DbSet<UserPass> UserPass { get; set; }
    }
}
