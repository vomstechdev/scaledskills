﻿CREATE FUNCTION [dbo].[Fun_TrainingMode](@ModeID int)  
RETURNS nvarchar(100)
AS   
-- Returns the stock level for the product.  
BEGIN  
    DECLARE @ret nvarchar(100);  
     select  @ret  = case(@ModeID) when 1 Then 'Online' else 'Offline' end
    RETURN @ret;  
END;