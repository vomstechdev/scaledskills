﻿

CREATE PROCEDURE [dbo].[TicketPaymentDetail_Proc]



@TicketId BIGINT = NULL



AS







BEGIN



IF NOT EXISTS( select 1 from TicketPaymentDetails where TicketId  = @TicketId)



BEGIN



select CAST(0 as bigint) Id, CAST(0 as bigint) TicketId,
CAST(md.Id as int) FeeType ,md.[Name] FeeText, ps.Servicefee Amount,CAST(1 as bit) IsPercentage, cast(2 as int) ModeType , cast(780 as bigint) UserId
 from PaymentSetting ps
join MasterDatas md on md.Code = 'Servicefee' and md.[key] ='PAYMENT_FEE_TYPE'
UNION
select CAST(0 as bigint) Id, CAST(0 as bigint) TicketId,
CAST(md.Id as int) FeeType ,md.[Name] FeeText, ps.Gateway Amount,CAST(1 as bit) IsPercentage, cast(2 as int) ModeType , cast(780 as bigint) UserId from PaymentSetting ps
join MasterDatas md on md.Code = 'Gateway' and md.[key] ='PAYMENT_FEE_TYPE'

UNION
select CAST(0 as bigint) Id, CAST(0 as bigint) TicketId,
CAST(md.Id as int) FeeType ,md.[Name] FeeText, ps.Affiliate Amount,CAST(1 as bit) 


IsPercentage, cast(1 as int) ModeType , cast(780 as bigint) UserId from PaymentSetting ps
join MasterDatas md on md.Code = 'Affiliate' and md.[key] ='PAYMENT_FEE_TYPE' 



UNION
select CAST(0 as bigint) Id, CAST(0 as bigint) TicketId,
CAST(md.Id as int) FeeType ,md.[Name] FeeText, ps.GST Amount,CAST(1 as bit) IsPercentage, cast(2 as int) ModeType , cast(780 as bigint) UserId  from PaymentSetting ps
join MasterDatas md on md


.Code = 'GST' and md.[key] ='PAYMENT_FEE_TYPE'



END



ELSE 



BEGIN



select CAST(ts.Id as bigint) Id, CAST(ts.TicketId as bigint) TicketId,
CAST(ts.FeeType as int) FeeType ,md.[Name] FeeText, ts.Amount Amount,ts.IsPercentage IsPercentage, ts.ModeType ModeType , cast(1 as bigint) UserId
 from TicketPaymentDetails ts
join MasterDatas md on md.Id = ts.FeeType and md.[key] ='PAYMENT_FEE_TYPE' 
where TicketId  = @TicketId



END



END











--TicketPaymentDetail_Proc	