﻿CREATE VIEW [dbo].[Trainings_view]        
AS         
select distinct t.Id TrainingId, t.[Name],t.[Description], t.StartDate,t.EndDate,t.HostedBy,t.IsRecurrence,t.OnlineLocation,      
t.[Url], t.[TrainingStatus] ,    
(case(t.ModeType) when 2 Then 'Online' else 'Offline' end) Mode,t.UserId,       
U.FirstName + ' '+U.LastName  UserName,tkt.PaymentCharge Amount,timg.Header,timg.[Card], SUM(case when TI.TrainingId is not null then 1 else 0 end) OVER (PARTITION BY TI.TrainingId) AS InterestCount ,       
a.Address1,a.Address2,a.City, mconty.[Name] as CountryName,mstate.[Name] as StateName , t.IsOnlineDetailsVisible ,t.[Name] as Keywords    
 from Trainings t        
INNER JOIN Users U      
on t.USERID = u.RecId      
LEFT JOIN         
TrainingTickets tkt on t.id = tkt.TrainingId and tkt.TicketType = 1        
LEFT JOIN         
TrainingInteresteds TI on t.id = TI.TrainingId and TI.IsInterested = 1        
LEFT JOIN         
Addresses a on  t.AddressId = a.id        
LEFT JOIN        
MasterDatas mconty on  a.CountryId = mconty.Id        
LEFT JOIN       
MasterDatas mstate on  a.StateId = mstate.Id        
LEFT JOIN         
Abouts ab on  t.AboutId = ab.id     
LEFT JOIN         
TrainingImages timg on  timg.TrainingId = t.Id