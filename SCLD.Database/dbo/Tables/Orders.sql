﻿CREATE TABLE [dbo].[Orders] (
    [Id]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [Status]       BIT             NOT NULL,
    [CreateDate]   DATETIME2 (7)   NOT NULL,
    [ModifiedDate] DATETIME2 (7)   NULL,
    [UserId]       BIGINT          NOT NULL,
    [OrderAppId]   AS              (upper((((((('Ord'+'-')+right(CONVERT([nvarchar](26),[CreateDate],(112)),(4)))+'-')+left([Reference],(1)))+right([Reference],(1)))+'-')+right(replicate('0',(9))+CONVERT([varchar],[Id]),(9)))) PERSISTED,
    [Reference]    NVARCHAR (MAX)  NULL,
    [OrderStatus]  INT             NOT NULL,
    [Address]      NVARCHAR (MAX)  NULL,
    [Description]  NVARCHAR (MAX)  NULL,
    [TotalAmount]  DECIMAL (18, 2) NOT NULL,
    [ReferralCode] NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([Id] ASC)
);

