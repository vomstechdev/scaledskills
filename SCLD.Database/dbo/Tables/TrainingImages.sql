﻿CREATE TABLE [dbo].[TrainingImages] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [TrainingId]   BIGINT         NOT NULL,
    [Header]       NVARCHAR (MAX) NULL,
    [Card]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TrainingImages] PRIMARY KEY CLUSTERED ([Id] ASC)
);

