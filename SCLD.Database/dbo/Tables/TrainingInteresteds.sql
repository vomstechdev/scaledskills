﻿CREATE TABLE [dbo].[TrainingInteresteds] (
    [Id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]       BIT           NOT NULL,
    [CreateDate]   DATETIME2 (7) NOT NULL,
    [ModifiedDate] DATETIME2 (7) NULL,
    [UserId]       BIGINT        NOT NULL,
    [TrainingId]   BIGINT        NOT NULL,
    [IsInterested] BIT           NOT NULL,
    CONSTRAINT [PK_TrainingInteresteds] PRIMARY KEY CLUSTERED ([Id] ASC)
);



