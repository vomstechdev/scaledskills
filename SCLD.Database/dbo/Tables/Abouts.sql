﻿CREATE TABLE [dbo].[Abouts] (
    [Id]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]             BIT            NOT NULL,
    [CreateDate]         DATETIME2 (7)  NOT NULL,
    [ModifiedDate]       DATETIME2 (7)  NULL,
    [Name]               NVARCHAR (MAX) NULL,
    [AboutText]          NVARCHAR (MAX) NULL,
    [CoursesOfferedText] NVARCHAR (MAX) NULL,
    [EntityId]           BIGINT         NOT NULL,
    [EntityType]         NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Abouts] PRIMARY KEY CLUSTERED ([Id] ASC)
);

