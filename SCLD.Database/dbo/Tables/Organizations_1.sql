﻿CREATE TABLE [dbo].[Organizations] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [OwnerName]    NVARCHAR (MAX) NULL,
    [ProfileUrl]   NVARCHAR (450) NULL,
    [Email]        NVARCHAR (MAX) NULL,
    [PhoneNumber]  NVARCHAR (MAX) NULL,
    [GST]          NVARCHAR (MAX) NULL,
    [PanNumber]    NVARCHAR (MAX) NULL,
    [AddressId]    BIGINT         NULL,
    [AboutId]      BIGINT         NULL,
    [BankDetailId] BIGINT         NULL,
    [SocialId]     BIGINT         NULL,
    [Image]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Organizations] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [OrganizationProfileUrlIndex]
    ON [dbo].[Organizations]([ProfileUrl] ASC) WHERE ([ProfileUrl] IS NOT NULL);

