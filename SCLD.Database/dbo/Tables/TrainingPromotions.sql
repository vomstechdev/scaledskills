﻿CREATE TABLE [dbo].[TrainingPromotions] (
    [Id]                        BIGINT          IDENTITY (1, 1) NOT NULL,
    [Status]                    BIT             NOT NULL,
    [CreateDate]                DATETIME2 (7)   NOT NULL,
    [ModifiedDate]              DATETIME2 (7)   NULL,
    [TrainingId]                BIGINT          NOT NULL,
    [IsAdvertisement]           BIT             NOT NULL,
    [AdvertisementRate]         DECIMAL (18, 2) NOT NULL,
    [IsDisplayBannerHomePage]   BIT             NOT NULL,
    [DisplayBannerHomePageRate] DECIMAL (18, 2) NOT NULL,
    [IsDisplayInCardHomePage]   BIT             NOT NULL,
    [DisplayCardHomePageRate]   DECIMAL (18, 2) NOT NULL,
    [IsAffliliate]              BIT             NOT NULL,
    [AffliliateType]            INT             NOT NULL,
    [AffliliateRate]            DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_TrainingPromotions] PRIMARY KEY CLUSTERED ([Id] ASC)
);

