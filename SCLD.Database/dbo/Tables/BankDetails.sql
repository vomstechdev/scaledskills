﻿CREATE TABLE [dbo].[BankDetails] (
    [Id]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]        BIT            NOT NULL,
    [CreateDate]    DATETIME2 (7)  NOT NULL,
    [ModifiedDate]  DATETIME2 (7)  NULL,
    [UserId]        BIGINT         NOT NULL,
    [Name]          NVARCHAR (MAX) NULL,
    [AccountNumber] NVARCHAR (MAX) NULL,
    [AccountType]   INT            NOT NULL,
    [BankName]      NVARCHAR (MAX) NULL,
    [BranchName]    NVARCHAR (MAX) NULL,
    [IFSCCode]      NVARCHAR (MAX) NULL,
    [GSTNum]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_BankDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);

