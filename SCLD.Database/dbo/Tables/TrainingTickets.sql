﻿CREATE TABLE [dbo].[TrainingTickets] (
    [Id]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [Status]        BIT             NOT NULL,
    [CreateDate]    DATETIME2 (7)   NOT NULL,
    [ModifiedDate]  DATETIME2 (7)   NULL,
    [Name]          NVARCHAR (MAX)  NULL,
    [TrainingId]    BIGINT          NOT NULL,
    [QTY]           INT             NOT NULL,
    [AvailableQty]  INT             NOT NULL,
    [MinBooking]    INT             NOT NULL,
    [MaxBooking]    INT             NULL,
    [TicketType]    INT             NOT NULL,
    [PaymentCharge] DECIMAL (18, 2) NOT NULL,
    [StartDate]     DATETIME2 (7)   NOT NULL,
    [EndDate]       DATETIME2 (7)   NOT NULL,
    [Description]   NVARCHAR (MAX)  NULL,
    [MSGForAtendee] NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_TrainingTickets] PRIMARY KEY CLUSTERED ([Id] ASC)
);



