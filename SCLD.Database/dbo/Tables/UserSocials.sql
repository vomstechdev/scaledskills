﻿CREATE TABLE [dbo].[UserSocials] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [LinkedInUrls] NVARCHAR (MAX) NULL,
    [FacebookUrls] NVARCHAR (MAX) NULL,
    [TwitterUrls]  NVARCHAR (MAX) NULL,
    [WebsiteUrls]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserSocials] PRIMARY KEY CLUSTERED ([Id] ASC)
);



