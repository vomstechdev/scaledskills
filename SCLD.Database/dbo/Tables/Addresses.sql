﻿CREATE TABLE [dbo].[Addresses] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [Address1]     NVARCHAR (MAX) NULL,
    [Address2]     NVARCHAR (MAX) NULL,
    [Address3]     NVARCHAR (MAX) NULL,
    [City]         NVARCHAR (MAX) NULL,
    [ZipCode]      NVARCHAR (MAX) NULL,
    [CountryId]    INT            NULL,
    [StateId]      INT            NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED ([Id] ASC)
);

