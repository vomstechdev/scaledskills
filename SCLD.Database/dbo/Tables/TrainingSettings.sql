﻿CREATE TABLE [dbo].[TrainingSettings] (
    [Id]                                  BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]                              BIT           NOT NULL,
    [CreateDate]                          DATETIME2 (7) NOT NULL,
    [ModifiedDate]                        DATETIME2 (7) NULL,
    [TrainingId]                          BIGINT        NOT NULL,
    [IsPublicRepeat]                      BIT           NOT NULL,
    [IsPublicCustomQuestion]              BIT           NOT NULL,
    [IsPublicRemainingTicket]             BIT           NOT NULL,
    [IsPublicSetReminder]                 BIT           NOT NULL,
    [IsPublicAffiliatePromoterLink]       BIT           NOT NULL,
    [IsPublicTrainingId]                  BIT           NOT NULL,
    [IsPublicPageViews]                   BIT           NOT NULL,
    [IsPublicInterestedUser]              BIT           NOT NULL,
    [IsPublicRegisteredUser]              BIT           NOT NULL,
    [IsPublicAddToCalendar]               BIT           NOT NULL,
    [IsPublicXSpotsLeft]                  BIT           NOT NULL,
    [IsPublicComments]                    BIT           NOT NULL,
    [IsPublicFeedback]                    BIT           NOT NULL,
    [IsPublicAboutOrganizer]              BIT           NOT NULL,
    [IsPublicMoreEventsFromSameOrganizer] BIT           NOT NULL,
    CONSTRAINT [PK_TrainingSettings] PRIMARY KEY CLUSTERED ([Id] ASC)
);

