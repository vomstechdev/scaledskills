﻿CREATE TABLE [dbo].[TrainingOrders] (
    [Id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]       BIT           NOT NULL,
    [CreateDate]   DATETIME2 (7) NOT NULL,
    [ModifiedDate] DATETIME2 (7) NULL,
    [UserId]       BIGINT        NOT NULL,
    [TrainingId]   BIGINT        NOT NULL,
    [OrderId]      BIGINT        NOT NULL,
    CONSTRAINT [PK_TrainingOrders] PRIMARY KEY CLUSTERED ([Id] ASC)
);

