﻿CREATE TABLE [dbo].[Contacts] (
    [Id]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]        BIT            NOT NULL,
    [CreateDate]    DATETIME2 (7)  NOT NULL,
    [ModifiedDate]  DATETIME2 (7)  NULL,
    [FirstName]     NVARCHAR (MAX) NULL,
    [LastName]      NVARCHAR (MAX) NULL,
    [UserId]        BIGINT         NOT NULL,
    [Email]         NVARCHAR (MAX) NULL,
    [PhoneNumber]   NVARCHAR (MAX) NULL,
    [Msg]           NVARCHAR (MAX) NULL,
    [ContactStatus] INT            NOT NULL,
    [Remarks]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([Id] ASC)
);

