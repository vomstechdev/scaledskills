﻿CREATE TABLE [dbo].[OrderItems] (
    [Id]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [Status]       BIT             NOT NULL,
    [CreateDate]   DATETIME2 (7)   NOT NULL,
    [ModifiedDate] DATETIME2 (7)   NULL,
    [OrderId]      BIGINT          NOT NULL,
    [ItemId]       BIGINT          NOT NULL,
    [Reference]    NVARCHAR (MAX)  NULL,
    [Qty]          INT             NOT NULL,
    [UnitPrice]    DECIMAL (18, 2) NOT NULL,
    [TotalPrice]   DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED ([Id] ASC)
);

