﻿CREATE TABLE [dbo].[UserLanguages] (
    [Id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]       BIT           NOT NULL,
    [CreateDate]   DATETIME2 (7) NOT NULL,
    [ModifiedDate] DATETIME2 (7) NULL,
    [UserId]       BIGINT        NOT NULL,
    [LanguageId]   INT           NOT NULL,
    [Proficiency]  INT           NOT NULL,
    CONSTRAINT [PK_UserLanguages] PRIMARY KEY CLUSTERED ([Id] ASC)
);

