﻿CREATE TABLE [dbo].[Settings] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [Key]          NVARCHAR (450) NULL,
    [Values]       NVARCHAR (MAX) NULL,
    [TypeOfValue]  INT            NOT NULL,
    [Parent]       INT            NOT NULL,
    CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [KeyIndex]
    ON [dbo].[Settings]([Key] ASC) WHERE ([Key] IS NOT NULL);

