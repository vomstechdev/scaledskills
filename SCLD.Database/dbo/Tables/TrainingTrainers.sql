﻿CREATE TABLE [dbo].[TrainingTrainers] (
    [Id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]       BIT           NOT NULL,
    [CreateDate]   DATETIME2 (7) NOT NULL,
    [ModifiedDate] DATETIME2 (7) NULL,
    [TrainingId]   BIGINT        NOT NULL,
    [TrainerId]    BIGINT        NOT NULL,
    [IsDelete]     BIT           NOT NULL,
    CONSTRAINT [PK_TrainingTrainers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

