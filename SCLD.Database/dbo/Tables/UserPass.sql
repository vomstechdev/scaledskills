﻿CREATE TABLE [dbo].[UserPass] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [Password]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserPass] PRIMARY KEY CLUSTERED ([Id] ASC)
);

