﻿CREATE TABLE [dbo].[Promotions] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [EntityId]     BIGINT         NOT NULL,
    [EntityType]   INT            NOT NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [Order]        INT            NOT NULL,
    [StartDate]    DATETIME2 (7)  NOT NULL,
    [EndDate]      DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_Promotions] PRIMARY KEY CLUSTERED ([Id] ASC)
);



