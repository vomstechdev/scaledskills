﻿CREATE TABLE [dbo].[Trainings] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]                 BIT            NOT NULL,
    [CreateDate]             DATETIME2 (7)  NOT NULL,
    [ModifiedDate]           DATETIME2 (7)  NULL,
    [UserId]                 BIGINT         NOT NULL,
    [Name]                   NVARCHAR (MAX) NULL,
    [Url]                    NVARCHAR (450) NULL,
    [HostedBy]               INT            NOT NULL,
    [Description]            NVARCHAR (MAX) NULL,
    [StartDate]              DATETIME2 (7)  NOT NULL,
    [EndDate]                DATETIME2 (7)  NOT NULL,
    [TimeZone]               INT            NOT NULL,
    [TrainingStatus]         INT            NOT NULL,
    [ModeType]               INT            NOT NULL,
    [AddressId]              BIGINT         NULL,
    [AboutId]                BIGINT         NULL,
    [OnlineLocation]         NVARCHAR (MAX) NULL,
    [LocationDetail]         NVARCHAR (MAX) NULL,
    [IsOnlineDetailsVisible] BIT            NOT NULL,
    [IsRecurrence]           BIT            NOT NULL,
    [IsUserAccept]           BIT            NOT NULL,
    [IsPromotion]            BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Trainings] PRIMARY KEY CLUSTERED ([Id] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [TrainingUrlIndex]
    ON [dbo].[Trainings]([Url] ASC) WHERE ([Url] IS NOT NULL);

