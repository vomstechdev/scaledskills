﻿CREATE TABLE [dbo].[UserCertificates] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [Authority]    NVARCHAR (MAX) NULL,
    [From]         DATETIME2 (7)  NOT NULL,
    [To]           DATETIME2 (7)  NULL,
    [IsExpired]    BIT            NOT NULL,
    [Image]        NVARCHAR (MAX) NULL,
    [Url]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserCertificates] PRIMARY KEY CLUSTERED ([Id] ASC)
);



