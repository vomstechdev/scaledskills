﻿CREATE TABLE [dbo].[Reviews] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]          BIT            NOT NULL,
    [CreateDate]      DATETIME2 (7)  NOT NULL,
    [ModifiedDate]    DATETIME2 (7)  NULL,
    [UserId]          BIGINT         NOT NULL,
    [TrainingId]      BIGINT         NOT NULL,
    [RateOfSatisfied] INT            NOT NULL,
    [LikeAbout]       NVARCHAR (MAX) NULL,
    [ImproveAbout]    NVARCHAR (MAX) NULL,
    [NeedInFeature]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Reviews] PRIMARY KEY CLUSTERED ([Id] ASC)
);



