﻿CREATE TABLE [dbo].[UserEducations] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [FieldOfStudy] NVARCHAR (MAX) NULL,
    [College]      NVARCHAR (MAX) NULL,
    [Grade]        NVARCHAR (MAX) NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [Image]        NVARCHAR (MAX) NULL,
    [Url]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserEducations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

