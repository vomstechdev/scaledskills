﻿CREATE TABLE [dbo].[EmailQueues] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]           BIT            NOT NULL,
    [CreateDate]       DATETIME2 (7)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7)  NULL,
    [UserId]           BIGINT         NOT NULL,
    [ToEmail]          NVARCHAR (MAX) NULL,
    [CCEmail]          NVARCHAR (MAX) NULL,
    [BCCEmail]         NVARCHAR (MAX) NULL,
    [Subject]          NVARCHAR (MAX) NULL,
    [Body]             NVARCHAR (MAX) NULL,
    [QueueTime]        DATETIME2 (7)  NULL,
    [EmailStatus]      INT            NULL,
    [SentTime]         DATETIME2 (7)  NULL,
    [SmtpServer]       NVARCHAR (MAX) NULL,
    [SmtpPort]         INT            NULL,
    [FromAddress]      NVARCHAR (MAX) NULL,
    [UserName]         NVARCHAR (MAX) NULL,
    [Retries]          INT            NOT NULL,
    [Password]         NVARCHAR (MAX) NULL,
    [DisplayName]      NVARCHAR (MAX) NULL,
    [Seperator]        NVARCHAR (MAX) NULL,
    [ErrorDescription] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EmailQueues] PRIMARY KEY CLUSTERED ([Id] ASC)
);

