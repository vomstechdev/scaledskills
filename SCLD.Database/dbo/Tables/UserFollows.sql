﻿CREATE TABLE [dbo].[UserFollows] (
    [Id]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]       BIT           NOT NULL,
    [CreateDate]   DATETIME2 (7) NOT NULL,
    [ModifiedDate] DATETIME2 (7) NULL,
    [UserId]       BIGINT        NOT NULL,
    [Type]         INT           NOT NULL,
    [TypeId]       BIGINT        NOT NULL,
    CONSTRAINT [PK_UserFollows] PRIMARY KEY CLUSTERED ([Id] ASC)
);

