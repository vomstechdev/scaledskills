﻿CREATE TABLE [dbo].[Trainers] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [ProfileUrl]   NVARCHAR (450) NULL,
    [Email]        NVARCHAR (MAX) NULL,
    [PhoneNumber]  NVARCHAR (MAX) NULL,
    [GST]          NVARCHAR (MAX) NULL,
    [PanNumber]    NVARCHAR (MAX) NULL,
    [AddressId]    BIGINT         NULL,
    [AboutId]      BIGINT         NULL,
    [BankDetailId] BIGINT         NULL,
    [SocialId]     BIGINT         NULL,
    CONSTRAINT [PK_Trainers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [TrainerProfileUrlIndex]
    ON [dbo].[Trainers]([ProfileUrl] ASC) WHERE ([ProfileUrl] IS NOT NULL);

