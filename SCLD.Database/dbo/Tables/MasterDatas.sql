﻿CREATE TABLE [dbo].[MasterDatas] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [Key]          NVARCHAR (MAX) NULL,
    [Code]         NVARCHAR (MAX) NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [ParentId]     BIGINT         NULL,
    CONSTRAINT [PK_MasterDatas] PRIMARY KEY CLUSTERED ([Id] ASC)
);



