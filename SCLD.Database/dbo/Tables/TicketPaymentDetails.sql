﻿CREATE TABLE [dbo].[TicketPaymentDetails] (
    [Id]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [Status]       BIT             NOT NULL,
    [CreateDate]   DATETIME2 (7)   NOT NULL,
    [ModifiedDate] DATETIME2 (7)   NULL,
    [TicketId]     BIGINT          NOT NULL,
    [FeeType]      INT             NOT NULL,
    [Amount]       DECIMAL (18, 2) NOT NULL,
    [IsPercentage] BIT             NOT NULL,
    [ModeType]     INT             NOT NULL,
    CONSTRAINT [PK_TicketPaymentDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);

