﻿CREATE TABLE [dbo].[UserExperiences] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [UserId]       BIGINT         NOT NULL,
    [Position]     NVARCHAR (MAX) NULL,
    [Company]      NVARCHAR (MAX) NULL,
    [Location]     NVARCHAR (MAX) NULL,
    [Headline]     NVARCHAR (MAX) NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [Image]        NVARCHAR (MAX) NULL,
    [Url]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserExperiences] PRIMARY KEY CLUSTERED ([Id] ASC)
);

