﻿CREATE TABLE [dbo].[UserSettings] (
    [Id]                      BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]                  BIT            NOT NULL,
    [CreateDate]              DATETIME2 (7)  NOT NULL,
    [ModifiedDate]            DATETIME2 (7)  NULL,
    [UserId]                  BIGINT         NOT NULL,
    [IsInterAffiliatePartner] BIT            NOT NULL,
    [ReferralID]              NVARCHAR (MAX) NULL,
    [ProfileUrl]              NVARCHAR (MAX) NULL,
    [IsPublishUrl]            BIT            NOT NULL,
    [IsPublicCertifications]  BIT            NOT NULL,
    [IsPublicSocialMedia]     BIT            NOT NULL,
    [IsDefaultLanguage]       BIT            NOT NULL,
    [AboutText]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_UserSettings] PRIMARY KEY CLUSTERED ([Id] ASC)
);

