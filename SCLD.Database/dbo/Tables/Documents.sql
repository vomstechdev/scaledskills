﻿CREATE TABLE [dbo].[Documents] (
    [Id]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [Status]       BIT              NOT NULL,
    [CreateDate]   DATETIME2 (7)    NOT NULL,
    [ModifiedDate] DATETIME2 (7)    NULL,
    [DocumentId]   UNIQUEIDENTIFIER NOT NULL,
    [Group]        UNIQUEIDENTIFIER NOT NULL,
    [FileName]     NVARCHAR (MAX)   NULL,
    [Ext]          NVARCHAR (MAX)   NULL,
    [Caption]      NVARCHAR (MAX)   NULL,
    [Size]         INT              NULL,
    [Type]         NVARCHAR (MAX)   NULL,
    [Filetype]     NVARCHAR (MAX)   NULL,
    [Directory]    NVARCHAR (MAX)   NULL,
    [Order]        INT              NULL,
    [IsDelete]     BIT              NULL,
    CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED ([Id] ASC)
);

