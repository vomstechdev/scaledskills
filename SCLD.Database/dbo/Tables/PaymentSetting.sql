﻿CREATE TABLE [dbo].[PaymentSetting] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Servicefee] DECIMAL (8, 2) NULL,
    [GST]        DECIMAL (8, 2) NULL,
    [Gateway]    DECIMAL (8, 2) NULL,
    [Affiliate]  DECIMAL (8, 2) NULL
);

