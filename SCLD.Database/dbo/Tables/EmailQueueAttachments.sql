﻿CREATE TABLE [dbo].[EmailQueueAttachments] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]       BIT            NOT NULL,
    [CreateDate]   DATETIME2 (7)  NOT NULL,
    [ModifiedDate] DATETIME2 (7)  NULL,
    [FileName]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EmailQueueAttachments] PRIMARY KEY CLUSTERED ([Id] ASC)
);

