﻿CREATE TABLE [dbo].[TrainingOrganizations] (
    [Id]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [Status]         BIT           NOT NULL,
    [CreateDate]     DATETIME2 (7) NOT NULL,
    [ModifiedDate]   DATETIME2 (7) NULL,
    [TrainingId]     BIGINT        NOT NULL,
    [OrganizationId] BIGINT        NOT NULL,
    [IsDelete]       BIT           NOT NULL,
    CONSTRAINT [PK_TrainingOrganizations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

