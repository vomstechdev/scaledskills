﻿CREATE TABLE [dbo].[TrainingRequests] (
    [Id]                  BIGINT         IDENTITY (1, 1) NOT NULL,
    [Status]              BIT            NOT NULL,
    [CreateDate]          DATETIME2 (7)  NOT NULL,
    [ModifiedDate]        DATETIME2 (7)  NULL,
    [UserId]              BIGINT         NOT NULL,
    [NeedType]            INT            NOT NULL,
    [TrainingType]        INT            NOT NULL,
    [CompanyName]         NVARCHAR (MAX) NULL,
    [NumberOfParticipant] INT            NOT NULL,
    [Summery]             NVARCHAR (MAX) NULL,
    [Description]         NVARCHAR (MAX) NULL,
    [Budget]              NVARCHAR (MAX) NULL,
    [Keywords]            NVARCHAR (MAX) NULL,
    [City]                NVARCHAR (MAX) NULL,
    [ZipCode]             NVARCHAR (MAX) NULL,
    [CountryId]           INT            NULL,
    [StateId]             INT            NULL,
    CONSTRAINT [PK_TrainingRequests] PRIMARY KEY CLUSTERED ([Id] ASC)
);

