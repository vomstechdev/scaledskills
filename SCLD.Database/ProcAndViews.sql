﻿
/****** Object:  UserDefinedFunction [dbo].[Fun_TrainingMode]    Script Date: 31-Dec-19 1:28:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fun_TrainingMode](@ModeID int)  
RETURNS nvarchar(100)
AS   
-- Returns the stock level for the product.  
BEGIN  
    DECLARE @ret nvarchar(100);  
     select  @ret  = case(@ModeID) when 1 Then 'Online' else 'Offline' end
    RETURN @ret;  
END; 
GO
/****** Object:  View [dbo].[Trainings_view]    Script Date: 31-Dec-19 1:28:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Trainings_view]        
AS         
select distinct t.Id TrainingId, t.[Name],t.[Description], t.StartDate,t.EndDate,t.HostedBy,t.IsRecurrence,t.OnlineLocation,      
t.[Url],      
(case(t.ModeType) when 2 Then 'Online' else 'Offline' end) Mode,t.UserId,       
U.FirstName + ' '+U.LastName  UserName,tkt.PaymentCharge Amount,timg.Header,timg.[Card], SUM(case when TI.TrainingId is not null then 1 else 0 end) OVER (PARTITION BY TI.TrainingId) AS InterestCount ,       
a.Address1,a.Address2,a.City, mconty.[Name] as CountryName,mstate.[Name] as StateName , t.IsOnlineDetailsVisible ,t.[Name] as Keywords    
 from Trainings t        
INNER JOIN Users U      
on t.USERID = u.RecId      
LEFT JOIN         
TrainingTickets tkt on t.id = tkt.TrainingId and tkt.TicketType = 1        
LEFT JOIN         
TrainingInteresteds TI on t.id = TI.TrainingId and TI.IsInterested = 1        
LEFT JOIN         
Addresses a on  t.AddressId = a.id        
LEFT JOIN        
MasterDatas mconty on  a.CountryId = mconty.Id        
LEFT JOIN       
MasterDatas mstate on  a.StateId = mstate.Id        
LEFT JOIN         
Abouts ab on  t.AboutId = ab.id     
LEFT JOIN         
TrainingImages timg on  timg.TrainingId = t.Id  

GO
/****** Object:  View [dbo].[CardTraining_view]    Script Date: 31-Dec-19 1:28:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[CardTraining_view]
AS
select t.TrainingId, t.Name, t.StartDate,t.EndDate,t.OnlineLocation,
t.Mode,t.UserId,t.Amount,t.[Url],
t.UserName, t.InterestCount , 
t.Address1,t.Address2,t.City,t.CountryName,t. StateName,t.[Card] as [Image]    from Trainings_view t
GO
/****** Object:  StoredProcedure [dbo].[GetMasterQ]    Script Date: 31-Dec-19 1:28:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMasterQ]  
as  
SELECT RecId UserId,Email 'Name' FROM Users
SELECT RecId UserId,Email 'Name' FROM Users
GO
/****** Object:  StoredProcedure [dbo].[GetUser]    Script Date: 31-Dec-19 1:28:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetUser]
AS
select * from UserSettings
GO
