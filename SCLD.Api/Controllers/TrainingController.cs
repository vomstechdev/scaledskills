﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("api/Training")]
    [ApiController]
    public class TrainingController : UserBaseController
    {
        readonly ITrainingRequestService trainingRequestService;
        readonly ITicketService ticketService;
        readonly ITrainingService trainingService;

        public TrainingController(ITicketService _ticketService, ITrainingRequestService _trainingRequestService, ITrainingService _trainingService)
        {
            trainingRequestService = _trainingRequestService;
            ticketService = _ticketService;
            trainingService = _trainingService;

        }
        [HttpGet(Routs.Training.TrainingRequest)]
        public IActionResult GetTrainingRequests()
        {
            var userId = UserId;
            var results = trainingRequestService.GetTrainingRequestByUserId(userId);
            return this.Success(results);
        }
        [HttpGet(Routs.Training.TrainingRequest + "/{id}")]
        public IActionResult GetTrainingRequestById(long id)
        {
            return this.Success(trainingRequestService.GetTrainingRequestById(id));
        }
        [HttpPost(Routs.Training.TrainingRequest)]
        public IActionResult SaveTrainingRequest(TrainingRequestModel model)
        {
            bool isInsert = model.Id <= 0;
            model.UserId = UserId;
            var id = isInsert ? trainingRequestService.InsertTrainingRequest(model) :
            trainingRequestService.UpdateTrainingRequest(model);
            return this.Success();
        }
        [HttpGet]
        public IActionResult GetTraining()
        {
            var userId = UserId;
            var results = trainingService.GetTrainingByUserId(userId).ToList();
            return this.Success(results);
        }
        [HttpGet("/{id}")]
        public IActionResult GetTrainingById(long id)
        {
            return this.Success(trainingService.GetTrainingById(id));
        }
        [HttpPost]
        public IActionResult SaveTraining(TrainingModel model)
        {
            bool isInsert = model.Id <= 0;
            model.UserId = UserId;
            if (model.HostedBy == 2 && model.OrganizationList.Any())
            {
                trainingService.SaveTrainingOrganization(model);
            }
            var id = isInsert ? trainingService.InsertTraining(model) :
            trainingService.UpdateTraining(model);
            return this.Success(id);
        }
        [HttpGet(Routs.Training.TrainingLocation)]
        public IActionResult GetLocationByTraining(long TrainingId)
        {
            var result = trainingService.GetTrainingAddress(TrainingId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingLocation)]
        public IActionResult SaveTrainingLocation(TrainingLocation location, long TrainingId)
        {
            var result = trainingService.UpdateAddress(location, TrainingId);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingAbout)]
        public IActionResult GetAboutByTraining(long TrainingId)
        {
            var result = trainingService.GetTicketAbout(TrainingId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingAbout)]
        public IActionResult SaveTrainingAbout(AboutModel location, long TrainingId)
        {
            var result = trainingService.UpdateAbout(location, TrainingId);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingImage)]
        public IActionResult GetTrainingImageByTraining(long TrainingId)
        {
            var result = trainingService.GetTrainingImage(TrainingId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingImage)]
        public IActionResult SaveTrainingImage(TrainingImageModel model, long TrainingId)
        {
            model.TrainingId = TrainingId;
            var result = trainingService.SaveTrainingImage(model);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingPromotions)]
        public IActionResult GetPromotionsByTraining(long TrainingId)
        {
            var pramo = trainingService.GetTrainingPromotion(TrainingId);
            return this.Success(pramo);
        }
        [HttpPost(Routs.Training.TrainingPromotions)]
        public IActionResult SavePromotionsLocation(TrainingPromotionModel model, long TrainingId)
        {
            model.TrainingId = TrainingId;
            trainingService.SaveTrainingPromotion(model);
            return this.Success();
        }
        [HttpGet(Routs.Training.TrainingSettings)]
        public IActionResult GetTrainingSettings(long TrainingId)
        {
            var result = trainingService.GetTrainingSetting(TrainingId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingSettings)]
        public IActionResult SaveTrainingSettings(TrainingSettingModel model,long TrainingId)
        {
            model.TrainingId = TrainingId;
            trainingService.SaveTrainingSetting(model);
            return this.Success();
        }
        [HttpGet(Routs.Training.TrainingTicket)]
        public IActionResult GetTrainingTickets(long TrainingId)
        {
            var result = trainingService.GetTrainingTicketByTicketId(TrainingId);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingTicket + "/{id}")]
        public IActionResult GetTickets(long TrainingId, long id)
        {
            var result = trainingService.GetTicketById(id);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingTicket)]
        public IActionResult SaveTrainingTicket(TrainingTicketModel model, long TrainingId)
        {
            model.TrainingId = TrainingId;
            var result = model.Id < 1 ? trainingService.InsertTrainingTicket(model) : trainingService.UpdateTrainingTicket(model);
            if(model.TicketPaymentDetails!=null && model.TicketPaymentDetails.Any())
            ticketService.SaveTicketPaymentDetail(model.TicketPaymentDetails?.Select(x=>{ x.TicketId = result; return x; }));
            return this.Success(result);
        }
        
        [HttpGet(Routs.Training.TrainingReview)]
        public IActionResult GetTrainingReviews(long TrainingId)
        {
            var result = trainingService.GetTrainingReviewByTrainingId(TrainingId);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingReview + "/{id}")]
        public IActionResult GetReviews(long TrainingId, long id)
        {
            var result = trainingService.GetReviewById(id);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingUserReview)]
        public IActionResult GetUserReviews(long TrainingId)
        {
            var result = trainingService.GetReviewByUserIdAndTrainingId(TrainingId,UserId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingReview)]
        public IActionResult SaveTrainingReview(TrainingReviewModel model, long TrainingId)
        {
            model.TrainingId = TrainingId;
            model.UserId = UserId;
            var result = model.Id < 1 ? trainingService.InsertTrainingReview(model) : trainingService.UpdateTrainingReview(model);
            return this.Success(result);
        }
        [HttpGet(Routs.Training.TrainingTrainers)]
        public IActionResult GetTrainingTrainers(long TrainingId)
        {
            var result = trainingService.GetTrainingTrainersTicket(TrainingId);
            return this.Success(result);
        }
        [HttpPost(Routs.Training.TrainingTrainers)]
        public IActionResult SaveTrainingTrainers(long trainnerId, long TrainingId)
        {
            trainingService.AddTrainingTrainer(trainnerId, TrainingId);
            return this.Success();
        }
        [HttpDelete(Routs.Training.TrainingTrainers)]
        public IActionResult DeleteTrainingTrainers(long trainnerId, long TrainingId)
        {
            trainingService.DeleteTrainingTrainer(trainnerId, TrainingId);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.ValidateUrl)]
        public IActionResult ValidateUrl(string url)
        {
            return this.Success(!trainingService.ValidateUrl(url, this.UserId));
        }
        [HttpPost(Routs.Training.TrainingInterest)]
        public IActionResult SetInterest(long TrainingId, [FromBody]TrainingIsInterestModel InterestModel)
        {
           var countInterest =  trainingService.SetTrainingInterast(new Core.Models.DataModels.TrainingInterested {
                IsInterested = InterestModel.IsInterest,
                TrainingId = TrainingId,
                UserId  = this.UserId
            });
            return this.Success(countInterest);
        }
        [HttpPost(Routs.Training.TrainingJoin)]
        public IActionResult JoinTraining(long TrainingId)
        {
            return SaveTrainingTrainers(this.UserId, TrainingId);
        }
        [HttpPost(Routs.Training.TrainingPublish)]
        public IActionResult PublishTraining(PublishTrainingModel publishTrainingModel)
        {
            publishTrainingModel.UserId = UserId;
            trainingService.PublishTrainingAsync(publishTrainingModel);
            return this.Success();
        }

    }
}