﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Models.Enums;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
	public class GlobalController : BaseController
	{
		readonly IMasterService masterService;
		public GlobalController(IMasterService _masterService)
		{
			masterService = _masterService;
		}
		[HttpGet(Routs.Global.Country)]
		public IActionResult GetCountry()
		{
			return this.Success(masterService.GetMasterData(nameof(MasterKey.COUN)));
		}
		[HttpGet(Routs.Global.City + "/{stateId}")]
		public IActionResult GetCity(long? stateId = null)
		{
			return this.Success(masterService.GetMasterData(nameof(MasterKey.CITY), stateId));
		}
		[HttpGet(Routs.Global.State + "/{countryId}")]
		public IActionResult GetState(long? countryId = null)
		{
			return this.Success(masterService.GetMasterData(nameof(MasterKey.STATE), countryId));
		}
		[HttpGet(Routs.Global.Language)]
		public IActionResult GetLanguage()
		{
			return this.Success(masterService.GetMasterData(nameof(MasterKey.LANG)));
		}
		[HttpGet(Routs.Global.GlobalDrop)]
		public IActionResult GetGlobalDrop()
		{
			return this.Success(masterService.GetMasterData(nameof(MasterKey.TRNGNEEDTYPE), nameof(MasterKey.TRNGTYPE),string.Empty));
		}
        [HttpGet(Routs.Global.Zone)]
        public IActionResult GetZone()
        {
            return this.Success(masterService.GetMasterData(nameof(MasterKey.ZONE)));
        }
        
    }
}