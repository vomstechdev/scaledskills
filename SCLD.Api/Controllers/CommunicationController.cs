﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebSockets.Internal;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    public class CommunicationController : UserBaseController
    {
        private readonly IConfigManager configManager;
        readonly IOrganizationService organizationService;
        public CommunicationController(IOrganizationService _organizationService, IConfigManager _configManager)
        {
            organizationService = _organizationService;
            configManager = _configManager;
        }
        [HttpGet("T")]
        public IActionResult GetListByOrganizer(Training_Hosted training_Hosted)
        {
            var result = organizationService.GetListByOrganizer(UserId, training_Hosted);
            return this.Success(result);
        }
        [HttpPost("T/user")]
        public IActionResult GetUsersByTrainings(CommunicationTrainingUserModel communicationTrainingUserModel)
        {
            var result = organizationService.GetUserListByTrainings(communicationTrainingUserModel.Trainings, communicationTrainingUserModel.userTypes);
            return this.Success(result);
        }
       
        [HttpPost("O/T")]
        public IActionResult GetUserOrganizations(long userId, List<long> organizations)
        {
            var result = organizationService.GetTrainingListByOrganizations(UserId, organizations);
            return this.Success(result);
        }
        [HttpGet("O")]
        public IActionResult GetTrainingListByOrganizations()
        {
            var result = organizationService.GetUserOrganizations(UserId);
            return this.Success(result);
        }
        //[HttpGet("T/email")]
        //public IActionResult GetEmailCommunications(List<int> trainings, List<int> users)
        //{
        //    var result = organizationService.GetUserListByTrainings(trainings, users.Select(x=>Enum.Parse<Training_Users>(x.ToString())));
        //    return this.Success(result);
        //}
        //[HttpGet("T/email/Template")]
        //public IActionResult GetEmailTemplate(CommunicationTrainingUserModel communicationTrainingUserModel)
        //{
        //    var result = organizationService.GetUserListByTrainings(communicationTrainingUserModel.Trainings, communicationTrainingUserModel.userTypes);
        //    return this.Success(result);
        //}
        [HttpPost("T/email")]
        public IActionResult SendEmail(CommunicationViewModel communicationViewModel)
        {
            communicationViewModel.Email =
                ((System.Security.Claims.ClaimsIdentity)this.User.Identity).FindFirst(SCLD.Core.Helper.Constants.Strings.JwtClaimIdentifiers.Email)?.Value;
            communicationViewModel.UserId = UserId;
            organizationService.SendCommunicationEmail(communicationViewModel);
            return this.Success();
        }
    }
}