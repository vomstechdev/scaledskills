﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IPurchasable purchasable;
        private readonly IConfigManager configManager;
        public PaymentController(IPurchasable _purchasable, IConfigManager _configManager)
        {
            purchasable = _purchasable;
            configManager = _configManager;
        }
        [HttpPost]
        public IActionResult SavePayment(PaymentModel paymentModel)
        {
            try
            {
                if (Request.Form.ContainsKey("referenceId") && !string.IsNullOrWhiteSpace(Request.Form["referenceId"]))
                {
                    paymentModel.TransactionId = Request.Form["referenceId"];
                    paymentModel.Amount = Convert.ToDouble(Request.Form["orderAmount"].ToString());
                    paymentModel.PaymentStatus = Enum.Parse<Core.Models.Enums.UserPayment_Status>(Request.Form["txStatus"]);
                    paymentModel.Messege = Request.Form["txMsg"];
                    var orderId = purchasable.SaveOrderAndMarkAsDone(paymentModel);
                    return Redirect($"/orderDetail/{orderId}");
                }
                throw new Exception("Failure payment");
            }
            catch (Exception)
            {
                return Redirect($"/order/failure");
            }
        }
        [HttpGet]
        public IActionResult GetPayment()
        {
            //model.Reference = nameof(TrainingTicketModel);
            //var orderId = purchasable.CreateOrder(model);
            return Redirect("/paymentError");
        }
        [HttpGet("{paymentId}", Name = "init")]
        public void GetPayment(string paymentId)
        {
            Response.Clear();
            string d = GetStringPayment(paymentId);
            Response.StatusCode = 301;
            Response.WriteAsync(d);
            //var result = purchasable.GetOrder(orderId);
            //return this.Success(result);
        }
        [NonAction]
        public string GetStringPayment(string paymentId)
        {
            var userPay = purchasable.GetPayment(paymentId);
            string data = string.Empty;
            SortedDictionary<string, string> formParams = new SortedDictionary<string, string>();
            var key = configManager.GetSection<PaymentKeys>();
            PaymentModel paymentModel = new PaymentModel
            {
                OrderId = userPay.PaymentId.ToString(),
                Amount = Convert.ToDouble(userPay.Total),
                CustomerName = userPay.CustomerName,
                CustomerEmail = userPay.CustomerEmail,
                CustomerPhone = "9999999999",
                ReturnUrl = userPay.ReturnUrl,
                NotifyUrl = userPay.NotifyUrl,
                ApiKey = key.ApiKey,
                SecretKey = key.SecretKey
            };
            formParams.Add("appId", paymentModel.ApiKey);
            formParams.Add("orderId", paymentModel.OrderId);
            formParams.Add("orderAmount", paymentModel.Amount.ToString());
            formParams.Add("orderCurrency", paymentModel.OrderCurrency);
            formParams.Add("orderNote", paymentModel.OrderNote);
            formParams.Add("customerName", paymentModel.CustomerName);
            formParams.Add("customerPhone", paymentModel.CustomerPhone);
            formParams.Add("customerEmail", paymentModel.CustomerEmail);
            formParams.Add("returnUrl", paymentModel.ReturnUrl);
            formParams.Add("notifyUrl", paymentModel.NotifyUrl);
            foreach (var kvp in formParams)
            {
                data = data + kvp.Key + kvp.Value;
            }

            //string txTime = DateTime.Now.NowDate().ToString();
            string signature = string.Empty;
            string signatureData = paymentModel.ApiKey + paymentModel.OrderId
                + paymentModel.Amount + paymentModel.OrderCurrency +
                paymentModel.OrderNote +
                paymentModel.CustomerName +
                paymentModel.CustomerPhone +
                paymentModel.CustomerEmail +
                paymentModel.ReturnUrl +
                paymentModel.NotifyUrl;
            var hmacsha256 = new HMACSHA256(StringEncode(paymentModel.SecretKey));
            byte[] gensignature = hmacsha256.ComputeHash(StringEncode(signatureData));
            string computedsignature = Convert.ToBase64String(gensignature);
            StringBuilder sb = new StringBuilder();
            sb.Append("<html>");
            sb.AppendFormat(@"<body onload='document.forms[""redirectForm""].submit()'>");
            sb.Append($"<form id='redirectForm' method='post' action='{key.URL}'>");
            sb.Append($"<input type='hidden' name='appId' value='{paymentModel.ApiKey}'/>");
            sb.Append($"<input type='hidden' name='orderId' value='{paymentModel.OrderId}'/>");
            sb.Append($"<input type='hidden' name='orderAmount' value='{paymentModel.Amount}'/>");
            sb.Append($"<input type='hidden' name='orderCurrency' value='{paymentModel.OrderCurrency}'/>");
            sb.Append($"<input type='hidden' name='orderNote' value='{paymentModel.OrderNote}'/>");
            sb.Append($"<input type='hidden' name='customerName' value='{paymentModel.CustomerName}'/>");
            sb.Append($"<input type='hidden' name='customerPhone' value='{paymentModel.CustomerPhone}'/>");
            sb.Append($"<input type='hidden' name='customerEmail' value='{paymentModel.CustomerEmail}'/>");
            sb.Append($"<input type='hidden' name='returnUrl' value='{paymentModel.ReturnUrl}'/>");
            sb.Append($"<input type='hidden' name='notifyUrl' value='{paymentModel.NotifyUrl}'/>");
            sb.Append($"<input type='hidden' name='signature' value='{CreateToken(data, paymentModel.SecretKey)}'/>");
            sb.Append("</form>");
            sb.Append("</body>");
            sb.Append("</html>");
            return sb.ToString();
            //var result = purchasable.GetOrder(orderId);
            //return this.Success(result);
        }
        [HttpPost(Routs.Order.Process)]
        public IActionResult ProcessOrder(string orderId)
        {
            var result = purchasable.ProcessOrder(orderId);
            return Redirect("/orderDetail/ORD-0209-TL-000000005");
        }
        private static byte[] StringEncode(string text)
        {
            var encoding = new ASCIIEncoding();
            return encoding.GetBytes(text);
        }
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);

            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
    }
}