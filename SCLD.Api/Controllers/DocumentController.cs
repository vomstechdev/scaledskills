﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Helper.Document;

namespace SCLD.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class DocumentController : BaseController
    {
        private readonly IImageWriter IImageWriter;
        public DocumentController(IImageWriter imageHandler)
        {
            IImageWriter = imageHandler;
        }

        /// <summary>
        /// Uplaods an image to the server.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost(Routs.Document.Upload)]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            var result = await IImageWriter.UploadImage(file);
            return this.Success(result);
        }
        [HttpGet(Routs.Document.Preview)]
        [AllowAnonymous]
        public async Task<IActionResult> GetImage(Guid fileId)
        {
            var stream = await IImageWriter.GetStream(fileId, out string fileName);
            if (stream == null)
                return NotFound();

            return string.IsNullOrWhiteSpace(fileName) ?
                File(stream, "application/octet-stream") : File(stream, "application/octet-stream", fileName);
        }
        [HttpGet(Routs.Document.Delete)]
        public IActionResult DeleteImage(Guid fileId)
        {
            IImageWriter.DeleteImage(fileId);
            return this.Success("Deleted Successfully");
        }
    }
}