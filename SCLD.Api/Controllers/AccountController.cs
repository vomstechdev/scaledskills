﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SCLD.Auth;
using SCLD.Auth.Models;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Resource;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AccountController : BaseController
    {
        readonly IUserService userService;
        private readonly IJwtFactory jwtFactory;
        private readonly JwtIssuerOptions jwtOptions;
        public AccountController(IUserService _userService,
            IJwtFactory _jwtFactory,
            IOptions<JwtIssuerOptions> _jwtOptions
            )
        {
            userService = _userService;
            jwtFactory = _jwtFactory;
            jwtOptions = _jwtOptions.Value;


        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody]RegisterUserRequest model)
        
        {
            model.UserName = model.Email;
            var result = await userService.CreateAsTrainner(model);
            return result ? this.Success() : this.BadErrorRequest();
        }
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]CredentialsViewModel credentials)
        {
            var result = await userService.GetUserByPasswordAsync(credentials.UserName, credentials.Password);
            {
                if (result != null)
                {
                    var identity = jwtFactory.GenerateClaimsIdentity(credentials.UserName, result);
                    if (identity == null)
                    {
                        return BadRequest();
                    }
                    var response = new
                    {
                        id = identity.Claims.Single(c => c.Type == "id").Value,
                        auth_token = await jwtFactory.GenerateEncodedToken(credentials.UserName, identity),
                        FirstName = result.FirstName,
                        LastName = result.LastName,
                        PhoneNumber = result.PhoneNumber,
                        Image = result.Image,
                        expires_in = (int)jwtOptions.ValidFor.TotalSeconds
                    };
                    
                    //var json = JsonConvert.SerializeObject(response, _serializerSettings);
                    return this.Success(response);
                }
                else
                {
                    return this.BadNotAcccesptable(ValidationMSG.User_Password);
                }
            }
        }
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                return await userService.ChangePassword(User.Identity.GetUniqUserId(), model.OldPassword, model.NewPassword) ?
                     this.Success() : this.BadErrorRequest("Password is not valid.");
            }
            else
            {
                return this.BadModelRequest();
            }
        }
        [HttpGet("forgotPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            if (ModelState.IsValid)
            {
                var code = await userService.GeneratePasswordResetTokenByEmailAsync(email);
                return this.Success("Email Send Successfully");
            }
            return this.BadErrorRequest();
        }
        [HttpPost("forgotPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmForgotPassword([FromBody]ChangeForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.ResetPassword(model.UserId, model.Code, model.NewPassword);
                return result ? this.Success("Password Change Successfully") : this.BadErrorRequest();
            }
            return this.BadErrorRequest();
        }
        [HttpPost("confirmEmail")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromBody]CodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.ConfirmEmail(model.UserId, model.Code);
                return result ? this.Success("Email Activated Successfully") : this.BadErrorRequest();
            }
            return this.BadErrorRequest();
        }
        //[HttpGet("confirmEmail")]
        //[AllowAnonymous]
        //public async Task<IActionResult> SendConfirmEmail(string email)
        //{
        //    await userService.SendConfirmEmailAsync(email);
        //    return this.Success();
        //}
    }
}