﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("api/Ticket")]
    public class TicketController : BaseController
    {
        private readonly IConfigManager configManager;
        readonly ITicketService ticketService;
        public TicketController(IConfigManager _configManager, ITicketService _ticketService)
        {
            configManager = _configManager;
            ticketService = _ticketService;
        }
        [HttpGet(Routs.Ticket.TicketFeeDetail)]
        public IActionResult GetTicketFeeDetail(long? ticketId)
        {
            var detail = ticketService.GetTicketPaymentDetails(ticketId);
            return this.Success(detail);
        }
        [HttpGet(Routs.Ticket.TicketFeePreview)]
        public IActionResult GetTicketFeePreview(long? ticketId)
        {
            var detail = ticketService.GetTicketPaymentDetails(ticketId);
            return this.Success(detail);
        }
        [HttpPost(Routs.Ticket.TicketFeePreview)]
        public IActionResult GetTicketFeePreview(TicketDetailData ticketDetailData)
        {
            var detail = ticketService.GetTicketPaymentPreview(ticketDetailData);
            return this.Success(detail);
        }
    }
}