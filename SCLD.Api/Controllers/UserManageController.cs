﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
	[Route("api/User")]
	[ApiController]
	public class UserManageController : UserBaseController
	{
		private readonly IUserEducationService userEducationService;
		private readonly IUserCertificateService userCertificateService;
		private readonly IUserExperienceService userExperienceService;
		private readonly IUserSocialService userSocialService;
		private readonly IUserSettingService userSettingService;
		private readonly IUserLanguagesService userLanguagesService;
		private readonly IBankDetailService bankDetailService;
		private readonly IAboutService tagService;

		public UserManageController(
			 IUserEducationService _userEducationService,
			 IUserCertificateService _userCertificateService,
			 IUserExperienceService _userExperienceService,
			 IUserLanguagesService _userLanguagesService,
			 IUserSocialService _userSocialService,
			 IUserSettingService _userSettingService,
			 IBankDetailService _bankDetailService,
			 IAboutService _tagService
			)
		{
			userEducationService = _userEducationService;
			userCertificateService = _userCertificateService;
			userExperienceService = _userExperienceService;
			userLanguagesService = _userLanguagesService;
			userSocialService = _userSocialService;
			userSettingService = _userSettingService;
			bankDetailService = _bankDetailService;
			tagService = _tagService;
		}

		[HttpGet(Routs.UserManage.Eduction)]
		public IActionResult GetEducations()
		{
			var userId = UserId;
			var results = userEducationService.GetUserEducationByUserId(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.Eduction + "/{id}")]
		[Produces(type: typeof(UserEducationModel))]
		public IActionResult GetEducationById(long id)
		{
			return this.Success(userEducationService.GetUserEducationById(id));
		}
		[HttpPost(Routs.UserManage.Eduction)]
		public IActionResult SaveUserEducation(UserEducationModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? userEducationService.InsertUserEducation(model) :
			userEducationService.UpdateUserEducation(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.Certificate)]
		public IActionResult GetCertificates()
		{
			var userId = UserId;
			var results = userCertificateService.GetUserCertificateByUserId(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.Certificate + "/{id}")]
		public IActionResult GetUserCertificateById(long id)
		{
			return this.Success(userCertificateService.GetUserCertificateById(id));
		}
		[HttpPost(Routs.UserManage.Certificate)]
		public IActionResult SaveUserCertificate(UserCertificateModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? userCertificateService.InsertUserCertificate(model) :
			userCertificateService.UpdateUserCertificate(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.Experience)]
		public IActionResult GetExperiences()
		{
			var userId = UserId;
			var results = userExperienceService.GetUserExperienceById(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.Experience + "/{id}")]
		public IActionResult GetExperienceById(long id)
		{
			return this.Success(userExperienceService.GetUserExperienceById(id));
		}
		[HttpPost(Routs.UserManage.Experience)]
		public IActionResult SaveUserExperience(UserExperienceModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? userExperienceService.InsertUserExperience(model) :
			userExperienceService.UpdateUserExperience(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.UserLanguage)]
		public IActionResult GetUserLanguages()
		{
			var userId = UserId;
			var results = userLanguagesService.GetUserLanguagesByUserId(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.UserLanguage + "/{id}")]
		public IActionResult GetUserLanguagesById(long id)
		{
			return this.Success(userLanguagesService.GetUserLanguagesById(id));
		}
		[HttpPost(Routs.UserManage.UserLanguage)]
		public IActionResult SaveUserLanguages(UserLanguageModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? userLanguagesService.InsertUserLanguages(model) :
			userLanguagesService.UpdateUserLanguages(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.UserSocial)]
		public IActionResult GetUserSocials()
		{
			var userId = UserId;
			var results = userSocialService.GetUserSocialByUserId(userId).FirstOrDefault();
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.UserSocial + "/{id}")]
		public IActionResult GetUserSocialsById(long id)
		{
			return this.Success(userSocialService.GetUserSocialById(id));
		}
		[HttpPost(Routs.UserManage.UserSocial)]
		public IActionResult SaveUserSocials(UserSocialModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? userSocialService.InsertUserSocial(model) :
			userSocialService.UpdateUserSocial(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.UserSetting)]
		public IActionResult GetUserSettings()
		{
			var userId = UserId;
			var results = userSettingService.GetUserSettingByUserId(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.UserSetting + "/{id}")]
		public IActionResult GetUserSettingsById(long id)
		{
			return this.Success(userSettingService.GetUserSettingById(id));
		}
		[HttpPost(Routs.UserManage.UserSetting)]
		public IActionResult SaveUserSettings(UserProfileUserSetting model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = userSettingService.UpdateProfileUserSetting(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.UserTag)]
		public IActionResult GetUserTags()
		{
			var userId = UserId;
			var results = tagService.GetTagById(userId);
			return this.Success(results);
		}
		[HttpGet(Routs.UserManage.UserTag + "/{id}")]
		public IActionResult GetUserTagById(long id)
		{
			return this.Success(tagService.GetTagById(id));
		}
		[HttpPost(Routs.UserManage.UserTag)]
		public IActionResult SaveUserTags(AboutModel model)
		{
			model.EntityType = nameof(Core.Models.DataModels.User);
			model.EntityId = this.UserId;
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? tagService.InsertAbout(model) :
			tagService.UpdateAbout(model);
			return this.Success();
		}
		[HttpPost(Routs.UserManage.BankDetail)]
		public IActionResult SaveUserBankDetail(BankDetailModel model)
		{
			bool isInsert = model.Id <= 0;
			model.UserId = UserId;
			var id = isInsert ? bankDetailService.InsertBankDetail(model) :
			bankDetailService.UpdateBankDetail(model);
			return this.Success();
		}
		[HttpGet(Routs.UserManage.BankDetail)]
		public IActionResult GetBankDetail()
		{
			var userId = UserId;
			var results = bankDetailService.GetBankDetailByUserId(userId);
			return this.Success(results);
		}
	}
}