﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [AllowAnonymous]
    public class HomeController : UserBaseController
    {
        private readonly ITrainingListService trainingListService;
        private readonly IConfigManager configManager;

        public HomeController(ITrainingListService _trainingListService,
            IConfigManager _configManager)
        {
            trainingListService = _trainingListService;
            configManager = _configManager;

        }
        [HttpGet]
        public IActionResult Index()
        {
            return this.Success();
        }
        [HttpPost(Routs.Home.RecentTraining)]
        public IActionResult GetRecentTraining(SearchModel searchModel)
        {
            if (searchModel.PageType?.ToUpper() == "P")
            {
                searchModel.UserId = User.Identity.IsAuthenticated ? (long?)User.Identity.GetUserId() : null;
            }
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetRecentTraining(searchModel);
            return this.Success(list);
        }
        [HttpPost(Routs.Home.PastTraining)]
        public IActionResult GetPastTraining(SearchModel searchModel)
        {
            if (searchModel.UserId == 0 && searchModel.PageType?.ToUpper() == "P")
            {
                searchModel.UserId = User.Identity.IsAuthenticated ? (long?)User.Identity.GetUserId() : null;
            }
            string referer = Request.Headers["Referer"].ToString();
            if (referer?.ToUpper().Contains("/P/") == true)
                searchModel.PageType = "TP";
            else if (referer?.ToUpper().Contains("/O/") == true)
                searchModel.PageType = "O";
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetPastTraining(searchModel);
            return this.Success(list);
        }
        [HttpPost(Routs.Home.UpCommingTraining)]
        public IActionResult GetUpCommingTraining(SearchModel searchModel)
        {
            if (searchModel.UserId == 0 && searchModel.PageType?.ToUpper() == "P")
            {
                searchModel.UserId = User.Identity.IsAuthenticated ? (long?)User.Identity.GetUserId() : null;
            }
            string referer = Request.Headers["Referer"].ToString();
            if (referer?.ToUpper().Contains("/P/") == true)
                searchModel.PageType = "TP";
           
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetUpCommingTraining(searchModel);
            return this.Success(list);
        }
        [HttpPost(Routs.Home.AttendiesUpCommingTraining)]
        public IActionResult GetAttendiesUpCommingTraining(SearchModel searchModel)
        {
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetAttendiesUpCommingTraining(searchModel);
            return this.Success(list);
        }
        [HttpPost(Routs.Home.AttendiesPastTraining)]
        public IActionResult GetAttendiesPastTraining(SearchModel searchModel)
        {
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetAttendiesPastTraining(searchModel);
            return this.Success(list);
        }

        [HttpPost(Routs.Home.RelatedTraining)]
        public IActionResult GetRelatedTraining(SearchModel searchModel)
        {
            if (!(searchModel.UserId > 0))
            {
                searchModel.UserId = User.Identity.IsAuthenticated ? (long?)User.Identity.GetUserId() : null;
            }
            searchModel.LoginUserId = User.Identity.GetUserId();
            var list = trainingListService.GetRelatedTraining(searchModel);
            return this.Success(list);
        }
        [HttpGet(Routs.Home.Training)]
        public IActionResult GetTrainingByUrl(string urlName)
        {
            var list = trainingListService.GetTrainingByUrl(urlName,UserId);
            list = (list == null || list.CanEdit) ? list : (list.TrainingStatus == Core.Models.Enums.Training_Status.Publish ? list : null);
            if (list != null)
            {
                
                var followModel = list.HostedBy == 2 ? new UserFollowModel
                {
                    Type = Core.Models.Enums.UserFollowType.Organization,
                    TypeId = list.UserId,
                    UserId = UserId
                } : new UserFollowModel
                {
                    Type = Core.Models.Enums.UserFollowType.Trainer,
                    TypeId = list.TrainnerId,
                    UserId = UserId
                };
                list.IsFollow = configManager.GetService<IHomeService>().IsFollow(followModel);
                
            }

            return this.Success(list);
        }
        [HttpGet(Routs.Home.TrainingTickets)]
        public IActionResult GetTrainingTicketsByUrl(string urlName)
        {
            var list = trainingListService.GetTrainingTicketsByUrl(urlName);
            return this.Success(list);
        }
        [HttpGet(Routs.Home.Trainner)]
        public async Task<IActionResult> GetTrainnerByUrl(string urlName)
        {
            var trainnerService = configManager.GetService<ITrainerService>();
            var obj = trainnerService.GetByUrl(urlName);
            if (obj != null)
            {
                var userService = configManager.GetService<IUserService>();
                var user = await userService.GetUserById(obj.UserId);
                if (user != null)
                {
                    obj.FirstName = user.FirstName;
                    obj.LastName = user.LastName;
                    obj.PhoneNumber = string.IsNullOrEmpty(obj.PhoneNumber) ? user.PhoneNumber : user.PhoneNumber;
                    obj.Email = string.IsNullOrEmpty(obj.Email) ? user.Email : user.Email;
                    obj.Address = user.Address;
                }
                return this.Success(new
                {
                    user = obj,
                    Image = user?.Image,
                    about = configManager.GetService<IAboutService>().GetTagById(obj.AboutId),
                    certificate = configManager.GetService<IUserCertificateService>().GetUserCertificateByUserId(obj.UserId),
                    social = configManager.GetService<IUserSocialService>().GetUserSocialByUserId(obj.UserId),
                    IsFollow = configManager.GetService<IHomeService>().IsFollow(new UserFollowModel
                    {
                        Type = Core.Models.Enums.UserFollowType.Trainer,
                        TypeId = obj.Id,
                        UserId = UserId
                    })
                });
            }
            return this.Success(obj);
        }
        [HttpGet(Routs.Home.Organization)]
        public IActionResult GetOrganizationByUrl(string urlName)
        {
            var organizationService = configManager.GetService<IOrganizationService>();
            var obj = organizationService.GetByUrl(urlName);
            if (obj != null)
            {

                obj.Address = configManager.GetService<IAddressService>().GetAddressById(obj.AddressId);

                return this.Success(new
                {
                    user = obj,
                    about = configManager.GetService<IAboutService>().GetTagById(obj.AboutId),
                    social = configManager.GetService<IUserSocialService>().GetUserSocialById(obj.SocialId),
                    IsFollow = configManager.GetService<IHomeService>().IsFollow(new UserFollowModel
                    {
                        Type = Core.Models.Enums.UserFollowType.Organization,
                        TypeId = obj.Id,
                        UserId = UserId
                    })
                });
            }
            return this.Success(obj);
        }
        [HttpGet(Routs.Home.Header)]
        public IActionResult GetHeaderByUrl(string urlName)
        {
            var obj = trainingListService.GetHomeHeader();
            return this.Success(obj);
        }

        [HttpGet("Culture")]
        public IActionResult GetALLCultureInfos()
        {
            List<string> list = new List<string>();
            list.Add($"Thread {Thread.CurrentThread.ManagedThreadId} with culture {Thread.CurrentThread.CurrentCulture} => {DateTime.Now}");
            //TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            //DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);

            list.Add($"Thread {Thread.CurrentThread.ManagedThreadId} with culture Current => {DateTime.Now.NowDate()}");
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                string specName = "(none)";
                try { specName = System.Globalization.CultureInfo.CreateSpecificCulture(ci.Name).Name; }

                catch { }
                list.Add(String.Format("{0,-12}{1,-12}{2}", ci.Name, specName, ci.EnglishName));
                var culture = new System.Globalization.CultureInfo(ci.Name);
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
                var threadId = Thread.CurrentThread.ManagedThreadId;
                var threadCulture = Thread.CurrentThread.CurrentCulture;
                list.Add($"Thread {threadId} with culture {threadCulture} => {DateTime.Now}");



                //list.Add(ci.EnglishName );
            }
            return this.Success(list);
        }
        [HttpPost(Routs.Home.TrainingFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowTraining(UserFollowModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.UserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Training;
            homeservice.UserFollow(followModel);
            return this.Success();
        }
        [HttpPost(Routs.Home.TrainnerFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowTrainner(UserFollowModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.UserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Trainer;
            homeservice.UserFollow(followModel);
            return this.Success();
        }
        [HttpPost(Routs.Home.OrganizationFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowOrganization(UserFollowModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.UserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Organization;
            homeservice.UserFollow(followModel);
            return this.Success();
        }
        [HttpPost(Routs.Home.TrainingMembersFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowSearchTraining(FollowSearchModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.LoginUserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Training;
            var result = homeservice.UserMemberFollowList(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.TrainnerMembersFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowSearchTrainner(FollowSearchModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.LoginUserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Trainer;
            var result = homeservice.UserMemberFollowList(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.OrganizationMembersFollow)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FollowSearchOrganization(FollowSearchModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            followModel.LoginUserId = UserId;
            followModel.Type = Core.Models.Enums.UserFollowType.Organization;
            var result = homeservice.UserMemberFollowList(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.RegisteredTrainerUsers)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult RegisteredTrainerUsersList(TrainingUserSearchModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            var result = homeservice.RegisterTrainingUsersList(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.TrainingTrainerUsers)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult TrainingTrainerUsersList(TrainingUserSearchModel followModel)
        {
            var homeservice = configManager.GetService<IHomeService>();
            var result = homeservice.RegisterTrainerUsersList(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.TrainingReview)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult TrainingReviewList(SearchModel followModel)
        {
            var result = trainingListService.GetTrainingReview(followModel);
            return this.Success(result);
        }
        [HttpPost(Routs.Home.Contact)]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Contact(ContactModel contactModel)
        {
            var homeservice = configManager.GetService<IMasterService>();
            contactModel.UserId  = UserId;
            homeservice.AddContact(contactModel);
            return this.Success();
        }

    }
}