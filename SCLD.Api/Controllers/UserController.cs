﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Threading.Tasks;

namespace SCLD.Api.Controllers
{
	public class UserController : UserBaseController
	{
		readonly IUserService userService;
		public UserController(
			IUserService _userService
			)
		{
			userService = _userService;
		}
		[HttpPut]
		public async Task<IActionResult> UpdateUser(UserModel model)
		{
			model.UserId = this.UserId;
			var result = await userService.UpdateWithAttributeDetails(model);
			{
				return this.Success(result);
			}

		}
		[HttpGet]
		public async Task<IActionResult> GetUser()
		{
			try
			{
				var result = await userService.GetUserById(this.ApplicationUserId);
				{
					return this.Success(result);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		[HttpGet("Unsubscribe")]
		[AllowAnonymous]
		public async Task<IActionResult> UnsubscribeUser(string id)
		{
			try
			{
				var result = await userService.UnsubscribeUser(id);
				{
					return this.Success(result);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
	}
}