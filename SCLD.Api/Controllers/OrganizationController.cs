﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("api/Organization")]
    public class OrganizationController : UserBaseController
    {
        readonly IOrganizationService organizationService;
       
        private readonly IUserCertificateService userCertificateService;
        private readonly IBankDetailService bankDetailService;
        private readonly IAboutService tagService;
        private readonly IConfigManager configManager;
        OrganizationModel cuurentOrganization;
        public OrganizationController(IOrganizationService _organizationService,
            IUserEducationService _userEducationService,
             IUserCertificateService _userCertificateService,
             IUserExperienceService _userExperienceService,
             IUserLanguagesService _userLanguagesService,
             IUserSettingService _userSettingService,
             IBankDetailService _bankDetailService,
             IAboutService _tagService,
              IConfigManager _configManager)
        {
            organizationService = _organizationService;
            tagService = _tagService;
            bankDetailService = _bankDetailService;
            userCertificateService = _userCertificateService;
            configManager = _configManager;
            //cuurentOrganization = organizationService.GetOrganizationByUserId(this.UserId);
        }
        public OrganizationModel CuurentOrganization { get { cuurentOrganization = cuurentOrganization ?? organizationService.GetOrganizationByUserId(this.UserId); return cuurentOrganization; } }
        [HttpGet(Routs.UserManage.UserTag)]
        public IActionResult GetAbouts()
        {
            var userId = UserId;
            var results = tagService.GetTagById(CuurentOrganization.AboutId);
            return this.Success(results);
        }
        [HttpPost(Routs.UserManage.UserTag)]
        public IActionResult SaveAbouts(AboutModel model)
        {
            organizationService.UpdateAbout(model, CuurentOrganization.Id);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.Certificate)]
        public IActionResult GetCertificates()
        {
            var userId = UserId;
            var results = userCertificateService.GetUserCertificateByUserId(userId);
            return this.Success(results);
        }
        [HttpGet(Routs.UserManage.Certificate + "/{id}")]
        public IActionResult GetUserCertificateById(long id)
        {
            return this.Success(userCertificateService.GetUserCertificateById(id));
        }
        [HttpPost(Routs.UserManage.Certificate)]
        public IActionResult SaveUserCertificate(UserCertificateModel model)
        {
            bool isInsert = model.Id <= 0;
            model.UserId = UserId;
            var id = isInsert ? userCertificateService.InsertUserCertificate(model) :
            userCertificateService.UpdateUserCertificate(model);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.BankDetail)]
        public IActionResult GetBankDetails()
        {
            var userId = UserId;
            var results = bankDetailService.GetBankDetailById(CuurentOrganization.BankDetailId);
            return this.Success(results);
        }
        [HttpPost(Routs.UserManage.BankDetail)]
        public IActionResult SaveBankDetails(BankDetailModel model)
        {
            organizationService.UpdateBankDetail(model, CuurentOrganization.Id);
            return this.Success();
        }
        [HttpGet]
        public IActionResult GetOrganization()
        {
            var obj = this.CuurentOrganization;
            if (this.CuurentOrganization?.AddressId != null)
            {
                this.CuurentOrganization.Address = configManager.GetService<IAddressService>().GetAddressById(this.CuurentOrganization.AddressId);
            }

            return this.Success(this.CuurentOrganization);
        }
        [HttpPost]
        public IActionResult SaveOrganization(OrganizationModel model)
        {
            model.UserId = UserId;
            model.Id = CuurentOrganization?.Id ?? 0;
            model.AddressId = CuurentOrganization?.AddressId;
            if (CuurentOrganization == null)
                organizationService.InsertOrganization(model);
            else
                organizationService.UpdateOrganization(model);
            return this.Success();
        }
        [HttpGet("list")]
        public IActionResult GetOrganizationList()
        {
            var result = organizationService.GetOrganizationList().Select(x => new SelectItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            return this.Success(result);
        }
        
    }
}