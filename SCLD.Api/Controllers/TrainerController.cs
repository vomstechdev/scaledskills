﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    [Route("api/Trainer")]
    public class TrainerController : UserBaseController
    {
        readonly ITrainerService trainerService;
        private readonly IUserCertificateService userCertificateService;
        private readonly IUserSocialService userSocialService;
        private readonly IBankDetailService bankDetailService;
        private readonly IAboutService tagService;
        private readonly IConfigManager configManager;
        TrainerModel cuurentTrainer;
        public TrainerController(ITrainerService _trainerService,
            IUserEducationService _userEducationService,
             IUserCertificateService _userCertificateService,
             IUserExperienceService _userExperienceService,
             IUserLanguagesService _userLanguagesService,
             IUserSocialService _userSocialService,
             IUserSettingService _userSettingService,
             IBankDetailService _bankDetailService,
             IAboutService _tagService,
             IConfigManager _configManager)
        {
            trainerService = _trainerService;
            tagService = _tagService;
            bankDetailService = _bankDetailService;
            userCertificateService = _userCertificateService;
            userSocialService = _userSocialService;
            configManager = _configManager;
        }
        public TrainerModel CuurentTrainer { get  { cuurentTrainer = cuurentTrainer ?? trainerService.GetTrainerByUserId(this.UserId) ?? new TrainerModel(); return cuurentTrainer; } }
        [HttpGet(Routs.UserManage.UserTag)]
        public IActionResult GetAbouts()
        {
            var userId = UserId;
            var results = tagService.GetTagById(CuurentTrainer.AboutId);
            return this.Success(results);
        }
        [HttpPost(Routs.UserManage.UserTag)]
        public IActionResult SaveAbouts(AboutModel model)
        {
            trainerService.UpdateAbout(model, CuurentTrainer.Id);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.Certificate)]
        public IActionResult GetCertificates()
        {
            var userId = UserId;
            var results = userCertificateService.GetUserCertificateByUserId(userId);
            return this.Success(results);
        }
        [HttpGet(Routs.UserManage.Certificate + "/{id}")]
        public IActionResult GetUserCertificateById(long id)
        {
            return this.Success(userCertificateService.GetUserCertificateById(id));
        }
        [HttpPost(Routs.UserManage.Certificate)]
        public IActionResult SaveUserCertificate(UserCertificateModel model)
        {
            bool isInsert = model.Id <= 0;
            model.UserId = UserId;
            var id = isInsert ? userCertificateService.InsertUserCertificate(model) :
            userCertificateService.UpdateUserCertificate(model);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.BankDetail)]
        public IActionResult GetBankDetails()
        {
            var userId = UserId;
            var results = bankDetailService.GetBankDetailById(CuurentTrainer.BankDetailId);
            return this.Success(results);
        }
        [HttpPost(Routs.UserManage.BankDetail)]
        public IActionResult SaveBankDetails(BankDetailModel model)
        {
            trainerService.UpdateBankDetail(model, CuurentTrainer.Id);
            return this.Success();
        }
        [HttpGet(Routs.UserManage.ValidateUrl)]
        public IActionResult ValidateUrl(string url)
        {
            return this.Success(!trainerService.ValidateUrl(url, this.UserId));
        }
        [HttpGet]
        public async Task<IActionResult> GetTrainer()
        {
            var current = this.CuurentTrainer;
            var userService = configManager.GetService<IUserService>();
            if (current != null)
            {
                var user = await userService.GetUserById(ApplicationUserId);
                current.FirstName = user.FirstName;
                current.LastName = user.LastName;
                current.PhoneNumber = string.IsNullOrEmpty(current.PhoneNumber) ? user.PhoneNumber : current.PhoneNumber;
                current.Email = string.IsNullOrEmpty(current.Email) ? user.Email : current.Email;
                if (!(current.Id > 0))
                {
                    SaveTrainer(new TrainerModel
                    {
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                        ProfileUrl = Guid.NewGuid().ToString(),
                        UserId = UserId
                    });
                }
                return this.Success(current);
            }
            return this.Success(current);
        }
        [HttpPost]
        public IActionResult SaveTrainer(TrainerModel model)
        {
            model.UserId = UserId;
            model.Id = CuurentTrainer?.Id ?? 0;
            model.AddressId = CuurentTrainer?.AddressId ?? 0;
            if (CuurentTrainer?.Id == 0)
                trainerService.InsertTrainer(model);
            else
                trainerService.UpdateTrainer(model);
            return this.Success();
        }
        [HttpPost(Routs.UserManage.Email)]
        public IActionResult SetMessege(Core.Models.ViewModels.EmailModel email)
        {
            email.EmailStatus = Core.Models.DataModels.EmailQueue.STATUS_PENDING;
            email.UserId = this.UserId;
            email.DisplayName = "Test";
            var masterService = configManager.GetService<IMasterService>();
            masterService.SetEmail(email);
            return this.Success();
        }

    }
}