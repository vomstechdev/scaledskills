﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Helper;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;

namespace SCLD.Api.Controllers
{
    public class OrderController : UserBaseController
    {
        private readonly IPurchasable purchasable;
        private readonly IConfigManager configManager;
        public OrderController(IPurchasable _purchasable, IConfigManager _configManager)
        {
            purchasable = _purchasable;
            configManager = _configManager;
        }
        [HttpPost(Routs.Order.Ticket)]
        public IActionResult CreateTicketOrder(OrderModel model)
        {
            model.UserId = UserId;
            model.Reference = nameof(TrainingTicketModel);
            var orderId = purchasable.CreateOrder(model);
            return this.Success(orderId);
        }
        [HttpGet("{orderId}")]
        public IActionResult GetOrder(string orderId)
        {
            var result = purchasable.GetOrder(orderId);
            return this.Success(result);
        }
        [HttpPost(Routs.Order.Process)]
        [AllowAnonymous]
        public void ProcessOrder(string orderId)
        {
            try
            {
                var result = purchasable.ProcessOrder(orderId);
                //return this.Success(result);
            }
            catch (PaymentRedirect paymentRedirect)
            {
                Response.Clear();
                var str = new PaymentController(purchasable, configManager).GetStringPayment(paymentRedirect.Message);
                Response.StatusCode = 301;
                Response.WriteAsync(str);

            }

        }
        [HttpGet(Routs.Order.Summary)]
        [AllowAnonymous]
        public IActionResult SummaryOrder(string orderId)
        {
            try
            {
                var result = purchasable.OrderSummary(orderId);
                return this.Success(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost(Routs.Order.Cancle)]
        public IActionResult CancleOrder(string orderId)
        {
            var result = purchasable.CancleOrder(orderId);
            return this.Success(result);
        }
    }
}