﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SCLD.Auth;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using AutoMapper;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using ElmahCore.Mvc;
using ElmahCore;
using Microsoft.AspNetCore.Rewrite;

namespace SCLD.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp";
            });
            services.AddElmah<XmlFileErrorLog>(option => option.LogPath = "log");
            services.AddDbContextPool<SCLD.Data.SCLDContext>(options =>
                              options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                                      b => b.MigrationsAssembly("SCLD.Data")));
            //services.AddDbContext<SCLD.Data.SCLDContext>(ServiceLifetime.Transient);
            services.AddScoped<IConfigManager, ConfigManager>();
            services.AddTransient<Core.Helper.Document.IImageWriter,
                                  Core.Helper.Document.ImageWriter>();
            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.AddSingleton<IAuthManager, Auth.Auth>();
            Mapping.DependencyConfig.Current.Build((inerface, implement, type) =>
                        {
                            if (type == (int)Core.Infrastructure.ObjectType.Singleton)
                            {
                                services.AddSingleton(inerface, implement);
                            }
                            else
                                services.AddScoped(inerface, implement);
                        });
            Mapping.DependencyConfig.Current.BuildPreObjectService((obj, type) => services.AddSingleton(obj));
            var iservice = services.BuildServiceProvider();
            new Core.Config.ConfigurationSection(iservice.GetService<IConfigManager>());
            IAuthManager authManager = iservice.GetService<IAuthManager>();
            authManager.AddAuthentication(ref services);
            services.AddIdentity<User, IdentityRole>
                (o =>
                {
                    // configure identity options
                    o.Password.RequireDigit = false;
                    o.Password.RequireLowercase = false;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireNonAlphanumeric = false;
                    o.Password.RequiredLength = 6;
                })
                .AddEntityFrameworkStores<SCLD.Data.SCLDContext>()
                .AddDefaultTokenProviders();
            
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() },
                });
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "My API", Version = "v1" });
                //c.OperationFilter<SWEGMyHeaderFilter>();
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    {

                        builder.WithOrigins("http://dev.scaledskills.com",
                                            "http://localhost:4200", "http://scaledskills.com", "http://localhost:4000", "http://localhost:5000", "http://localhost:4300")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                    });

                options.AddPolicy("AnotherPolicy",
                    builder =>
                    {
                        builder.WithOrigins("http://dev.scaledskills.com",
                           "http://localhost:4200", "http://localhost:4000", "http://localhost:5000", "http://scaledskills.com", "http://localhost:4300")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod();
                    });

            });
            services.AddAntiforgery(options =>
            {
                options.FormFieldName = "forgoryTKN";
            });
            services.AddHostedService<Core.Infrastructure.HostedServices.SCLDHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.Accepted;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature.Error is Core.Helper.DataValidationException)
                    {
                        var validationError = contextFeature.Error as Core.Helper.DataValidationException;
                        await context.Response.WriteAsync(Newtonsoft.Json.JsonConvert.SerializeObject(new Core.Models.ViewModels.Response<object>
                        {
                            ResponseCode = Core.Models.Enums.ResponseCode.NotAcceptable,
                            ResponseMessege = contextFeature.Error.Message,
                            Result = validationError.ValidationError
                        }));
                    }
                    else
                    {
                        await context.Response.WriteAsync(Newtonsoft.Json.JsonConvert.SerializeObject(new Core.Models.ViewModels.Response<string>
                        {
                            ResponseCode = Core.Models.Enums.ResponseCode.BadRequest,
                            ResponseMessege = contextFeature.Error.Message,
                            Result = contextFeature.Error.Message
                        }));
                    }

                });
            });
            var rewrite = new RewriteOptions()
               .AddRewrite("contact", "/", true);
            app.UseRewriter(rewrite);
            app.UseMiddleware<Helper.UrlUIMiddleware>();
            app.UseElmah();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseAuthentication();


            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SCLD API V1");
            });
            app.UseCors("AnotherPolicy");
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
                //if (env.IsDevelopment())
                //{
                //    spa.UseReactDevelopmentServer(npmScript: "start");
                //}
            });

        }
    }
}
