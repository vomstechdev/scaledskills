﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCLD.Api.Helper
{
    public class Routs
    {
        public class UserManage
        {
            public const string Eduction = "Education";
            public const string User = "User";
            public const string Certificate = "Certificate";
            public const string Experience = "Experience";
            public const string UserLanguage = "UserLanguage";
            public const string UserSocial = "UserSocial";
            public const string UserSetting = "UserSetting";
            public const string UserTag = "UserTag";
            public const string BankDetail = "BankDetail";
            public const string ValidateUrl = "VU";
            public const string Email = "Email";
        }
        public class Global
        {
            public const string Country = "Country";
            public const string City = "City";
            public const string State = "State";
            public const string Language = "Language";
            public const string GlobalDrop = "GlobalDropdown";
            public const string Zone = "Zone";
            public const string Email = "Email";
        }
        public class Training
        {
            public const string TrainingRequest = "Request";
            public const string TrainingLocation = "{TrainingId}/Location";
            public const string TrainingAbout = "{TrainingId}/About";
            public const string TrainingImage = "{TrainingId}/Image";
            public const string TrainingTicket = "{TrainingId}/Ticket";
            public const string TrainingReview = "{TrainingId}/Review";
            public const string TrainingUserReview = "{TrainingId}/U/Review";
            public const string TrainingPromotions = "{TrainingId}/Promotions";
            public const string TrainingSettings = "{TrainingId}/Settings";
            public const string TrainingTrainers = "{TrainingId}/Trainers";
            public const string ValidateData = "{TrainingId}/VU";
            public const string TrainingInterest = "{TrainingId}/Interest";
            public const string TrainingJoin = "{TrainingId}/Join";
            public const string TrainingPublish = "{TrainingId}/Publish";

        }
        public class Home
        {
            public const string RecentTraining = "RT";
            public const string PastTraining = "PT";
            public const string UpCommingTraining = "UCT";
            public const string AttendiesPastTraining = "AT/PT";
            public const string AttendiesUpCommingTraining = "AT/UCT";
            public const string RelatedTraining = "RelatedT";
            public const string Training = "T/{urlName}";
            public const string TrainingTickets = "T/Tickets/{urlName}";
            public const string Trainner = "P/{urlName}";
            public const string Organization = "O/{urlName}";
            public const string Header = "h/{urlName?}";
            public const string TrainingFollow = "T/f";
            public const string TrainnerFollow = "P/f";
            public const string OrganizationFollow = "O/f";
            public const string TrainingMembersFollow = "T/M/f";
            public const string TrainnerMembersFollow = "P/M/f";
            public const string OrganizationMembersFollow = "O/M/f";
            public const string RegisteredTrainerUsers = "T/M/R";
            public const string TrainingTrainerUsers = "T/M/T";
            public const string TrainingReview = "T/review";
            public const string Contact = "Contact";



        }
        public class Document
        {
            public const string Upload = "Upload";
            public const string Preview = "P/{fileId}";
            public const string Delete = "d/{fileId}";
        }
        public class Order
        {
            public const string Ticket = "T";
            public const string Process = "P/{orderId}";
            public const string Summary = "S/{orderId}";
            public const string Cancle = "C/{orderId}";
        }
        public class Ticket
        {
            public const string TicketFeeDetail = "Fee/{ticketId?}";
            public const string TicketFeePreview = "Preview";
        }
    }
}
