﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SCLD.Api.Helper
{
    public class UrlUIMiddleware
    {
                    private readonly RequestDelegate next;
            //private readonly JsonSerializerOptions _jsonSerializerOptions;

            public UrlUIMiddleware(
                RequestDelegate _next,
                IHostingEnvironment hostingEnv,
                ILoggerFactory loggerFactory)
            {
                next = _next;
                //_options = options ?? new SwaggerUIOptions();

                //_staticFileMiddleware = CreateStaticFileMiddleware(next, hostingEnv, loggerFactory, options);

                //_jsonSerializerOptions = new JsonSerializerOptions();
                //_jsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                //_jsonSerializerOptions.IgnoreNullValues = true;
                //_jsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase, false));
            }

            public async Task Invoke(HttpContext httpContext)
            {
                var httpMethod = httpContext.Request.Method;
                var path = httpContext.Request.Path.Value;
                var routPrifix = "home";
                // If the RoutePrefix is requested (with or without trailing slash), redirect to index URL
                if (httpMethod == "GET" && Regex.IsMatch(path, $"^/{routPrifix}/?$"))
                {
                    // Use relative redirect to support proxy environments
                    //var relativeRedirectPath = path.EndsWith("/")
                    //    ? "index.html"
                    //    : $"{path.Split('/').Last()}/index.html";
                    var relativeRedirectPath = "/";

                    RespondWithRedirect(httpContext.Response, relativeRedirectPath);
                    return;
                }

                //if (httpMethod == "GET" && Regex.IsMatch(path, $"/{_options.RoutePrefix}/?index.html"))
                //{
                //    await RespondWithIndexHtml(httpContext.Response);
                //    return;
                //}

                await next.Invoke(httpContext);
            }

            //private StaticFileMiddleware CreateStaticFileMiddleware(
            //    RequestDelegate next,
            //    IHostingEnvironment hostingEnv,
            //    ILoggerFactory loggerFactory
            //    )
            //{
            //    var staticFileOptions = new StaticFileOptions
            //    {
            //        RequestPath = string.IsNullOrEmpty(options.RoutePrefix) ? string.Empty : $"/{options.RoutePrefix}",
            //        FileProvider = new EmbeddedFileProvider(typeof(SwaggerUIMiddleware).GetTypeInfo().Assembly, EmbeddedFileNamespace),
            //    };

            //    return new StaticFileMiddleware(next, hostingEnv, Options.Create(staticFileOptions), loggerFactory);
            //}

            private void RespondWithRedirect(HttpResponse response, string location)
            {
                response.StatusCode = 301;
                response.Headers["Location"] = location;
            }

            //private async Task RespondWithIndexHtml(HttpResponse response)
            //{
            //    response.StatusCode = 200;
            //    response.ContentType = "text/html;charset=utf-8";

            //    using (var stream = _options.IndexStream())
            //    {
            //        // Inject arguments before writing to response
            //        var htmlBuilder = new StringBuilder(new StreamReader(stream).ReadToEnd());
            //        foreach (var entry in GetIndexArguments())
            //        {
            //            htmlBuilder.Replace(entry.Key, entry.Value);
            //        }

            //        await response.WriteAsync(htmlBuilder.ToString(), Encoding.UTF8);
            //    }
            //}

            //private IDictionary<string, string> GetIndexArguments()
            //{
            //    return new Dictionary<string, string>()
            //{
            //    { "%(DocumentTitle)", _options.DocumentTitle },
            //    { "%(HeadContent)", _options.HeadContent },
            //    { "%(ConfigObject)", JsonSerializer.Serialize(_options.ConfigObject, _jsonSerializerOptions) },
            //    { "%(OAuthConfigObject)", JsonSerializer.Serialize(_options.OAuthConfigObject, _jsonSerializerOptions) }
            //};
            //}
        
    }
}
