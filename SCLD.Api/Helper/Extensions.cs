﻿using Microsoft.AspNetCore.Mvc;
using SCLD.Api.Controllers;
using SCLD.Core.Helper;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace SCLD.Api
{
    public static class Extensions
    {
        public static long GetUserId(this IIdentity identity)
        {
            var claim = ((System.Security.Claims.ClaimsIdentity)identity).FindFirst(Constants.Strings.JwtClaimIdentifiers.UserId);
            var val = claim?.Value;
            return (val != null) ? Convert.ToInt64(val) : default(long);
        }
        public static string GetUniqUserId(this IIdentity identity)
        {
            var claim = ((System.Security.Claims.ClaimsIdentity)identity).FindFirst(Constants.Strings.JwtClaimIdentifiers.Id);
            return claim?.Value;
        }

        public static IActionResult BadModelRequest(this BaseController controller)
        {
            var errors = string.Join(", ", controller.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            var resp = controller.CreateRes(
                new Response<object>
                {
                    ResponseCode = ResponseCode.BadRequest,
                    ResponseMessege = errors,
                    Result = controller.ModelState.ToDictionary(
                              kvp => kvp.Key,
                              kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray())
                });
            return resp;
        }
        public static IActionResult BadErrorRequest<T>(this BaseController controller, T result)
        {
            var error = typeof(T) == typeof(string) ? result.ToString() : "Failuar";
            var resp = controller.CreateRes(
                new Response<T>
                {
                    ResponseCode = ResponseCode.BadRequest,
                    ResponseMessege = error,
                    Result = result
                });
            return resp;
        }
        public static IActionResult BadNotAcccesptable<T>(this BaseController controller, T result)
        {
            var error = typeof(T) == typeof(string) ? result.ToString() : "Failuar";
            var resp = controller.CreateRes(
                new Response<T>
                {
                    ResponseCode = ResponseCode.NotAcceptable,
                    ResponseMessege = error,
                    Result = result
                });
            return resp;
        }
        public static IActionResult BadErrorRequest(this BaseController controller)
        {
            var error = "Failuar";
            var resp = controller.CreateRes(
                new Response<object>
                {
                    ResponseCode = ResponseCode.BadRequest,
                    ResponseMessege = error,
                    Result = null
                });
            return resp;
        }
        public static IActionResult Success<T>(this BaseController controller, T result)
        {
            if (result == null)
            {
                return controller.Success();
            }
            var resp = controller.CreateRes(
                new Response<T>
                {
                    Result = result
                });

            return resp;
        }
        //public static IActionResult Success<T>(this BaseController controller, T result, System.Net.CookieCollection cookie)
        //{
        //    var resp = controller.CreateRes(
        //        new Response<T>
        //        {
        //            Result = result
        //        });
        //    if (cookie != null)
        //    {
        //        var cookiesValue = new List<Net.Http.Headers.CookieHeaderValue>();
        //        foreach (Net.Cookie coo in cookie)
        //        {
        //            var cooheader = new Net.Http.Headers.CookieHeaderValue(coo.Name, coo.Value);
        //            //cooheader.Expires = coo.Expires;
        //            //cooheader.Secure = coo.Secure;
        //            cookiesValue.Add(cooheader);
        //        }
        //        resp.Headers.AddCookies(cookiesValue);
        //    }
        //    return new ResponseMessageResult(resp);
        //}
        public static IActionResult Success<T>(this BaseController controller, Response<T> result)
        {
            var resp = controller.CreateRes(result);
            return resp;
        }
        public static IActionResult Success(this BaseController controller)
        {
            var resp = controller.CreateRes(
                new Response<object>
                {
                    Result = new Dictionary<string, string>()
                });
            return resp;
        }
        public static IActionResult CreateRes<T>(this BaseController con, ResponseCode code, T result)
        {
            var error = typeof(T) == typeof(string) ? result.ToString() : "Failuar";
            var res = con.CreateRes(new Response<T>
            {
                ResponseCode = code,
                ResponseMessege = error,
                Result = result
            });
            return res;
        }
        public static IActionResult CreateRes<T>(this BaseController con, Response<T> result)
        {
            //return con.Request.CreateResponse((Net.HttpStatusCode)Enum.Parse(
            //    typeof(Net.HttpStatusCode), result.ResponseCode.ToString()), result);
            return new OkObjectResult(result);
        }

        public static void UseDefaultCulture()
        {
            var cultureInfo = new CultureInfo("en-IN");
          //  cultureInfo.NumberFormat.CurrencySymbol = "€";

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }
    }
}
