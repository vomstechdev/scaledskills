﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SCLD.Core.Infrastructure;

namespace SCLD.Api
{
    public class ConfigManager : IConfigManager
    {
        IConfiguration configuration;
        IServiceProvider serviceProvider;
        public ConfigManager(IConfiguration _configuration, [FromServices] IServiceProvider _serviceProvider)
        {
            configuration = _configuration;
            serviceProvider = _serviceProvider;
        }
        public T GetSection<T>()
        {
            var section = GetSection<T>(typeof(T).Name);
            return section;
        }
        public T GetSection<T>(string key)
        {
            var section = configuration.GetSection(key).Get<T>();
            return section;
        }
        public T GetService<T>()
        {
            var section = (T)GetService(typeof(T));
            return section;
        }
        public object GetService(Type serviceType)
        {
            var section = serviceProvider.GetService(serviceType);
            return section;
        }

    }
}   
