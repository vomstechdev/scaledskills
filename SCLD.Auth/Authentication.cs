﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SCLD.Auth.Models;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCLD.Auth
{
    public interface IAuthManager
    {
        void AddAuthentication(ref IServiceCollection services);
    }
    public class Auth : IAuthManager
    {
        readonly IConfigManager configManager;
        private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH"; // todo: get this from somewhere secure
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        public Auth(IConfigManager _configManager)
        {
            configManager = _configManager;
        }
        public void AddAuthentication(ref IServiceCollection services)
        {

            var seviceCollection = services;
            var jwtAppSettingOptions = configManager.GetSection<JwtIssuerOptions>(nameof(JwtIssuerOptions));
            seviceCollection.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions.Issuer;
                options.Audience = jwtAppSettingOptions.Audience;
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });
            //seviceCollection.AddAuthorization(options =>
            //{
            //    options.AddPolicy("ApiUser", policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.ApiAccess));
            //});

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions.Issuer,

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions.Audience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };
            seviceCollection.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => 
            {
                x.ClaimsIssuer = jwtAppSettingOptions.Issuer;
                x.TokenValidationParameters = tokenValidationParameters;
                x.SaveToken = true;
            });
            
        }
    }
}
