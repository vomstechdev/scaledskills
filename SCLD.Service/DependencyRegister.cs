﻿using SCLD.Core.Infrastructure.DependencyManagement;
using SCLD.Core.Service;
using SCLD.Service.Logic;
using System;
using System.Collections.Generic;

namespace SCLD.Service
{
    public class DependencyRegister: IDependencyRegistrar
    {
        public int Order => 2;

        public void Register(ref Dictionary<Type, Type> dependencies)
        {
            dependencies.Add(typeof(IUserService), typeof(UserService));
            dependencies.Add(typeof(IAddressService), typeof(AddressService));
            dependencies.Add(typeof(IGoogleService), typeof(GoogleService));
            dependencies.Add(typeof(IUserEducationService), typeof(UserEducationService));
            dependencies.Add(typeof(IUserCertificateService), typeof(UserCertificateService));
            dependencies.Add(typeof(IUserExperienceService), typeof(UserExperienceService));
            dependencies.Add(typeof(IUserLanguagesService), typeof(UserLanguagesService));
            dependencies.Add(typeof(IUserSocialService), typeof(UserSocialService));
            dependencies.Add(typeof(IUserSettingService), typeof(UserSettingService));
			dependencies.Add(typeof(IAboutService), typeof(AboutService));
			dependencies.Add(typeof(IMasterService), typeof(MasterService));
			dependencies.Add(typeof(ITrainingRequestService), typeof(TrainingRequestService));
			dependencies.Add(typeof(IBankDetailService), typeof(BankDetailService));
			dependencies.Add(typeof(ITrainerService), typeof(TrainerService));
            dependencies.Add(typeof(ITrainingImageService), typeof(TrainingImageService));
            dependencies.Add(typeof(IOrganizationService), typeof(OrganizationService));
			dependencies.Add(typeof(IEmailService), typeof(EmailService));
            dependencies.Add(typeof(IEmailSendingService), typeof(EmailService));
            dependencies.Add(typeof(ITrainingService), typeof(TrainingService));
            dependencies.Add(typeof(IFileRepository), typeof(DocumentService));
            dependencies.Add(typeof(ITrainingListService), typeof(TrainingListService));
            dependencies.Add(typeof(IHomeService), typeof(UserHomeService));
            dependencies.Add(typeof(ITicketOrderService), typeof(TicketOrderService));
            dependencies.Add(typeof(IPurchasable), typeof(Purchasable));
            dependencies.Add(typeof(IServicePurchasable), typeof(PurchasableService));
            dependencies.Add(typeof(ITicketService), typeof(TicketService));

        }
    }
}
