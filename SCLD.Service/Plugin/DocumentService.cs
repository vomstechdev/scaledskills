﻿using SCLD.Core.Config;
using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using SCLD.Service.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Service
{
    class DocumentService : BaseService, IFileRepository
    {
        readonly IRepository<Document> documentRepository;
        public DocumentService(IRepository<Document> _documentRepository)
        {
            documentRepository = _documentRepository;
        }
        public DocumentModel Add(DocumentModel model)
        {
            var doc = model.ToEntity<Document>();
            documentRepository.Insert(doc);
            model.Id = doc.Id;
            return model;
        }

        public void Delete(Guid Id)
        {
            var doc = documentRepository.Table.First(x => x.DocumentId == Id);
            doc.IsDelete = true;
            documentRepository.Update(doc);
        }

        public IEnumerable<DocumentModel> GetAllFile(Guid Group)
        {
            var docList = documentRepository.TableNoTracking.Where(x => x.Group == Group && x.IsDelete == false).ToList()
                     .Select(x => x.ToModel<DocumentModel>());
            return docList;
        }

        public DocumentModel GetFile(Guid Id)
        {
            var doc = documentRepository.Table.First(x => x.DocumentId == Id);
            return doc.ToModel<DocumentModel>();
        }

        public void SetOrderFile(Guid Id, int order)
        {
            var doc = documentRepository.Table.First(x => x.Group == Id);
            doc.Order = order;
            documentRepository.Update(doc);
        }

        public DocumentModel Update(DocumentModel model)
        {
            var doc = model.ToEntity<Document>();
            documentRepository.Update(doc);
            model.Id = doc.Id;
            return model;
        }
    }
}
