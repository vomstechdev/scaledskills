﻿using SCLD.Core.Config;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Service;
using SCLD.Service.Logic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Service
{
    class GoogleService : BaseService, IGoogleService
    {
        readonly IConfigManager configManager;
        public GoogleService(IConfigManager _configManager)
        {
            configManager = _configManager;
        }
        public async Task<object> GetInfoForImage(string path)
        {
            var baseString = Util.GetBase64StringForImage(path);
            string myJson = $@"{{
                    ""requests"":[
                        {{
                            ""image"":{{
                                ""content"": ""{baseString}""
                            }},
                            ""features"":[
                                {{
                                    ""type"": ""LANDMARK_DETECTION""
                                }},
                                {{ 
                                    ""type"": ""FACE_DETECTION""
                                }},
                                {{
                                    ""type"": ""OBJECT_LOCALIZATION""
                                }},
                                {{
                                    ""type"": ""LOGO_DETECTION""
                                }},
                                {{
                                    ""type"": ""LABEL_DETECTION""
                                }},
                                {{
                                    ""type"": ""DOCUMENT_TEXT_DETECTION""
                                }},
                                {{
                                    ""type"": ""SAFE_SEARCH_DETECTION""
                                }},
                                {{
                                    ""type"": ""IMAGE_PROPERTIES""
                                }},
                                {{
                                    ""type"": ""CROP_HINTS""
                                }},
                                {{
                                    ""type"": ""WEB_DETECTION""
                                }}
                            ]
                        }}
                    ]
                }}";
            var config = ConfigurationSection.Current.GoogleConfig;
            var stringUrl = config.VisionApiUrl + "?key=" + config.Key; ;
            using (var client = new ServiceClient())
            {
                await client.PostExecuteAsync((x) =>
                {
                    x.SetBaseUrl(stringUrl);
                    x.SetContent(myJson, Encoding.UTF8, mediaType: Core.Helper.Client.MediaType.Json);
                });
                return client.ReadAsAsync<object>();
            }
        }
    }
}
