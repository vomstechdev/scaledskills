﻿using SCLD.Core.Config;
using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using SCLD.Service.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SCLD.Service
{
    class EmailService : BaseService, IEmailService, IEmailSendingService
    {
        private readonly IRepository<EmailQueue> emailQueueRepository;
        private readonly IConfigManager configManager;
        public EmailService(IRepository<EmailQueue> _emailQueueRepository, IConfigManager _configManager)
        {
            configManager = _configManager;
            emailQueueRepository = _emailQueueRepository;
        }
        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            EmailQueue token = (EmailQueue)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                token.EmailStatus = EmailQueue.STATUS_PENDING;
                token.Retries++;
                token.SentTime = DateTime.Now.NowDate();
                token.ErrorDescription = e.Error.Message.ToString();
                emailQueueRepository.Update(token);
            }
            else
            {
                Console.WriteLine("Message sent.");
                token.EmailStatus = EmailQueue.STATUS_SENT;
                token.SentTime = DateTime.Now.NowDate();
                emailQueueRepository.Update(token);
            }

            //mailSent = true;
        }
        public void SendEmailsAsync()
        {
            try
            {
                var emailQuees = emailQueueRepository.Table.Where(x => x.EmailStatus == EmailQueue.STATUS_PENDING && x.Retries < 3).ToList();
                var tasks = new List<Task>();
                emailQuees.ForEach(x =>
                {
                    SendEmail(x);
                    //var task = Task.Run(() => SendEmail(x));
                    //tasks.Add(task);
                });
            }
            catch (Exception)
            {
            }

            //Task.WaitAll(tasks.ToArray());
        }
        private void SendEmail(EmailQueue email)
        {
            email.EmailStatus = EmailQueue.STATUS_InProgress;
            emailQueueRepository.Update(email);
            SMTPConfig sMTPConfig = configManager.GetSection<SMTPConfig>();
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                using (var client = new SmtpClient())
                {
                    client.Port = sMTPConfig.Port;
                    client.Host = sMTPConfig.Domain;
                    client.EnableSsl = true;
                    var objMessage = new MailMessage
                    {
                        From = new MailAddress(sMTPConfig.FromAddress, sMTPConfig.FromName)
                    };
                    var toEmail = email.ToEmail;
                    var displayEmail = email.DisplayName;
                    objMessage.To.Add(new MailAddress(toEmail, displayEmail, System.Text.Encoding.UTF8));
                    if (!string.IsNullOrWhiteSpace(email.CCEmail))
                    {
                        email.CCEmail.Split(";").ToList().ForEach((x) =>
                        {
                            objMessage.CC.Add(new MailAddress(x));
                        });                       
                    }
                    if (!string.IsNullOrWhiteSpace(email.BCCEmail))
                    {
                        email.BCCEmail.Split(";").ToList().ForEach(x =>
                        {
                            objMessage.Bcc.Add(new MailAddress(x));
                        });
                    }

                  // objMessage.Bcc.Add(new MailAddress("scaledskills@gmail.com", "Scaledskills", System.Text.Encoding.UTF8));                   

                    objMessage.Subject = email.Subject;
                    objMessage.BodyEncoding = objMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                    objMessage.Body = email.Body;
                    objMessage.IsBodyHtml = sMTPConfig.IsHtml;
                    var userState = email;
                    client.UseDefaultCredentials = false;//sMTPConfig.UseDefaultCredentials;
                    var cred = new NetworkCredential(sMTPConfig.User, sMTPConfig.Password);
                    client.Credentials = cred;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.Send(objMessage);
                    email.EmailStatus = EmailQueue.STATUS_SENT;
                    email.SentTime = DateTime.Now.NowDate();
                    email.SmtpServer = sMTPConfig.Domain;
                    email.SmtpPort = sMTPConfig.Port;
                    email.FromAddress = sMTPConfig.FromAddress;
                    email.UserName = sMTPConfig.User;
                    emailQueueRepository.Update(email);
                    // client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                    // System.Threading.Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                email.EmailStatus = EmailQueue.STATUS_PENDING;
                email.Retries++;
                email.SentTime = DateTime.Now.NowDate();
                email.ErrorDescription = e.Message + (e.InnerException?.Message ?? string.Empty) + e.StackTrace;
                emailQueueRepository.Update(email);
            }

            //	var add =  new Core.Models.ViewModels.AddressModel { Address1 = Name };
            //add.City = add.Address2 = add.Address3 = add.Address1;
            //add.CountryId = add.StateId = 0;
            //addressService.Save(add);
        }
        public long InsertEmail(EmailModel model)
        {
            var entity = model.ToInsertBindEntity<EmailQueue>();
            emailQueueRepository.Insert(entity);
            return entity.Id;
        }
        public long Save(EmailModel model)
        {
            if (model.Id > 0)
            {
                model.Id = UpdateEmail(model);
            }
            else
            {
                model.Id = InsertEmail(model);
            }
            return model.Id;
        }
        public long SaveUserEmail(EmailModel model)
        {
            var dbObj = emailQueueRepository.Table.FirstOrDefault(x => x.UserId == model.UserId);
            if (dbObj == null)
            {
                model.Id = InsertEmail(model);
            }
            else
            {
                model.Id = dbObj.Id;
                model.Id = UpdateEmail(model);
            }
            return model.Id;
        }
        public long UpdateEmail(EmailModel model)
        {
            var dbObj = emailQueueRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            emailQueueRepository.Update(dbObj);
            return dbObj.Id;
        }
        public string MainBody(string template)
        {
            var bodies = System.IO.File.ReadAllText(string.Format(@"{0}\Template\Email\Layout.html", System.IO.Directory.GetCurrentDirectory()));
            bodies = bodies.Replace(string.Format("{{{0}}}", "Year"), DateTime.Now.NowDate().Year.ToString());
            bodies = bodies.Replace(string.Format("{{{0}}}", "RenderBody"), template);
            return bodies;
        }
        public string GetBody(Body body, object model)
        {
            if (model != null)
            {
                //if (body == Body.ForgotPassword)
                //{
                //    var bodies = "Please reset your password by clicking <a href='{CallbackUrl}'>here</a>";
                //    var type = model.GetType();
                //    foreach (var p in type.GetProperties())
                //    {
                //        var value = p.GetValue(model, null) ?? string.Empty;
                //        bodies = bodies.Replace(string.Format("{{{0}}}", p.Name), value?.ToString());
                //    }
                //    return bodies;
                //}
                //else if (body == Body.ConfirmEmail)
                var bodies = BindProperty(body, model);
                return MainBody(bodies);
                //}
            }
            return string.Empty;

        }
        public string BindProperty(Body body, object model)
        {
            var fullPath = string.Format(@"{0}\Template\Email\{1}.html", System.IO.Directory.GetCurrentDirectory(), body.ToString());
            var bodies = System.IO.File.ReadAllText(fullPath);
            var type = model.GetType();
            foreach (var p in type.GetProperties())
            {
                var value = p.GetValue(model, null) ?? string.Empty;
                bodies = bodies.Replace(string.Format("{{{0}}}", p.Name), value?.ToString());
            }
            return bodies;
        }
    }
}
