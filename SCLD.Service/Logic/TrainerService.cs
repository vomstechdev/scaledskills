﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class TrainerService : BaseService, ITrainerService
    {
        public readonly IRepository<Trainer> trainerRepository;
        public readonly IConfigManager iConfigManager;
        public TrainerService(IRepository<Trainer> _trainerRepository,
            IConfigManager _iConfigManager)
        {
            trainerRepository = _trainerRepository;
            iConfigManager = _iConfigManager;
        }
        public TrainerModel GetTrainerById(object id)
        {
            try
            {
                return trainerRepository.GetById(id)?.ToModel<TrainerModel>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TrainerModel GetTrainerByUserId(long userId)
        {
            return trainerRepository.Table.Where(x => x.UserId == userId).FirstOrDefault()?.ToModel<TrainerModel>();
        }
        public TrainerModel GetByUrl(string url)
        {
            return trainerRepository.Table.FirstOrDefault(x => x.ProfileUrl == url)?.ToModel<TrainerModel>();
        }
        public long InsertTrainer(TrainerModel model)
        {
            //if (model.Address != null)
            //{
            //    model.AddressId = iConfigManager.GetService<IAddressService>().Save(model.Address);
            //}
            var entity = model.ToInsertBindEntity<Trainer>();
            entity.ProfileUrl = entity.ProfileUrl ?? Guid.NewGuid().ToString().ToLower();
            trainerRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateTrainer(TrainerModel model)
        {
            var dbObj = trainerRepository.GetById(model.Id);
            //if (model.Address != null)
            //{
            //    model.Address.Id = dbObj.AddressId ?? 0;
            //    model.AddressId = iConfigManager.GetService<IAddressService>().Save(model.Address);
            //}
            var updateObj = model.ToUpdateBindEntity(dbObj);
            trainerRepository.Update(updateObj);
            return dbObj.Id;
        }
        public long UpdateAbout(AboutModel model, long id)
        {
            var dbObj = trainerRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.AboutId ?? 0;
                dbObj.AboutId = iConfigManager.GetService<IAboutService>().Save(model);
            }
            trainerRepository.Update(dbObj);
            return dbObj.Id;
        }
        public bool ValidateUrl(string url,long? userId)
        {
            return trainerRepository.Table//.Where(x=>userId.HasValue ? x.UserId == userId.Value : true)
                .Any(x=>x.ProfileUrl.ToLower() == url.ToLower());
        }
        public long UpdateUserSocial(UserSocialModel model, long id)
        {
            var dbObj = trainerRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.SocialId ?? 0;
                dbObj.SocialId = iConfigManager.GetService<IUserSocialService>().Save(model);
            }
            trainerRepository.Update(dbObj);
            return dbObj.Id;
        }
        public long UpdateBankDetail(BankDetailModel model, long id)
        {
            var dbObj = trainerRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.BankDetailId ?? 0;
                dbObj.BankDetailId = iConfigManager.GetService<IBankDetailService>().Save(model);
            }
            trainerRepository.Update(dbObj);
            return dbObj.Id;
        }

    }
}
