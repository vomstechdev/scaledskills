﻿using SCLD.Core.Data;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Linq;
using System.Text;

namespace SCLD.Service.Logic
{
    partial class Purchasable : BaseService, IPurchasable
    {
        protected readonly IConfigManager iConfigManager;
        public Purchasable(IConfigManager configManager)
        {
            iConfigManager = configManager;
        }
        protected IOrderService GetByType(string type)
        {
            //Type typeObj = null;
            if (OrderRefrence.TryGetValue(type, out Type typeObj))
            {
                return (IOrderService)iConfigManager.GetService(typeObj);
            }
            return null;
        }
        private IOrderService GetOrderByType(string orderId)
        {
            var repo = iConfigManager.GetService<Core.Data.IRepository<Core.Models.DataModels.Order>>();
            var refrence = repo.Table.Where(x => x.OrderAppId == orderId.ToUpper()).Select(x => x.Reference).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(refrence)) throw new DataValidationException(x =>
            {
                x.Add("Order", new string[] { "This is not a valid Order" });
            });
            return GetByType(refrence);
        }

        public string CreateOrder(OrderModel model)
        {
            var orderService = GetByType(model.Reference);
            var orderId = orderService.CreateAsOpenOrder(model);
            return orderId;
        }

        public OrderModel GetOrder(string OrderUniqId)
        {
            var orderService = GetOrderByType(OrderUniqId);
            var orderId = orderService.GetOrder(OrderUniqId);
            return orderId;
        }

        public IOrderService GetOrderServiceByOrderId(string OrderUniqId)
        {
            return GetOrderByType(OrderUniqId);
        }

        public string PaymentAndProcessOrder(string OrderUniqId)
        {
            return ProcessOrder(OrderUniqId);
        }
        public string CancleOrder(string OrderUniqId)
        {
            var orderService = GetOrderByType(OrderUniqId);
            var orderId = CancleOrder(OrderUniqId, orderService);
            return orderId;
        }
        protected string CancleOrder(string OrderUniqId, IOrderService orderService)
        {
            var orderId = orderService.SaveAsCancleOrder(OrderUniqId);
            return orderId;
        }
        public string ProcessOrder(string OrderUniqId)
        {
            var orderService = GetOrderByType(OrderUniqId);
            var orderId = orderService.SaveAsCompleteOrder(OrderUniqId);
            return orderId;
        }
        public string SaveOrderAndMarkAsDone(PaymentModel paymentModel)
        {
            try
            {
                var userPaymentService = iConfigManager.GetService<IRepository<UserPayment>>();
                var paymentId = System.Guid.Parse(paymentModel.OrderId);
                var userPayment = userPaymentService.Table.FirstOrDefault(x => !x.IsPaid && x.PaymentId == paymentId);
                if (paymentModel.Amount == Convert.ToDouble(userPayment.Total))
                {
                    userPayment.PaymentType = paymentModel.PaymentMode;
                    userPayment.TransactionId = paymentModel.TransactionId;
                    userPayment.PaymentStatus = paymentModel.PaymentStatus;
                    userPayment.Messege = paymentModel.Messege;
                    if (userPayment.PaymentStatus == Core.Models.Enums.UserPayment_Status.SUCCESS)
                    {
                        userPayment.IsPaid = true;
                        userPaymentService.Update(userPayment);
                        var orderId = this.PaymentAndProcessOrder(userPayment.OrderId);
                    }
                    else
                    {
                        userPaymentService.Update(userPayment);
                        throw new NullReferenceException(userPayment.Messege);
                    }
                    

                    return userPayment.OrderId;
                }

                throw new NullReferenceException("Failure payment");

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public object OrderSummary(string OrderUniqId)
        {
            var orderService = GetOrderByType(OrderUniqId);
            return orderService.OrderSummary(OrderUniqId);


        }
        public UserPayment GetPayment(string paymentId)
        {
            var userPaymentService = iConfigManager.GetService<IRepository<UserPayment>>();
            var payid = Guid.Parse(paymentId);
            var userPayDetail = userPaymentService.Table.FirstOrDefault(x => x.PaymentId == payid);
            return userPayDetail;
        }
    }
}
