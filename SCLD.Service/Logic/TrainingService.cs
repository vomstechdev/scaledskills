﻿using SCLD.Core.Data;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Resource;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCLD.Service.Logic
{
    class TrainingService : BaseService, ITrainingService
    {
        public readonly IRepository<Training> TrainingRepository;
        public readonly IConfigManager iConfigManager;
        readonly IRepository<TrainingTicket> TrainingTicketRepository;
        readonly IRepository<TrainingReview> TrainingReviewRepository;
        readonly IRepository<TrainingTrainer> TrainingTrainerRepository;
        readonly IRepository<TrainingPromotion> TrainingPromotionRepository;
        readonly IRepository<TrainingSetting> TrainingSettingRepository;
        readonly IRepository<TrainingImage> TrainingImageRepository;
        readonly IUserService userService;
       
        public TrainingService(IRepository<Training> _TrainingRepository,
            IRepository<TrainingTicket> _TrainingTicketRepository,
            IRepository<TrainingTrainer> _TrainingTrainerRepository, IRepository<TrainingPromotion> _TrainingPromotionRepository,
            IRepository<TrainingSetting> _TrainingSettingRepository,
            IRepository<TrainingImage> _TrainingImageRepository,
            IRepository<TrainingReview> _TrainingReviewRepository,
        IConfigManager _iConfigManager, IUserService _userService)
        {
            TrainingRepository = _TrainingRepository;
            iConfigManager = _iConfigManager;
            TrainingTicketRepository = _TrainingTicketRepository;
            TrainingTrainerRepository = _TrainingTrainerRepository;
            TrainingPromotionRepository = _TrainingPromotionRepository;
            TrainingSettingRepository = _TrainingSettingRepository;
            TrainingImageRepository = _TrainingImageRepository;
            TrainingReviewRepository = _TrainingReviewRepository;
            userService = _userService;
        }
        public TrainingModel GetTrainingById(object id)
        {
            var model = TrainingRepository.GetById(id)?.ToModel<TrainingModel>();
            model.HostedByListObj = Enum.GetValues(typeof(Training_Hosted)).Cast<Training_Hosted>().Select(v => new SelectItem
            {
                Text = v.ToString(),
                IsSelect = model.HostedBy == (int)v,
                Value = ((int)v).ToString()
                
            }).Where(x=>x.IsSelect).ToList();
            if (model.HostedBy == 2)
            {
                model.OrganizationList = GetOrganizationList(model.Id).Select(x => x.Id);
                model.OrganizationListObj = GetOrganizationList(model.Id).Select(x => new SelectItem { Text = x.Name, Value = x.Id.ToString() });
            }
            return model;
        }
        public IEnumerable<Training> GetTrainingByUserId(long id)
        {
            var data = TrainingRepository.GetFromSql<Core.Models.DataModels.Query.MasterQ>("Exec GetMasterQ").ToList();
            return TrainingRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertTraining(TrainingModel model)
        {
            var entity = model.ToInsertBindEntity<Training>();
            TrainingRepository.Insert(entity);
            return entity.Id;
        }
        public long SaveTrainingOrganization(TrainingModel model)
        {
            var entityRepo = iConfigManager.GetService<IRepository<TrainingOrganization>>();
            var date = DateTime.Now.NowDate();
            if (entityRepo.Table.Any(x => x.TrainingId == model.Id))
            {
                var data = entityRepo.Table.FirstOrDefault(x => x.TrainingId == model.Id);
                data.ModifiedDate = date;
                data.OrganizationId = model.OrganizationList.FirstOrDefault();
                entityRepo.Update(data);
                return data.Id;
            }
            else
            {
                var result = model.OrganizationList.Select(x => new TrainingOrganization
                {
                    CreateDate = date,
                    ModifiedDate = date,
                    IsDelete = false,
                    OrganizationId = x,
                    Status = true,
                    TrainingId = model.Id
                });
                entityRepo.Insert(result);
                return result.First().Id;
            }
        }
        public IEnumerable<OrganizationattrResult> GetOrganizationList(long TrainingId)
        {
            var entityRepo = iConfigManager.GetService<IRepository<TrainingOrganization>>().Table;
            var orgentityRepo = iConfigManager.GetService<IRepository<Organization>>().Table;
            var result = (from torg in entityRepo
                          join org in orgentityRepo
                          on torg.OrganizationId equals org.Id
                          where torg.TrainingId == TrainingId
                          select new OrganizationattrResult { Name = org.Name, Id = org.Id, 
                              URL = org.ProfileUrl, Email = org.Email, PhoneNumber = org.PhoneNumber
                          });
            return result.ToList();
        }

        public long UpdateTraining(TrainingModel model)
        {
            var dbObj = TrainingRepository.GetById(model.Id);
            model.UserId = dbObj.UserId;
            var updateObj = model.ToUpdateBindEntity(dbObj);
            TrainingRepository.Update(dbObj);
            return dbObj.Id;
        }
        public long UpdateAbout(AboutModel model, long id)
        {
            var dbObj = TrainingRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.AboutId ?? 0;
                dbObj.AboutId = iConfigManager.GetService<IAboutService>().Save(model);
            }
            TrainingRepository.Update(dbObj);
            return dbObj.Id;
        }
        public bool ValidateUrl(string url, long? userId)
        {
            return TrainingRepository.Table//.Where(x=>userId.HasValue ? x.UserId == userId.Value : true)
                .Any(x => x.Url.ToLower() == url.ToLower());
        }
        public AboutModel GetTicketAbout(long id)
        {
            var dbObj = TrainingRepository.GetById(id);
            return iConfigManager.GetService<IAboutService>().GetTagById(dbObj.AboutId);
        }
        public long UpdateAddress(TrainingLocation model, long id)
        {
            var dbObj = TrainingRepository.GetById(id);
            if (model.ModeType == TrainingModeType.Offline)
            {
                if (model?.AddressModel != null)
                {
                    dbObj.ModeType = model.ModeType;
                    model.AddressModel.Id = dbObj.AddressId ?? 0;
                    model.AddressModel.UserId = dbObj.UserId;
                    dbObj.AddressId = iConfigManager.GetService<IAddressService>().Save(model.AddressModel);
                }
            }
            else
            {
                dbObj.OnlineLocation = model.OnlineLocation;
                dbObj.LocationDetail = model.LocationDetail;
            }
            dbObj.IsOnlineDetailsVisible = model.IsOnlineDetailsVisible;
            dbObj.ModeType = model.ModeType;
            TrainingRepository.Update(dbObj);
            return dbObj.Id;
        }
        public TrainingLocation GetTrainingAddress(long id)
        {
            var model = new TrainingLocation();
            var dbObj = TrainingRepository.GetById(id);
            model.ModeType = dbObj.ModeType;
            model.OnlineLocation = dbObj.OnlineLocation;
            model.LocationDetail = dbObj.LocationDetail;
            model.IsOnlineDetailsVisible = dbObj.IsOnlineDetailsVisible;
            model.AddressModel = iConfigManager.GetService<IAddressService>().GetAddressById(dbObj.AddressId);
            return model;
        }
        public TrainingTicketModel GetTicketById(object id)
        {
            return TrainingTicketRepository.GetById(id)?.ToModel<TrainingTicketModel>();
        }
        public IEnumerable<TrainingTicketModel> GetTrainingTicketByTicketId(long id)
        {
            return TrainingTicketRepository.Table.Where(x => x.TrainingId == id).ToList().Select(x => x.ToModel<TrainingTicketModel>());
        }
        public long InsertTrainingTicket(TrainingTicketModel model)
        {
            var entity = model.ToInsertBindEntity<TrainingTicket>();
            entity.AvailableQty = entity.QTY;
            TrainingTicketRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateTrainingTicket(TrainingTicketModel model)
        {
            var dbObj = TrainingTicketRepository.GetById(model.Id);
            model.TrainingId = dbObj.TrainingId;
            var avilableqty = dbObj.QTY - dbObj.AvailableQty;
            avilableqty = model.QTY - avilableqty;
            var updateObj = model.ToUpdateBindEntity(dbObj);
            updateObj.AvailableQty = avilableqty;
            TrainingTicketRepository.Update(updateObj);
            if (updateObj.TicketType == 1)
            {
                var ticketPaymentDetailRepo = iConfigManager.GetService<IRepository<TicketPaymentDetail>>();
                ticketPaymentDetailRepo.Delete(ticketPaymentDetailRepo.Table.Where(x => x.TicketId == updateObj.Id));
            }
            return dbObj.Id;
        }
        public IEnumerable<TrainingTrainer> GetTrainingTrainersTicket(long TrainingId)
        {
            return TrainingTrainerRepository.TableNoTracking.Where(x => !x.IsDelete && x.TrainerId == TrainingId).ToList();
        }
        public void AddTrainingTrainer(long TrainersId, long TrainingId)
        {   
            var results = TrainingTrainerRepository.Table.Where(x => x.TrainingId == TrainingId && x.TrainerId == TrainersId);
            TrainingTrainerRepository.Delete(results);
            TrainingTrainerRepository.Insert(new TrainingTrainer { TrainerId = TrainersId, TrainingId = TrainingId, CreateDate = DateTime.Now.NowDate(), Status = true });
        }

        public async Task PublishTrainingAsync(PublishTrainingModel publishTrainingModel)
        {
            ValidateTraining(publishTrainingModel.TrainingId);
            var results = TrainingRepository.Table.FirstOrDefault(x => x.Id == publishTrainingModel.TrainingId);
            results.ModifiedDate = DateTime.Now.NowDate();
            results.TrainingStatus = publishTrainingModel.Status;
            results.IsUserAccept = publishTrainingModel.IsAccept;
            results.IsPromotion = publishTrainingModel.IsPromotion;
            TrainingRepository.Update(results);

            var trainingDetail = (from training in TrainingRepository.Table
             join trainingTrainer in TrainingTrainerRepository.Table
             on training.Id equals trainingTrainer.TrainingId
             into trainer
             from trainingTranerDetail in trainer.DefaultIfEmpty()

             where training.Id == publishTrainingModel.TrainingId
             select new { training, trainingTrainer = trainingTranerDetail }).FirstOrDefault();

            TrainingModel trainingModel = trainingDetail.training.ToModel<TrainingModel>();
            trainingModel.TrainerId = trainingDetail.trainingTrainer?.TrainerId ?? 0;
            TrainingImageModel trainingImage = GetTrainingImage(publishTrainingModel.TrainingId);
            await userService.SendMailToSubscriber(trainingModel, trainingImage);
        }

        

        private void ValidateTraining(long trainingId)
        {
            if (!TrainingTicketRepository.Table.Any(x => x.TrainingId == trainingId))
            {
                throw new DataValidationException(x =>
                {
                    x.Add("TicketNotAvilable", new string[] { ValidationMSG.Training_TicketNotAvilable});

                });
            }
            else if (!TrainingRepository.Table.Any(x=>x.Id == trainingId 
            && (x.ModeType == TrainingModeType.Offline ? x.AddressId.HasValue : !string.IsNullOrEmpty(x.OnlineLocation))))
            {
                throw new DataValidationException(x =>
                {
                    x.Add("Location", new string[] { ValidationMSG.Training_Location });

                });
            }
        }
        public void DeleteTrainingTrainer(long TrainersIds, long TrainingId)
        {
            var results = TrainingTrainerRepository.Table.Where(x => x.TrainingId == TrainingId && x.TrainerId == TrainersIds);
            TrainingTrainerRepository.Delete(results);
        }
        public long SaveTrainingPromotion(TrainingPromotionModel model)
        {
            var dbObj = TrainingPromotionRepository.TableNoTracking.Where(x => x.TrainingId == model.TrainingId);
            if (dbObj.Any())
            {
                model.Id = dbObj.First().Id;
                TrainingPromotionRepository.Update(model.ToInsertBindEntity<TrainingPromotion>());
            }
            else
                TrainingPromotionRepository.Insert(model.ToInsertBindEntity<TrainingPromotion>());
            return model.Id;
        }
        public TrainingPromotionModel GetTrainingPromotion(long id)
        {
            var dbObj = TrainingPromotionRepository.TableNoTracking.FirstOrDefault(x => x.TrainingId == id);
            if (dbObj == null)
            {

            }
            return dbObj?.ToModel<TrainingPromotionModel>() ?? new TrainingPromotionModel();
        }
        public long SaveTrainingImage(TrainingImageModel model)
        {
            var dbObj = TrainingImageRepository.TableNoTracking.Where(x => x.TrainingId == model.TrainingId);
            if (dbObj.Any())
            {
                var df = dbObj.First();
                model.Id = df.Id;
                TrainingImageRepository.Update(model.ToUpdateBindEntity<TrainingImage>(df));
            }
            else
                TrainingImageRepository.Insert(model.ToInsertBindEntity<TrainingImage>());
            return model.Id;
        }
        public TrainingImageModel GetTrainingImage(long id)
        {
            var dbObj = TrainingImageRepository.TableNoTracking.FirstOrDefault(x => x.TrainingId == id);
            var model = dbObj?.ToModel<TrainingImageModel>() ?? new TrainingImageModel();
            var keys = iConfigManager.GetSection<AppKeys>();
            if (model != null)
            {
                model.CardUrl = Core.Helper.Util.GetTrainingPreview(model.Card);
                model.HeaderUrl = Core.Helper.Util.GetTrainingPreview(model.Header);
            }

            return model;
        }
        public long SaveTrainingSetting(TrainingSettingModel model)
        {
            var dbObj = TrainingSettingRepository.TableNoTracking.Where(x => x.TrainingId == model.TrainingId);
            if (dbObj.Any())
            {
                model.Id = dbObj.First().Id;
                TrainingSettingRepository.Update(model.ToInsertBindEntity<TrainingSetting>());
            }
            else
                TrainingSettingRepository.Insert(model.ToInsertBindEntity<TrainingSetting>());
            return model.Id;
        }
        public TrainingSettingModel GetTrainingSetting(long id)
        {
            var dbObj = TrainingSettingRepository.TableNoTracking.FirstOrDefault(x => x.TrainingId == id);
            return dbObj?.ToModel<TrainingSettingModel>() ?? new TrainingSettingModel();
        }
        public TrainingReviewModel GetReviewById(object id)
        {
            return TrainingReviewRepository.GetById(id)?.ToModel<TrainingReviewModel>();
        }
        public TrainingReviewModel GetReviewByUserIdAndTrainingId(long trainingId, long userId)
        {
            return TrainingReviewRepository.Table.FirstOrDefault(x => x.UserId == userId && x.TrainingId == trainingId)?.ToModel<TrainingReviewModel>();
        }

        public IEnumerable<TrainingReviewModel> GetTrainingReviewByTrainingId(long id)
        {
            return TrainingReviewRepository.Table.Where(x => x.TrainingId == id).ToList().Select(x => x.ToModel<TrainingReviewModel>());
        }

        public long InsertTrainingReview(TrainingReviewModel model)
        {
            var entity = model.ToInsertBindEntity<TrainingReview>();
            TrainingReviewRepository.Insert(entity);
            return entity.Id;
        }

        public long UpdateTrainingReview(TrainingReviewModel model)
        {
            var dbObj = TrainingReviewRepository.GetById(model.Id);
            model.TrainingId = dbObj.TrainingId;
            var updateObj = model.ToUpdateBindEntity(dbObj);
            TrainingReviewRepository.Update(dbObj);
            return dbObj.Id;
        }

        public int SetTrainingInterast(TrainingInterested trainingInterested)
        {
            var repo = iConfigManager.GetService<IRepository<TrainingInterested>>();
            var userTra = repo.TableNoTracking.FirstOrDefault(x => x.UserId == trainingInterested.UserId
             && x.TrainingId == trainingInterested.TrainingId);
            if (userTra != null)
            {
                userTra.IsInterested = trainingInterested.IsInterested;
                repo.Update(userTra);
            }
            else
            {
                userTra = trainingInterested;
                userTra.ModifiedDate = userTra.CreateDate = DateTime.Now.NowDate();
                repo.Update(userTra);
            }
            return repo.TableNoTracking.Count(x => x.IsInterested && x.TrainingId == trainingInterested.TrainingId);

        }
    }
}
