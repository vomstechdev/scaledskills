﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    abstract class OrderService : BaseService, IOrderService
    {
        protected readonly IConfigManager iConfigManager;
        protected readonly IRepository<Order> orderRepo;
        protected readonly IRepository<OrderItem> orderItemRepo;
        public OrderService(IConfigManager configManager)
        {
            iConfigManager = configManager;
            orderRepo = configManager.GetService<IRepository<Order>>();
            orderItemRepo = configManager.GetService<IRepository<OrderItem>>();
        }

        public string CreateAsOpenOrder(OrderModel orderModel)
        {
            var order = orderModel.ToInsertBindEntity<Order>();
            var listOfItems = Enumerable.Empty<OrderItem>().ToList();
            orderModel.Items.ToList().ForEach(x =>
            {
                listOfItems.Add(((OrderAttrItemModel)x).ToInsertBindEntity<OrderItem>());
            });
            OnOrderValidate(orderModel, order, listOfItems);
            order.OrderStatus = Core.Models.Enums.OrderStatus.Open;
            SaveOrder(orderModel,order, listOfItems);
            return order.OrderAppId;
        }
        private void SaveOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            order = OnSaveOrder(orderModel, order, orderItem);
            if (order.Id > 0)
            {
                orderRepo.Update(order);
                OnCompleteSaveOrder(orderModel, order, orderItem);
                //orderItem = orderItem.Select(x => { x.OrderId = order.Id; return x; });
                //orderItemRepo.Update(orderItem);
            }
            else
            {
                
                orderRepo.Insert(order);
                orderItem = orderItem.Select(x => { x.OrderId = order.Id; return x; });
                orderItemRepo.Insert(orderItem);
                OnCompleteSaveOrder(orderModel, order, orderItem);
            }
            

        }

        public abstract void OnOrderValidate(OrderModel model, Order orderModel, IEnumerable<OrderItem> orderItems);
        protected virtual Order OnSaveOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            return order;
        }
        protected virtual Order OnCompleteSaveOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            return order;
        }

        public string SaveAsCompleteOrder(string OrderUniqId)
        {
            var order = orderRepo.Table.FirstOrDefault(x => x.OrderAppId == OrderUniqId);
            order.OrderStatus = Core.Models.Enums.OrderStatus.Complete;
            var OrderItem = orderItemRepo.Table.Where(x => x.OrderId == order.Id);
            SaveOrder(null, order, OrderItem);
            return order.OrderAppId;
        }
        public virtual object OrderSummary(string OrderUniqId)
        {
            return null;
        }

        public string SaveAsPendingOrder(string OrderUniqId)
        {
            var order = orderRepo.Table.FirstOrDefault(x => x.OrderAppId == OrderUniqId);
            order.OrderStatus = Core.Models.Enums.OrderStatus.Pending;
            var OrderItem = orderItemRepo.Table.Where(x => x.OrderId == order.Id);
            SaveOrder(null, order, OrderItem);
            return order.OrderAppId;
        }
        public string SaveAsCancleOrder(string OrderUniqId)
        {
            var order = orderRepo.Table.FirstOrDefault(x => x.OrderAppId == OrderUniqId);
            order.OrderStatus = Core.Models.Enums.OrderStatus.Cancel;
            order.ModifiedDate = DateTime.Now.NowDate();
            var OrderItem = orderItemRepo.Table.Where(x => x.OrderId == order.Id);
            SaveOrder(null, order, OrderItem);
            return order.OrderAppId;
        }
        protected virtual OrderModel OnGetOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            return orderModel;
        }
        public OrderModel GetOrder(string OrderUniqId)
        {
            var order = orderRepo.Table.FirstOrDefault(x => x.OrderAppId == OrderUniqId);
            var model = order.ToModel<OrderModel>();
            var orderItems = orderItemRepo.Table.Where(x => x.OrderId == order.Id).ToList();
            model.Items = orderItems.Select(x => x.ToModel<OrderItemModel<object>>());
            return OnGetOrder(model, order, orderItems);
        }
    }
}
