﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class AddressService : BaseService, IAddressService
    {
        public readonly IRepository<Address> addressRepository;
        readonly IRepository<MasterData> masterDataRepository;
        public AddressService(IRepository<Address> _addressRepository,
            IRepository<MasterData> _masterDataRepository)
        {
            addressRepository = _addressRepository;
            masterDataRepository = _masterDataRepository;
        }
        public AddressModel GetAddressById(object id)
        {
            id = id ?? default(long);
            long Objid = Convert.ToInt64(id);
            var address = (from  userAddress in addressRepository.Table

                              join userCountry in masterDataRepository.Table
                               on userAddress.CountryId equals (int)userCountry.Id
                               into userCountryss
                               from userCountrys in userCountryss.DefaultIfEmpty()

                               join userState in masterDataRepository.Table
                               on userAddress.StateId equals (int)userState.Id
                               into userStatess
                               from userStates in userStatess.DefaultIfEmpty()
                               where userAddress.Id == Objid
                           select new { userAddress, CountryName = userCountrys.Name, StateName = userStates.Name }).FirstOrDefault();
            var model = address?.userAddress?.ToModel<AddressModel>();
            if (model != null)
            {
                model.CountryObj = new Core.Models.SelectModelItem
                {
                    IsSelect = true,
                    Text = address.CountryName,
                    Value = model.CountryId.ToString()
                };
                model.StateObj = new Core.Models.SelectModelItem
                {
                    IsSelect = true,
                    Text = address.StateName,
                    Value = model.StateId.ToString()
                };
            }
            return model;
        }
        public IEnumerable<Address> GetAddressByUserId(long id)
        {
            return addressRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertAddress(AddressModel model)
        {
            var entity = model.ToInsertBindEntity<Address>();
            addressRepository.Insert(entity);
            return entity.Id;
        }
        public long Save(AddressModel model)
        {
            if (model.Id > 0)
            { 
                model.Id = UpdateAddress(model);
            }
            else
            {
                model.Id= InsertAddress(model);
            }
            return model.Id;
        }
		public long SaveUserAddress(AddressModel model)
		{
			var dbObj = addressRepository.Table.FirstOrDefault(x => x.UserId == model.UserId);
			if (dbObj == null)
			{
				model.Id = InsertAddress(model);
			}
			else
			{
				model.Id = dbObj.Id;
				model.Id = UpdateAddress(model);
			}
			return model.Id;
		}
		public long UpdateAddress(AddressModel model)
        {
            var dbObj = addressRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            addressRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
