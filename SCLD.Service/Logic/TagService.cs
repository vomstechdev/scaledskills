﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
	class AboutService : BaseService, IAboutService
	{
		readonly IRepository<About> aboutRepository;
		public AboutService(IRepository<About> _aboutRepository)
		{
			aboutRepository = _aboutRepository;
		}
		public AboutModel GetTagById(object id)
		{
			return aboutRepository.GetById(id)?.ToModel<AboutModel>();
		}
		public IEnumerable<About> GetTagByEntityId(long id)
		{
			return aboutRepository.Table.Where(x => x.EntityId == id);
		}
		public long InsertAbout(AboutModel model)
		{
			var entity = model.ToInsertBindEntity<About>();
			aboutRepository.Insert(entity);
			return entity.Id;
		}
		public long UpdateAbout(AboutModel model)
		{
			var dbObj = aboutRepository.GetById(model.Id);
			var updateObj = model.ToUpdateBindEntity(dbObj);
			aboutRepository.Update(dbObj);
			return dbObj.Id;
		}
		public long Save(AboutModel model)
		{
			if (model.Id > 0)
			{
				model.Id = UpdateAbout(model);
			}
			else
			{
				model.Id = InsertAbout(model);
			}
			return model.Id;
		}
	}
}
