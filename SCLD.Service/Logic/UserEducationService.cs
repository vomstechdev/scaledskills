﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class UserEducationService : BaseService, IUserEducationService
    {
        public readonly IRepository<UserEducation> UserEducationRepository;
        public UserEducationService(IRepository<UserEducation> _UserEducationRepository)
        {
            UserEducationRepository = _UserEducationRepository;
        }
        public UserEducationModel GetUserEducationById(object id)
        {
            return UserEducationRepository.GetById(id)?.ToModel<UserEducationModel>();
        }
        public IEnumerable<UserEducation> GetUserEducationByUserId(long id)
        {
            return UserEducationRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertUserEducation(UserEducationModel model)
        {
            var entity = model.ToInsertBindEntity<UserEducation>();
            UserEducationRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateUserEducation(UserEducationModel model)
        {
            var dbObj = UserEducationRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            UserEducationRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
