﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using SCLD.Service.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service
{
    class TicketService : BaseService, ITicketService
    {
        readonly IRepository<TicketPaymentDetail> ticketPaymentDetailRepo;
        public TicketService(IRepository<TicketPaymentDetail> ticketPaymentDetailRepo)
        {
            this.ticketPaymentDetailRepo = ticketPaymentDetailRepo;
        }
       public IEnumerable<TicketPaymentDetailModel> GetTicketPaymentDetails(long? ticketId)
        {
           var result =  ticketPaymentDetailRepo.GetFromSql<TicketPaymentDetailModel>($"TicketPaymentDetail_Proc {ticketId}");
            return result.ToList();
        }
        public UserPaymentResult GetTicketPaymentPreview(TicketDetailData ticketDetailData)
        {
            var UserPaymentResult = new UserPaymentResult();
            UserPaymentResult.BuyerTotal = ticketDetailData.TotalAmount;
            UserPaymentResult.OrganizerTotal = ticketDetailData.TotalAmount;
            UserPaymentResult.SubTotal = ticketDetailData.TotalAmount;
            Action<decimal, TicketPaymentDetailModel> d = (x, Model) =>
            {
                if (Model.ModeType == Core.Models.Enums.Payment_ModeType.Organizer)
                {
                    UserPaymentResult.OrganizerTotal = UserPaymentResult.OrganizerTotal - x;
                   // UserPaymentResult.SubTotal = UserPaymentResult.SubTotal - x;
                }
                else
                    UserPaymentResult.BuyerTotal = UserPaymentResult.BuyerTotal + x;
            };
            ticketDetailData.PaymentDetails.ToList().ForEach(x =>
            {
                if (x.FeeType == Core.Models.Enums.Payment_FeeType.ServiceTax)
                {
                    UserPaymentResult.ServiceTax = CalculateData(x,ticketDetailData.TotalAmount);
                    d(UserPaymentResult.ServiceTax, x);
                }
                else if (x.FeeType == Core.Models.Enums.Payment_FeeType.AffilationMarketing)
                {
                    UserPaymentResult.Affilation = CalculateData(x, ticketDetailData.TotalAmount);
                    d(UserPaymentResult.Affilation, x);
                }
                else if (x.FeeType == Core.Models.Enums.Payment_FeeType.GST)
                {
                    UserPaymentResult.GST = CalculateData(x, ticketDetailData.TotalAmount);
                    d(UserPaymentResult.GST, x);
                }
                else if (x.FeeType == Core.Models.Enums.Payment_FeeType.GateWay)
                {
                    UserPaymentResult.Getwayfee = CalculateData(x, ticketDetailData.TotalAmount);
                    d(UserPaymentResult.Getwayfee, x);
                }
            });
            UserPaymentResult.Total = Math.Max(UserPaymentResult.BuyerTotal, UserPaymentResult.BuyerTotal);
            return UserPaymentResult;
               
        }
        private decimal CalculateData(TicketPaymentDetailModel data, decimal totalAmount)
        {
            
            return (data.IsPercentage ? Math.Round((data.Amount * totalAmount) / 100,2) : data.Amount);
        }

        public UserPaymentResult GetTicketPaymentPreview(long? ticketId)
        {
            return GetTicketPaymentPreview(new TicketDetailData());
        }

        public void SaveTicketPaymentDetail(IEnumerable<TicketPaymentDetailModel> ticketPaymentDetails)
        {
            var data = ticketPaymentDetailRepo.Table.Where(x => x.TicketId == ticketPaymentDetails.First().TicketId);
            foreach (var detail in ticketPaymentDetails)
            {
                if (detail.Id == 0)
                {
                    ticketPaymentDetailRepo.Insert(detail.ToEntity<TicketPaymentDetail>());
                }
                else
                {
                    var dbObj = data.FirstOrDefault(x => x.Id == detail.Id);
                    ticketPaymentDetailRepo.Update(detail.ToUpdateBindEntity(dbObj));
                }
            }
        }

        
    }
}
