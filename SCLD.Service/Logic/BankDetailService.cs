﻿using SCLD.Core.Data;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
	class BankDetailService : BaseService, IBankDetailService
	{
		 readonly IRepository<BankDetail> BankDetailRepository;
         readonly IConfigManager configManager;
        public BankDetailService(IRepository<BankDetail> _BankDetailRepository, IConfigManager _configManager)
		{
			BankDetailRepository = _BankDetailRepository;
            configManager = _configManager;
        }
		public BankDetailModel GetBankDetailById(object id)
		{
			var model =  BankDetailRepository.GetById(id)?.ToModel<BankDetailModel>();
            var key = configManager.GetSection<AppKeys>();
            if (model != null)
            {
                model.ExemptionDocUrl = Util.GetDocPreview(model.ExemptionDocId, key.ApiUrl);
                model.PanCardDocUrl = Util.GetDocPreview(model.PanCardDocId, key.ApiUrl);
                model.AdharCardDocUrl = Util.GetDocPreview(model.AdharCardDocId, key.ApiUrl);
                model.CancellationDocUrl = Util.GetDocPreview(model.CancellationDocId, key.ApiUrl);
            }
            
            return model;
		}
		public BankDetailModel GetBankDetailByUserId(long id)
		{
            var model = BankDetailRepository.Table.FirstOrDefault(x => x.UserId == id)?.ToModel<BankDetailModel>();
            var key = configManager.GetSection<AppKeys>();
            if (model != null)
            {
                model.ExemptionDocUrl = Util.GetDocPreview(model.ExemptionDocId, key.ApiUrl);
                model.PanCardDocUrl = Util.GetDocPreview(model.PanCardDocId, key.ApiUrl);
                model.AdharCardDocUrl = Util.GetDocPreview(model.AdharCardDocId, key.ApiUrl);
                model.CancellationDocUrl = Util.GetDocPreview(model.CancellationDocId, key.ApiUrl);
            }
            return model;
		}
		public long InsertBankDetail(BankDetailModel model)
		{
			var entity = model.ToInsertBindEntity<BankDetail>();
			BankDetailRepository.Insert(entity);
			return entity.Id;
		}
		public long UpdateBankDetail(BankDetailModel model)
		{
			var dbObj = BankDetailRepository.GetById(model.Id);
			var updateObj = model.ToUpdateBindEntity(dbObj);
			BankDetailRepository.Update(dbObj);
			return dbObj.Id;
		}
		public long Save(BankDetailModel model)
		{
			if (model.Id > 0)
			{
				model.Id = UpdateBankDetail(model);
			}
			else
			{
				model.Id = InsertBankDetail(model);
			}
			return model.Id;
		}
	}
}
