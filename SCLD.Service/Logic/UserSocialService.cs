﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class UserSocialService : BaseService, IUserSocialService
    {
        public readonly IRepository<UserSocial> UserSocialRepository;
        public UserSocialService(IRepository<UserSocial> _UserSocialRepository)
        {
            UserSocialRepository = _UserSocialRepository;
        }
        public UserSocialModel GetUserSocialById(object id)
        {
            return UserSocialRepository.GetById(id)?.ToModel<UserSocialModel>();
        }
        public IEnumerable<UserSocial> GetUserSocialByUserId(long id)
        {
            return UserSocialRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertUserSocial(UserSocialModel model)
        {
            var entity = model.ToInsertBindEntity<UserSocial>();
            UserSocialRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateUserSocial(UserSocialModel model)
        {
            var dbObj = UserSocialRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            UserSocialRepository.Update(dbObj);
            return dbObj.Id;
        }
		public long Save(UserSocialModel model)
		{
			if (model.Id > 0)
			{
				model.Id = UpdateUserSocial(model);
			}
			else
			{
				model.Id = InsertUserSocial(model);
			}
			return model.Id;
		}
	}
}
