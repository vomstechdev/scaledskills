﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class TrainingImageService : BaseService, ITrainingImageService
    {
        public readonly IRepository<TrainingImage> TrainingImageRepository;
        public TrainingImageService(IRepository<TrainingImage> _TrainingImageRepository)
        {
            TrainingImageRepository = _TrainingImageRepository;
        }
        public TrainingImageModel GetTrainingImageById(object id)
        {
            return TrainingImageRepository.GetById(id)?.ToModel<TrainingImageModel>();
        }
        public IEnumerable<TrainingImage> GetTrainingImageByTrainingId(long id)
        {
            return TrainingImageRepository.Table.Where(x => x.TrainingId == id);
        }
        public long InsertTrainingImage(TrainingImageModel model)
        {
            var entity = model.ToInsertBindEntity<TrainingImage>();
            TrainingImageRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateTrainingImage(TrainingImageModel model)
        {
            var dbObj = TrainingImageRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            TrainingImageRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
