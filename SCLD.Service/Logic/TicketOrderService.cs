﻿using SCLD.Core.Data;
using SCLD.Core.Helper;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    internal class TicketOrderService : OrderService, ITicketOrderService
    {
        protected readonly IRepository<Training> trainingRepo;
        protected readonly IRepository<TrainingOrder> trainingOrderRepo;
        protected readonly IRepository<TrainingTicket> trainingTicketRepo;
        IConfigManager configManager;
        public TicketOrderService(IConfigManager _configManager) : base(_configManager)
        {
            trainingRepo = _configManager.GetService<IRepository<Training>>();
            trainingTicketRepo = _configManager.GetService<IRepository<TrainingTicket>>();
            trainingOrderRepo = _configManager.GetService<IRepository<TrainingOrder>>();
            configManager = _configManager;
        }

        protected override OrderModel OnGetOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            if (orderModel.Items?.Any() == true)
            {
                var items = orderModel.Items.Select(x => x.ItemId);
                var ticketlist = trainingTicketRepo.Table.Where(x => items.Contains(x.Id)).ToList();
                orderModel.Items = orderModel.Items.Select(x => { x.Item = ticketlist.FirstOrDefault(y => y.Id == x.ItemId)?.ToModel<TrainingTicketModel>(); return x; });
            }
            return base.OnGetOrder(orderModel, order, orderItem);
        }
        protected override Order OnSaveOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            if (order.OrderStatus == Core.Models.Enums.OrderStatus.Pending || order.OrderStatus == Core.Models.Enums.OrderStatus.Complete)
            {
                orderItem = orderItem.ToList();
                if (orderItem.Any() && orderItem.Sum(x => x.UnitPrice) > 0)
                {
                    var userPaymentRepo = iConfigManager.GetService<IRepository<UserPayment>>();
                    var userPayment = userPaymentRepo.Table.FirstOrDefault(x => x.OrderId == order.OrderAppId);
                    if (userPayment == null)
                    {
                        userPayment = CreateUserPayment(orderItem, order);
                        userPaymentRepo.Insert(userPayment);
                        throw new PaymentRedirect(userPayment.PaymentId.ToString());
                    }
                    else if (userPayment.IsPaid)
                    {
                        order.OrderStatus = Core.Models.Enums.OrderStatus.Complete;
                    }
                    else
                    throw new PaymentRedirect(userPayment.PaymentId.ToString());
                }
            }
            return base.OnSaveOrder(orderModel, order, orderItem);
        }
        public override void OnOrderValidate(OrderModel model, Order orderModel, IEnumerable<OrderItem> orderItems)
        {
            orderItems = orderItems.ToList();
            var validation = new Dictionary<string, string[]>();
            var item = orderItems.ToList().Select(x => x.ItemId).ToList();
            var tickets = trainingTicketRepo.Table.Where(x => item.Contains(x.Id)).ToList();

            orderItems.ToList().ForEach(x =>
            {
                var errorList = new List<string>();
                var ticket = tickets.SingleOrDefault(y => y.Id == x.ItemId);
                if (ticket != null)
                {
                    if (ticket.AvailableQty < x.Qty)
                    {
                        errorList.Add("This ticket is not avilable in stock");
                    }
                    else if (!(ticket.MinBooking <= x.Qty && (!ticket.MaxBooking.HasValue || ticket.MaxBooking >= x.Qty)))
                    {
                        errorList.Add($"Quantity should between {ticket.MinBooking} and {(ticket.MaxBooking.HasValue ? ticket.MaxBooking : ticket.AvailableQty)}");
                    }

                }
                if (errorList.Any())
                {
                    validation.Add(x.ItemId.ToString(), errorList.ToArray());
                }
            });
            if (validation.Any())
            {
                throw new DataValidationException(validation);
            }
        }
        protected override Order OnCompleteSaveOrder(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem)
        {
            if (order.OrderStatus == Core.Models.Enums.OrderStatus.Complete)
            {
                orderItem = orderItem.ToList();
                var item = orderItem.Select(x => x.ItemId).FirstOrDefault();
                var trainingId = trainingTicketRepo.Table.Where(x => x.Id == item).Select(x => x.TrainingId).FirstOrDefault();
                trainingOrderRepo.Insert(new TrainingOrder
                {
                    CreateDate = order.ModifiedDate.Value,
                    ModifiedDate = order.ModifiedDate.Value,
                    OrderId = order.Id,
                    TrainingId = trainingId,
                    Status = true,
                    UserId = order.UserId
                });
                SendTrainingRegisterEmail(orderModel, order, orderItem, trainingId);

            }
            else if (order.OrderStatus == Core.Models.Enums.OrderStatus.Open)
            {
                orderItem = orderItem.ToList();
                var item = orderItem.Select(x => x.ItemId).ToList();
                var tickets = trainingTicketRepo.Table.Where(x => item.Contains(x.Id));
                tickets.ToList().ForEach(x =>
                {
                    x.AvailableQty = x.AvailableQty - orderItem.FirstOrDefault(y => y.ItemId == x.Id).Qty;
                });
                trainingTicketRepo.Update(tickets);

            }
            else if (order.OrderStatus == Core.Models.Enums.OrderStatus.Cancel)
            {
                orderItem = orderItem.ToList();
                var item = orderItem.Select(x => x.ItemId).ToList();
                var tickets = trainingTicketRepo.Table.Where(x => item.Contains(x.Id));
                tickets.ToList().ForEach(x =>
                {
                    x.AvailableQty = x.AvailableQty + orderItem.FirstOrDefault(y => y.ItemId == x.Id).Qty;
                });
                trainingTicketRepo.Update(tickets);

            }
            return order;
        }

        public UserPayment CreateUserPayment(IEnumerable<OrderItem> orderItems, Order order)
        {
            orderItems = orderItems.ToList();

            if (orderItems.Any() && orderItems.Sum(x => x.UnitPrice) > 0)
            {
                var userPaymentList = new List<UserPaymentResult>();
                var ticketPaymentDetailRepo = iConfigManager.GetService<IRepository<TicketPaymentDetail>>();
                var ticketServiceRepo = iConfigManager.GetService<ITicketService>();
                var userRepo = iConfigManager.GetService<IUserRepository>();
                var itemIds = orderItems.Select(x => x.ItemId);
                var ticketDetails = ticketPaymentDetailRepo.Table.Where(x => itemIds.Contains(x.TicketId)).ToList();
                var tickets = trainingTicketRepo.Table.Where(x => itemIds.Contains(x.Id)).ToList();
                orderItems.Where(x => x.UnitPrice > 0).ToList().ForEach(x =>
                    {
                        var ticket = tickets.FirstOrDefault(y => y.Id == x.ItemId);
                        if (ticket != null)
                        {
                            var detail = (ticketServiceRepo.GetTicketPaymentPreview(new TicketDetailData
                            {
                                TotalAmount = ticket.PaymentCharge,
                                PaymentDetails = ticketDetails.Where(y => y.TicketId == x.ItemId)
                                .Select(y => y.ToModel<TicketPaymentDetailModel>())
                            }));
                            x.UnitPrice = detail.BuyerTotal;
                            for (int i = x.Qty; i > 0; i--)
                            {
                                userPaymentList.Add(detail);
                            }
                        }

                    });
                var dat = DateTime.Now.NowDate();
                var user = userRepo.Users.First(x => x.RecId == order.UserId);
                var key = iConfigManager.GetSection<PaymentKeys>();
                return new UserPayment
                {
                    CreateDate = dat,
                    ModifiedDate = dat,
                    CustomerEmail = user.Email,
                    CustomerName = user.FirstName,
                    CustomerPhone = user.PhoneNumber,
                    IsPaid = false,
                    PaymentStatus = Core.Models.Enums.UserPayment_Status.PENDING,
                    GetwayFee = userPaymentList.Sum(x => x.Getwayfee),
                    ServiceFee = userPaymentList.Sum(x => x.ServiceTax),
                    GST = userPaymentList.Sum(x => x.GST),
                    OrderId = order.OrderAppId,
                    PaymentId = Guid.NewGuid(),
                    OrderNote = $"Payment for Order {order.OrderAppId}",
                    OtherFee = userPaymentList.Sum(x => x.Affilation),
                    Status = true,
                    SubTotal = userPaymentList.Sum(x => x.SubTotal),
                    ReturnUrl = key.ReturnUrl,
                    NotifyUrl = key.NotifyUrl,
                    UserId = order.UserId,
                    Total = userPaymentList.Sum(x => x.SubTotal)+ userPaymentList.Sum(x => x.ServiceTax)+ userPaymentList.Sum(x => x.GST)//userPaymentList.Sum(x => x.BuyerTotal),
                };

            }
            else
            {
                return null;
            }
        }

        public override object OrderSummary(string OrderUniqId)
        {
            var order = orderRepo.Table.FirstOrDefault(x => x.OrderAppId == OrderUniqId);
            var OrderItem = orderItemRepo.Table.Where(x => x.OrderId == order.Id);
            return CreateUserPayment(OrderItem, order);
        }
        protected void SendTrainingRegisterEmail(OrderModel orderModel, Order order, IEnumerable<OrderItem> orderItem, long trainingId)
        {
            var trainingRepo = trainingOrderRepo.GetFromSql<Result_Trainings_view>();
            var trainingListService = configManager.GetService<ITrainingListService>();
            var userPaymentRepo = iConfigManager.GetService<IRepository<UserPayment>>().Table;
            var emailService = configManager.GetService<IEmailService>();
            var userRepository = configManager.GetService<IUserRepository>();
            var users = userRepository.Users;
           var emailDataObj = (from  trng in trainingRepo
                          join orgUsr in users
                          on trng.UserId equals orgUsr.RecId
                          join usr in users
                          on order.UserId equals usr.RecId
                          join up  in userPaymentRepo 
                          on order.OrderAppId equals up.OrderId into ups
                               from up in ups.DefaultIfEmpty()

                          where trng.TrainingId == trainingId
                          select
            
          

              new
            {
                  trng,
                  up,
                FirstName = usr.FirstName,
                LastName = usr.LastName,
                AEmail = usr.Email,
                TrainingName = trng.Name,
                OrganizerName = orgUsr.FirstName+ " " + orgUsr.LastName,
                OrganizerContact = orgUsr.PhoneNumber,
                AContact = usr.PhoneNumber,
                MessageToAttendee = string.Empty,
                Email = orgUsr.Email
              }).FirstOrDefault();
            var training = emailDataObj.trng;
            trainingListService.SetTrainingHosted(ref training);
            var dateFormet = $"ddd, MMM d, yyyy | h:mm tt IST";
            var venu = emailDataObj.trng.Mode == "Online" ? emailDataObj.trng.OnlineLocation :
                   $"{emailDataObj.trng.Address1} {emailDataObj.trng.Address2} {emailDataObj.trng.City} {emailDataObj.trng.CountryName}";
            var tickets = string.Empty;
            var msgForAtendee = string.Empty;
            var items = orderItem.Select(x => x.ItemId);
            var ticketList = trainingTicketRepo.Table.Where(x => items.Contains(x.Id));
            ticketList.ToList().ForEach(x =>
            {
                 msgForAtendee = x.MSGForAtendee;
                tickets += emailService.BindProperty(Body.Ticket_TrainingRegister,
                    new { TicketName = x.Name, TicketType = x.PaymentCharge > 0 ? "Paid" : "Free", Price = x.PaymentCharge });
            });
            var emailObj = new
           {
               FirstName = emailDataObj.FirstName,
               LastName = emailDataObj.LastName,
               AEmail = emailDataObj.AEmail,
               Qty = orderItem.Count(),
               IsOnlinePayment = emailDataObj.up == null ? "none":"",
               TrainingName = emailDataObj.trng.Name,
               TrainingUrl = Util.GetUrlWithUrl(Core.Helper.Util.GetTrainingPreview(emailDataObj.trng.Url)),
               OrderId = order.OrderAppId,
               BookingDate = order.CreateDate.ToString("yyyy-MM-dd hh:mm tt") + " IST",
               GrandTotal = emailDataObj.up != null ? $"INR {emailDataObj.up.SubTotal} + INR {emailDataObj.up.ServiceFee}(processing fees) + INR {emailDataObj.up.GST}(GST) = INR {emailDataObj.up.Total}" :string.Empty,
               OrganizerName = training.UserName,
               OrganizerContact = training.UserPhoneNumber,

               TrainingDate =
                $"{emailDataObj.trng.StartDate.ToString(dateFormet)} to {emailDataObj.trng.EndDate.ToString(dateFormet)} ",
               Venue = venu,
               VenueUrl = emailDataObj.trng.Mode == "Online" ? venu : $"https://www.google.com/maps/place/{venu}",
               AContact = emailDataObj.AContact,
               MessageToAttendee = msgForAtendee,
               Email = training.UserEmail
            };
        
            var body = emailService.GetBody(Body.TrainingRegister, emailObj);
          
           
            body = body.Replace(string.Format("{{{0}}}", "TicketList"), tickets);
            emailService.Save(new EmailModel
            {
                ToEmail = emailDataObj.AEmail,
                Body = body,
                BCCEmail= "scaledskills@gmail.com",
                CCEmail = emailDataObj.Email,
                Subject = $"Registration Confirmed : {emailObj.TrainingName}",
                DisplayName = emailDataObj.FirstName,
                EmailStatus = EmailQueue.STATUS_PENDING,
                UserId = order.UserId
            });


        }
    }
}
