﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class OrganizationService : BaseService, IOrganizationService
    {
        public readonly IRepository<Organization> OrganizationRepository;
        public readonly IConfigManager iConfigManager;
        public OrganizationService(IRepository<Organization> _OrganizationRepository,
            IConfigManager _iConfigManager)
        {
            OrganizationRepository = _OrganizationRepository;
            iConfigManager = _iConfigManager;
        }
        public OrganizationModel GetOrganizationById(object id)
        {
            return OrganizationRepository.GetById(id)?.ToModel<OrganizationModel>();
        }
        public OrganizationModel GetByUrl(string url)
        {
            return OrganizationRepository.Table.FirstOrDefault(x => x.ProfileUrl == url)?.ToModel<OrganizationModel>();
        }
        public OrganizationModel GetOrganizationByUserId(long userId)
        {
            var result = OrganizationRepository.Table.Where(x => x.UserId == userId).FirstOrDefault()?.ToModel<OrganizationModel>();
            return result;

        }
        public IEnumerable<OrganizationResult> GetOrganizationList()
        {
            return OrganizationRepository.Table.Where(x => x.Status).Select(x => new OrganizationResult
            {
                Name = x.Name,
                Id = x.Id
            }).ToList();
        }
        public long InsertOrganization(OrganizationModel model)
        {
            var entity = model.ToInsertBindEntity<Organization>();
            if (model.Address != null)
            {
                entity.AddressId = iConfigManager.GetService<IAddressService>().Save(model.Address);
            }

            entity.ProfileUrl = entity.ProfileUrl ?? Guid.NewGuid().ToString().ToLower();
            OrganizationRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateOrganization(OrganizationModel model)
        {
            var dbObj = OrganizationRepository.GetById(model.Id);
            if (model.Address != null)
            {
                model.Address.Id = dbObj.AddressId ?? 0;
                dbObj.AddressId = iConfigManager.GetService<IAddressService>().Save(model.Address);
            }
            var updateObj = model.ToUpdateBindEntity(dbObj);
            OrganizationRepository.Update(updateObj);
            return dbObj.Id;
        }
        public long UpdateAbout(AboutModel model, long id)
        {
            var dbObj = OrganizationRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.AboutId ?? 0;
                dbObj.AboutId = iConfigManager.GetService<IAboutService>().Save(model);
            }
            OrganizationRepository.Update(dbObj);
            return dbObj.Id;
        }
        public long UpdateUserSocial(UserSocialModel model, long id)
        {
            var dbObj = OrganizationRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.SocialId ?? 0;
                dbObj.SocialId = iConfigManager.GetService<IUserSocialService>().Save(model);
            }
            OrganizationRepository.Update(dbObj);
            return dbObj.Id;
        }
        public long UpdateBankDetail(BankDetailModel model, long id)
        {
            var dbObj = OrganizationRepository.GetById(id);
            if (model != null)
            {
                model.Id = dbObj.BankDetailId ?? 0;
                dbObj.BankDetailId = iConfigManager.GetService<IBankDetailService>().Save(model);
            }
            OrganizationRepository.Update(dbObj);
            return dbObj.Id;
        }
        public IEnumerable<SelectItem> GetListByOrganizer(long userId, Training_Hosted training_Hosted)
        {
            var entityRepo = iConfigManager.GetService<IRepository<TrainingOrganization>>().Table;
            var trainingRepo = iConfigManager.GetService<IRepository<Training>>().Table;
            var orgentityRepo = iConfigManager.GetService<IRepository<Organization>>().Table;
            if (training_Hosted == Training_Hosted.Organization)
            {
                var result = (from torg in entityRepo
                              join org in orgentityRepo
                              on torg.OrganizationId equals org.Id
                              join trg in trainingRepo
                             on torg.TrainingId equals trg.Id
                              where org.UserId == userId
                              select new SelectItem
                              {
                                  Text = trg.Name,
                                  Value = trg.Id.ToString(),
                                  IsSelect = false
                              });
                return result.ToList();
            }
            else
            {
                var result = (from trg in trainingRepo
                              where trg.UserId == userId
                              select new SelectItem
                              {
                                  Text = trg.Name,
                                  Value = trg.Id.ToString(),
                                  IsSelect = false
                              });
                return result.ToList();
            }


        }
        public IEnumerable<SelectItem> GetTrainingListByOrganizations(long userId, List<long> organizations)
        {
            var entityRepo = iConfigManager.GetService<IRepository<TrainingOrganization>>().Table;
            var trainingRepo = iConfigManager.GetService<IRepository<Training>>().Table;
            var orgentityRepo = iConfigManager.GetService<IRepository<Organization>>().Table;
            var result = (from torg in entityRepo
                          join org in orgentityRepo
                          on torg.OrganizationId equals org.Id
                          join trg in trainingRepo
                         on torg.TrainingId equals trg.Id
                          where org.UserId == userId &&
                          organizations.Any(x=> x==torg.OrganizationId)
                          select new SelectItem
                          {
                              Text = trg.Name,
                              Value = trg.Id.ToString(),
                              IsSelect = false
                          });
            return result.ToList();


        }
        public IEnumerable<SelectItem> GetUserListByTrainings(List<int> trainings, IEnumerable<Training_Users> users)
        {
            var trainingRepo = iConfigManager.GetService<IRepository<Training>>().Table;
            var trainingOrderRepo = iConfigManager.GetService<IRepository<TrainingOrder>>().Table;
            var userRepo = iConfigManager.GetService<IUserRepository>().Users;
            var result = Enumerable.Empty<SelectItem>();
            if (users.Contains(Training_Users.Registered))
            {
                 result = result.Union( from registerd in trainingOrderRepo
                              join usr in userRepo
                              on registerd.UserId equals usr.RecId
                            where trainings.Any(x=>x == registerd.TrainingId)
                            select new SelectItem
                              {
                                  Text = usr.FirstName + " " + usr.LastName,
                                  Value = usr.Email,
                                  IsSelect = false
                              });
            }
             if (users.Contains(Training_Users.Interested))
            {
                var trainingInterestedRepo = iConfigManager.GetService<IRepository<TrainingInterested>>().Table;
                result = result.Union(from registerd in trainingInterestedRepo
                          join usr in userRepo
                          on registerd.UserId equals usr.RecId
                          where trainings.Any(x => x == registerd.TrainingId)
                          select new SelectItem
                          {
                              Text = usr.FirstName + " " + usr.LastName,
                              Value = usr.Email,
                              IsSelect = false
                          });
            }
            else if (users.Contains(Training_Users.Member))
            {
                var trainingUserFollow = iConfigManager.GetService<IRepository<UserFollow>>().Table;
                result = result.Union(from registerd in trainingUserFollow
                          join usr in userRepo
                          on registerd.UserId equals usr.RecId
                          where trainings.Any(x => x == registerd.TypeId)
                          select new SelectItem
                          {
                              Text = usr.FirstName + " " + usr.LastName,
                              Value = usr.Email,
                              IsSelect = false
                          });
            }
            else if (users.Contains(Training_Users.FeedbackReceived))
            {
                var trainingUserFollow = iConfigManager.GetService<IRepository<TrainingReview>>().Table;
                result = result.Union(from registerd in trainingUserFollow
                                      join usr in userRepo
                                      on registerd.UserId equals usr.RecId
                                      where trainings.Any(x => x == registerd.TrainingId)
                                      select new SelectItem
                                      {
                                          Text = usr.FirstName + " " + usr.LastName,
                                          Value = usr.Email,
                                          IsSelect = false
                                      });
            }
            return result.Distinct();


        }
        public IEnumerable<SelectItem> GetUserOrganizations(long loginId)
        {
            var trainingOrganizationRepo = iConfigManager.GetService<IRepository<TrainingOrganization>>().Table;
            var organizationRepo = iConfigManager.GetService<IRepository<Organization>>().Table;
            var trainingRepo = iConfigManager.GetService<IRepository<Training>>().Table;
            var result = (from tro in trainingOrganizationRepo
            join o in organizationRepo
            on tro.OrganizationId equals o.Id
            join t in trainingRepo
            on tro.TrainingId equals t.Id
            where t.UserId == loginId
            select new SelectItem
            {
                Text = o.Name,
                Value = o.Id.ToString(),
                IsSelect = false
            });
            return result;
        }

        public void SendCommunicationEmail(CommunicationViewModel communicationViewModel)
        {
           var emailService = iConfigManager.GetService<IEmailService>();

            emailService.Save(new EmailModel
            {
                ToEmail = communicationViewModel.Email,
                Body = emailService.MainBody(communicationViewModel.EmailBody),
                BCCEmail = string.Join(";", communicationViewModel.Emails),
                Subject = communicationViewModel.EmailSubject,
                EmailStatus = EmailQueue.STATUS_PENDING,
                UserId = communicationViewModel.UserId.Value
            });
        }
    }
}
