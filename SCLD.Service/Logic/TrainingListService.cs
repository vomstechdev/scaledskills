﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class TrainingListService : BaseService, ITrainingListService
    {
        readonly IRepository<Training> trainings;
        private readonly IConfigManager configManager;
        public TrainingListService(IRepository<Training> _trainings, IConfigManager _configManager)
        {
            configManager = _configManager;
            trainings = _trainings;
        }
        private IEnumerable<Result_CardTraining_view> trainings_view_Repository
        {
            get { return trainings.GetFromSql<Result_CardTraining_view>(); }
        }
        private IEnumerable<Result_Trainings_view> Trainings_view
        {
            get { return trainings.GetFromSql<Result_Trainings_view>(); }
        }
        public IEnumerable<Result_CardTraining_view> GetPastTraining(long? userId = null)
        {
            var list = trainings_view_Repository.Where(x => x.EndDate < DateTime.Now.NowDate());
            if (userId.HasValue)
                list = list.Where(x => x.UserId == userId.Value);
            return list.ToList();
        }
        public SearchPageResult<Result_CardTraining_view> GetPastTraining(SearchModel trainingSearchModel)
        {
            var list = trainings_view_Repository.Where(x => x.EndDate < DateTime.Now.NowDate()).OrderBy(x => x.StartDate);
            return GetSearchPageResult(trainingSearchModel, list);
        }
        private SearchPageResult<Result_CardTraining_view> GetSearchPageResult(SearchModel trainingSearchModel, IEnumerable<Result_CardTraining_view> result)
        {
            var trainingInteres = configManager.GetService<IRepository<TrainingInterested>>().Table;

            if (trainingSearchModel.PageType?.ToUpper() == "O")
            {
                var organization = configManager.GetService<IRepository<TrainingOrganization>>().Table.Where(x => x.OrganizationId == trainingSearchModel.UserId);
                //.Select(x=>x.TrainingId);
                result = (from trn in result
                          join o in organization
                          on trn.TrainingId equals o.TrainingId
                          select trn
                            );
            }
            else if (trainingSearchModel.PageType?.ToUpper() == "TP")
            {
                var trainers = configManager.GetService<IRepository<Trainer>>().Table;
                var trainings = configManager.GetService<IRepository<Training>>().Table;
                var trngTrnrs = configManager.GetService<IRepository<TrainingTrainer>>().Table;
                var trngs =
                    (from trnr in trainers
                     join training in trainings
                     on trnr.UserId equals training.UserId
                     where trnr.Id == trainingSearchModel.UserId
                     select training.Id).
                     Union(
                        from ttrnr in trngTrnrs
                        join trnr in trainers on ttrnr.TrainerId equals trnr.Id
                        join training in trainings
                                       on ttrnr.TrainingId equals training.Id
                        where trnr.Id == trainingSearchModel.UserId && ttrnr.Status && trnr.Status
                        select training.Id);
                result = result.Where(x => trngs.Contains(x.TrainingId));
            }
            else
            {
                if (trainingSearchModel.UserId.HasValue && trainingSearchModel.UserId != 0)
                    result = result.Where(x => x.UserId == trainingSearchModel.UserId.Value);
            }


            if (!string.IsNullOrWhiteSpace(trainingSearchModel.SearchText))
                result = result.Where(x => x.Name.ToLower().Contains(trainingSearchModel.SearchText.ToLower()));
            var recordToSkip = trainingSearchModel.Page * trainingSearchModel.PageSize;
            var traningInterrest = trainingInteres.Where(x => x.UserId == trainingSearchModel.LoginUserId && x.IsInterested)
                .Select(x => x.TrainingId).ToList();
            var resultdata = new SearchPageResult<Result_CardTraining_view>
            {
                Results = result.Skip(recordToSkip).Take(trainingSearchModel.PageSize).ToList()
                .Select(x => { x.IsInterest = traningInterrest.Any(y => y == x.TrainingId); return x; }),
                TotalCount = result.Count()
            };
            return resultdata;

        }
        public SearchPageResult<Result_CardTraining_view> GetRecentTraining(SearchModel model)
        {
            var list = trainings_view_Repository.Where(x =>
            //x.StartDate <= DateTime.Now.NowDate() && x.EndDate >= DateTime.Now.NowDate()).OrderBy(x=>x.StartDate);
            x.TrainingStatus == Core.Models.Enums.Training_Status.Publish &&
            x.EndDate > DateTime.Now.NowDate()).OrderBy(x => x.StartDate);
            return GetSearchPageResult(model, list);
        }
        public SearchPageResult<Result_CardTraining_view> GetRelatedTraining(SearchModel model)
        {
            var trainingRepo = configManager.GetService<IRepository<Training>>().Table;
            var aboutRepo = configManager.GetService<IRepository<About>>().Table;
            var trainingId = model.UserId;
            var keyword = (from t in trainingRepo
                           join a in aboutRepo
                           on t.AboutId equals a.Id
                           where t.Id == trainingId
                           select a.Name).FirstOrDefault();
            var keywords = keyword?.ToLower().Split(",");
            var query = (from t in trainingRepo
                         join a in aboutRepo
                         on t.AboutId equals a.Id
                         join tv in trainings_view_Repository
                         on t.Id equals tv.TrainingId
                         where keywords != null && keyword.Any(x => a.Name.ToLower().Contains(x)) && t.Id != trainingId
                         select tv);
            model.UserId = null;
            model.PageType = string.Empty;
            var list = query.Where(x =>
            //x.StartDate <= DateTime.Now.NowDate() && x.EndDate >= DateTime.Now.NowDate()).OrderBy(x=>x.StartDate);
            x.EndDate > DateTime.Now.NowDate()).OrderBy(x => x.StartDate);
            return GetSearchPageResult(model, list);
        }
        public SearchPageResult<Result_CardTraining_view> GetAttendiesUpCommingTraining(SearchModel model)
        {
            var trainingOrdRepo = configManager.GetService<IRepository<TrainingOrder>>().Table;
            var userid = model.LoginUserId ?? 0;
            var query = (from o in trainingOrdRepo
                         join t in trainings_view_Repository
                         on o.TrainingId equals t.TrainingId
                         where o.UserId == userid
                         select t);
            model.UserId = null;
            model.PageType = string.Empty;
            var list = query.Where(x =>
            x.EndDate > DateTime.Now.NowDate()).OrderBy(x => x.StartDate);
            return GetSearchPageResult(model, list);
        }
        public SearchPageResult<Result_CardTraining_view> GetAttendiesPastTraining(SearchModel model)
        {
            var trainingOrdRepo = configManager.GetService<IRepository<TrainingOrder>>().Table;
            var userid = model.LoginUserId ?? 0;
            var query = (from o in trainingOrdRepo
                         join t in trainings_view_Repository
                         on o.TrainingId equals t.TrainingId
                         where o.UserId == userid
                         select t);
            model.UserId = null;
            model.PageType = string.Empty;

            var list = query.Where(x => x.EndDate < DateTime.Now.NowDate());
            return GetSearchPageResult(model, list);
        }

        public SearchPageResult<Result_CardTraining_view> GetUpCommingTraining(SearchModel model)
        {
            var list = trainings_view_Repository.Where(x =>
            //x.StartDate > DateTime.Now.NowDate()).OrderBy(x => x.StartDate);
            x.EndDate > DateTime.Now.NowDate()).OrderBy(x => x.StartDate);


            return GetSearchPageResult(model, list);
        }
        public HomeHeaderResult<Result_CardTraining_view> GetHomeHeader()
        {
            var promoRepo = configManager.GetService<IRepository<Promotion>>();
            var resultSet = new HomeHeaderResult<Result_CardTraining_view>();
            var currentDate = DateTime.Now.NowDate();
            resultSet.Trainings = from p in promoRepo.Table
                                  join training in trainings_view_Repository on p.EntityId equals training.TrainingId
                                  where p.StartDate <= currentDate && p.EndDate >= currentDate && p.Status &&
                                  p.EntityType == Promotion.EntityType_Training
                                  select training;

            resultSet.TopHeaders = from p in promoRepo.Table
                                   join training in Trainings_view on p.EntityId equals training.TrainingId
                                   where p.StartDate <= currentDate && p.EndDate >= currentDate && p.Status &&
                                   p.EntityType == Promotion.EntityType_Image
                                   select new TopHeader { Image = training.Header, Description = training.Name, Url = training.Url, ImageType = 2 };
            var list = new List<TopHeader>() {
                new TopHeader { Image = "c43c2d66-715f-48c1-b0ff-3cbba68d9bd5", Url = string.Empty, ImageType = 1 }
            };
            list.AddRange(resultSet.TopHeaders);
            resultSet.TopHeaders = list;
            return resultSet;
        }
        public Result_Trainings_view GetTrainingByUrl(string url, long userId)
        {
            var training = trainings.GetFromSql<Result_Trainings_view>().FirstOrDefault(x => x.Url == url);
            if (training != null)
            {
                training.CanEdit = training.UserId == userId;
                training.IsInterest = configManager
                    .GetService<IRepository<TrainingInterested>>()
                    .Table.Any(x => x.UserId == userId && x.IsInterested && x.TrainingId == training.TrainingId);
                var leftTickets = configManager
                     .GetService<IRepository<TrainingTicket>>()
                     .Table.Where(x => x.TrainingId == training.TrainingId)
                     .GroupBy(f => f.TrainingId)
                    .Select(group => new
                    {
                        AvailableQtyTotal = group.Sum(f => f.AvailableQty),
                        QtyTotal = group.Sum(f => f.QTY)
                    }).FirstOrDefault();
                if (leftTickets != null)
                {
                    training.AvilableQtyTotal = leftTickets.AvailableQtyTotal;
                    training.QtyTotal = leftTickets.QtyTotal;
                }

                var orderItem = configManager
                   .GetService<IRepository<OrderItem>>().Table;
                var order = configManager
                   .GetService<IRepository<Order>>().Table;
                var traininerOrder = configManager
                   .GetService<IRepository<TrainingOrder>>().Table;

                var q = from oi in orderItem
                        join o in order
                        on oi.OrderId equals o.Id
                        join to in traininerOrder
                        on oi.OrderId equals to.OrderId
                        where o.OrderStatus == Core.Models.Enums.OrderStatus.Complete
                        select new { to };

                training.IsRegister = q.Any(x => x.to.TrainingId == training.TrainingId && x.to.UserId == userId);
                SetTrainingHosted(ref training);
            }

            return training;
        }
        public void SetTrainingHosted(ref Result_Trainings_view training)
        {
            var userId = training.UserId;
            var trainner = configManager.GetService<IRepository<Trainer>>().Table.Where(x => x.UserId == userId)
                .Select(x => new { x.Id, x.ProfileUrl,x.Email, x.PhoneNumber }).FirstOrDefault();
            if (trainner != null)
            {
                training.IsDisplayFollow = true;
                training.TrainnerId = trainner.Id;
                training.UserUrl = trainner.ProfileUrl;
                training.UserEmail = trainner.Email;
                training.UserPhoneNumber = trainner.PhoneNumber;
            }
            if (training.HostedBy == 2)
            {
                var data = configManager.GetService<ITrainingService>().GetOrganizationList(training.TrainingId);
                if (data.Any())
                {
                    training.UserName = data.FirstOrDefault()?.Name;
                    training.UserId = data.FirstOrDefault().Id;
                    training.UserUrl = data.FirstOrDefault().URL;
                    training.UserEmail = data.FirstOrDefault().Email;
                    training.UserPhoneNumber = data.FirstOrDefault().PhoneNumber;
                }
            }
        }
        public IEnumerable<TrainingTicketModel> GetTrainingTicketsByUrl(string url)
        {
            var trainingService = configManager.GetService<ITrainingService>();
            var trainingId = trainings.Table.Where(x => x.Url.ToUpper() == url.ToUpper()).Select(x => x.Id).FirstOrDefault();
            var tickets = trainingService.GetTrainingTicketByTicketId(trainingId);

            return tickets;
        }
        public ReviewSearchPageResult GetTrainingReview(SearchModel model)
        {
            var trainingReviews = configManager.GetService<IRepository<TrainingReview>>().Table;
            if (model.PageType == SearchBase.PageType_Training)
            {
                trainingReviews = trainingReviews.Where(x => x.TrainingId == model.UserId);
            }
            else if (model.PageType == SearchBase.PageType_Trainer)
            {
                var trainers = configManager.GetService<IRepository<Trainer>>().Table;
                var trainings = configManager.GetService<IRepository<Training>>().Table;
                var trngTrnrs = configManager.GetService<IRepository<TrainingTrainer>>().Table;
                var trngs =
                    (from trnr in trainers
                     join training in trainings
                     on trnr.UserId equals training.UserId
                     where trnr.Status && training.Status && trnr.Id == model.UserId
                     select training).
                     Union(
                        from ttrnr in trngTrnrs
                        join trnr in trainers on ttrnr.TrainerId equals trnr.Id
                        join training in trainings
                                       on ttrnr.TrainingId equals training.Id
                        where trnr.Id == model.UserId && ttrnr.Status && trnr.Status && training.Status
                        select training);

                trainingReviews = from review in trainingReviews
                                  join trn in trngs on review.TrainingId equals trn.Id
                                  select review;

            }
            else if (model.PageType == SearchBase.PageType_Organization)
            {
                var trainings = configManager.GetService<IRepository<Training>>().Table;
                var trngOrgs = configManager.GetService<IRepository<TrainingOrganization>>().Table;
                var trngs = from org in trngOrgs
                            join training in trainings
                            on org.TrainingId equals training.Id
                            where org.Status && training.Status && org.OrganizationId == model.UserId
                            select training;
                trainingReviews = from review in trainingReviews
                                  join trn in trngs on review.TrainingId equals trn.Id
                                  select review;

            }
            var userrepo = configManager.GetService<IUserRepository>().Users;
            var query = (from tr in trainingReviews
                         join usr in userrepo
                         on tr.UserId equals usr.RecId
                         where tr.Status
                         select new
                         {
                             tr,
                             usr.FirstName,
                             usr.LastName
                         });

            var recordToSkip = model.Page * model.PageSize;
            var resultdata = new ReviewSearchPageResult
            {
                Results = query.Skip(recordToSkip).Take(model.PageSize).ToList().Select(x =>
                {
                    var data = x.tr.ToModel<TrainingReviewModel>();
                    data.FirstName = x.FirstName;
                    data.LastName = x.LastName;
                    return data;
                }),
                AvgRating = Math.Round(query.DefaultIfEmpty().Average(x => x.tr.RateOfSatisfied), 1),
                TotalCount = query.Count()
            };

            return resultdata;
        }
    }
}
