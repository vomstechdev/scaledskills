﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.Enums;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    public class UserHomeService : IHomeService
    {
        private readonly IConfigManager iConfigManager;

        public UserHomeService(IConfigManager _iConfigManager)
        {
            iConfigManager = _iConfigManager;
        }
        public void UserFollow(UserFollowModel model)
        {
            var usrFollowrepo = iConfigManager.GetService<IRepository<UserFollow>>();
            if (model.Type == UserFollowType.Training)
            {
                var trainingrepo = iConfigManager.GetService<IRepository<Training>>().Table;
                var trainerepo = iConfigManager.GetService<IRepository<Trainer>>().Table;
                var orgepo = iConfigManager.GetService<IRepository<TrainingOrganization>>().Table;

                var obj = (from trng in trainingrepo
                           join tnr in trainerepo on trng.UserId equals tnr.UserId into tnrg
                           from tnr in tnrg.DefaultIfEmpty()
                           join Org in orgepo on trng.Id equals Org.TrainingId into orgg
                           from Org in orgg.DefaultIfEmpty()
                           where trng.Id == model.TypeId
                           select new { trng.UserId, typeId = trng.HostedBy ==1 ? tnr.Id : Org.OrganizationId, trng.HostedBy }).FirstOrDefault();
                if (obj != null)
                {
                    if (obj.HostedBy == 1)
                    {
                        model.TypeId = obj.typeId;
                        model.Type = UserFollowType.Trainer;
                    }
                    else
                    {
                        model.TypeId = obj.typeId;
                        model.Type = UserFollowType.Organization;
                    }
                    
                }
                else
                { return; }
            }
            if (!usrFollowrepo.Table.Any(x => x.Type == model.Type && x.TypeId == model.TypeId && x.UserId == model.UserId))
            {
                var entity = model.ToInsertBindEntity<UserFollow>();
                usrFollowrepo.Insert(entity);
            }

        }
        public SearchPageResult<UserFollowUserModel> UserMemberFollowList(FollowSearchModel model)
        {
            var usrFollowrepo = iConfigManager.GetService<IRepository<UserFollow>>().Table;
            var userrepo = iConfigManager.GetService<IUserRepository>().Users;
            var result = from uf in usrFollowrepo
                         join usr in userrepo
                         on uf.UserId equals usr.RecId
                         where uf.Type == model.Type && uf.TypeId == model.Id
                         select new { uf, usr };
            var recordToSkip = model.Page * model.PageSize;
            var resultdata = new SearchPageResult<UserFollowUserModel>
            {
                Results = ((recordToSkip > 0) ? result.Skip(recordToSkip) : result).Take(model.PageSize).Select(x => new UserFollowUserModel
                {
                    Id = x.uf.Id,
                    TypeId = x.uf.TypeId,
                    User = new UserModel()
                    {
                        FirstName = x.usr.FirstName,
                        Image = x.usr.Image,
                        LastName = x.usr.LastName,
                        Email = x.usr.Email,
                        Gender = x.usr.Gender
                    }
                }).ToList(),
                TotalCount = result.Count()
            };
            return resultdata;
        }

        public SearchPageResult<UserModel> RegisterTrainingUsersList(TrainingUserSearchModel model)
        {
            var userTraining = iConfigManager.GetService<IRepository<TrainingOrder>>().Table;
            var userrepo = iConfigManager.GetService<IUserRepository>().Users;
            var result = from uf in userTraining
                         join usr in userrepo
                         on uf.UserId equals usr.RecId
                         where uf.TrainingId == model.TrainingId
                         select new { uf, usr };
            result = result.GroupBy(x => x.usr.Email).Select(x => x.First());
            var recordToSkip = model.Page * model.PageSize;
            var resultdata = new SearchPageResult<UserModel>
            {
                Results = ((recordToSkip > 0) ? result.Skip(recordToSkip) : result).Take(model.PageSize).Select(x =>
                new UserModel
                {

                    FirstName = x.usr.FirstName,
                    Image = x.usr.Image,
                    LastName = x.usr.LastName,
                    Email = x.usr.Email,
                    Gender = x.usr.Gender

                }).ToList(),
                TotalCount = result.Count()
            };
            return resultdata;
        }
        public SearchPageResult<TrainerUserModel> RegisterTrainerUsersList(TrainingUserSearchModel model)
        {
            var userTraining = iConfigManager.GetService<IRepository<TrainingTrainer>>().Table;
            var userTrainer = iConfigManager.GetService<IRepository<Trainer>>().Table;
            var userrepo = iConfigManager.GetService<IUserRepository>().Users;
            var result = from trainer in userTrainer
                         join usr in userrepo
                         on trainer.UserId equals usr.RecId
                         select new { usr, trainer.ProfileUrl, TrainerId = trainer.Id };
            if (model.PageType == "P")
            {
                result = from r in result
                         join usrtr in userTraining
                         on r.TrainerId equals usrtr.TrainerId
                         where !usrtr.IsDelete && usrtr.TrainingId == model.TrainingId
                         select r;
            }

            var recordToSkip = model.Page * model.PageSize;
            var resultdata = new SearchPageResult<TrainerUserModel>
            {
                Results = ((recordToSkip > 0) ? result.Skip(recordToSkip) : result).Take(model.PageSize).Select(x =>
                new TrainerUserModel
                {
                    Id = x.TrainerId,
                    ProfileUrl = x.ProfileUrl,
                    FirstName = x.usr.FirstName,
                    Image = x.usr.Image,
                    LastName = x.usr.LastName,
                }).ToList(),
                TotalCount = result.Count()
            };
            return resultdata;
        }
        public bool IsFollow(UserFollowModel model)
        {
            var usrFollowrepo = iConfigManager.GetService<IRepository<UserFollow>>();
            return usrFollowrepo.Table.Any(x => x.Type == model.Type && x.TypeId == model.TypeId && x.UserId == model.UserId);
        }
    }
}
