﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class TrainingSettingService : BaseService, ITrainingSettingService
    {
        public readonly IRepository<TrainingSetting> TrainingSettingRepository;
        public TrainingSettingService(IRepository<TrainingSetting> _TrainingSettingRepository)
        {
            TrainingSettingRepository = _TrainingSettingRepository;
        }
        public TrainingSettingModel GetTrainingSettingById(object id)
        {
            return TrainingSettingRepository.GetById(id)?.ToModel<TrainingSettingModel>();
        }
        public IEnumerable<TrainingSetting> GetTrainingSettingByTrainingId(long id)
        {
            return TrainingSettingRepository.Table.Where(x => x.TrainingId == id);
        }
        public long InsertTrainingSetting(TrainingSettingModel model)
        {
            var entity = model.ToInsertBindEntity<TrainingSetting>();
            TrainingSettingRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateTrainingSetting(TrainingSettingModel model)
        {
            var dbObj = TrainingSettingRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            TrainingSettingRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
