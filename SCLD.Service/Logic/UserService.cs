﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCLD.Service.Logic
{
    class UserService : IUserService
    {
        readonly IUserRepository userRepository;
        readonly IAddressService addressService;
        readonly IUserSettingService userSettingService;
        readonly IRepository<UserSetting> userSettingRepository;
        readonly IEmailService emailService;
        readonly IRepository<Address> addressRepository;
        IRepository<UserLanguage> userLanguageRepository;
        readonly IRepository<MasterData> masterDataRepository;
        readonly ITrainerService trainerService;
        readonly IConfigManager iConfigManager;

        public UserService(IUserRepository _userRepository
            , IAddressService _addressService
            , IUserSettingService _userSettingService,
            IRepository<UserSetting> _userSettingRepository,
            IRepository<Address> _addressRepository,
            IEmailService _emailService,
            IRepository<UserLanguage> _userLanguageRepository,
             IRepository<MasterData> _masterDataRepository,
             ITrainerService _trainerService,
             IConfigManager _iConfigManager
        )
        {
            userRepository = _userRepository;
            addressService = _addressService;
            userSettingService = _userSettingService;
            userSettingRepository = _userSettingRepository;
            addressRepository = _addressRepository;
            emailService = _emailService;
            userLanguageRepository = _userLanguageRepository;
            masterDataRepository = _masterDataRepository;
            trainerService = _trainerService;
            iConfigManager = _iConfigManager;
        }
        public Task<User> GetUserByPasswordAsync(string userName, string password)
        {
            return userRepository.GetUserByPasswordAsync(userName, password);
        }

        public Task<bool> Create(RegisterUserRequest model)
        {
            return userRepository.CreateAsync(model);
        }
        private bool CreateDefaultSettingForTrainer(RegisterUserRequest model)
        {

            userLanguageRepository.Insert(new UserLanguage { LanguageId = 1, Proficiency = 1, Status = true, UserId = model.UserId });
            userSettingRepository.Insert(new UserSetting
            {
                UserId = model.UserId,
                IsDefaultLanguage = true,
                ProfileUrl = Core.Helper.Util.GetReferralId(model.Email),
                ReferralID = Core.Helper.Util.GetReferralId(model.FirstName)
            });
            return true;
        }
        public async Task<bool> CreateAsTrainner(RegisterUserRequest model)
        {
            if (await userRepository.CreateAsync(model))
            {
                await SendConfirmEmailAsync(model.Email);
                CreateDefaultSettingForTrainer(model);
            }
            return true;
        }

        public async Task<UserModel> GetUserById(object id)
        {
            var user = await userRepository.GetByUserId(id is string ? id as string : string.Empty);
            if (id is long)
            {
                user = userRepository.Users.FirstOrDefault(x => x.RecId == (long)id);
            }
            var model = user.ToModel<UserModel>();
            var userSetting = (from userSet in userSettingRepository.Table
                               join userAddress in addressRepository.Table
                               on userSet.UserId equals userAddress.UserId
                               into userAddresss
                               from userAddress in userAddresss.DefaultIfEmpty()

                               join userCountry in masterDataRepository.Table
                               on userAddress.CountryId equals (int)userCountry.Id
                               into userCountryss
                               from userCountrys in userCountryss.DefaultIfEmpty()

                               join userState in masterDataRepository.Table
                               on userAddress.StateId equals (int)userState.Id
                               into userStatess
                               from userStates in userStatess.DefaultIfEmpty()
                               where userSet.UserId == user.RecId
                               select new { userAddress, userSet, CountryName = userCountrys.Name, StateName = userStates.Name }).FirstOrDefault();
            model.Address = userSetting?.userAddress?.ToModel<AddressModel>();
            if (model.Address != null)
            {
                model.Address.CountryObj = new Core.Models.SelectModelItem
                {
                    IsSelect = true,
                    Text = userSetting.CountryName,
                    Value = model.Address.CountryId.ToString()
                };
                model.Address.StateObj = new Core.Models.SelectModelItem
                {
                    IsSelect = true,
                    Text = userSetting.StateName,
                    Value = model.Address.StateId.ToString()
                };
            }
            model.IsInterAffiliatePartner = userSetting?.userSet?.IsInterAffiliatePartner ?? false;
            model.ReferralID = userSetting?.userSet?.ReferralID;
            return model;
        }

        public Task<bool> Update(UserModel model)
        {
            return userRepository.UpdateAsync(model);
        }
        public Task<bool> UpdateWithAttributeDetails(UserModel model)
        {
            model.Address.UserId = model.UserId;
            addressService.SaveUserAddress(model.Address);
            userSettingService.UpdateInterAffiliatePartner(model.UserId, model.IsInterAffiliatePartner, model.ReferralID);
            return Update(model);
        }
        public Task<bool> ChangePassword(string userId, string currentPassword, string newPassword)
        {
            return userRepository.ChangePassword(userId, currentPassword, newPassword);
        }
        public Task<string> GeneratePasswordResetTokenByEmailAsync(string email)
        {
            return userRepository.GeneratePasswordResetTokenAsync(email, (user, code) =>
            {
                emailService.Save(new EmailModel
                {
                    ToEmail = email,
                    Body = emailService.GetBody(Body.ForgotPassword, new { Email = email, CallbackUrl = string.Format("{0}?code={1}&usr={2}", Core.Helper.Util.UserResetPasswordUrl, Core.Helper.Util.Base64Encode(code), user.Id) }),
                    Subject = "Reset Password",
                    DisplayName = "Test",
                    EmailStatus = EmailQueue.STATUS_PENDING,
                    UserId = user.RecId
                });
            });
        }
        public Task SendConfirmEmailAsync(string email)
        {
            return userRepository.GenerateEmailConfirmTokenAsync(email, (user, code) =>
           {
               var emailObj = new EmailModel
               {
                   ToEmail = email,
                   Body = emailService.GetBody(Body.ConfirmEmail, new { CallbackUrl = string.Format("{0}?code={1}&userId={2}", Core.Helper.Util.UserEmailConfirmUrl, Core.Helper.Util.Base64Encode(code), user.Id) }),
                   Subject = "Confirm Email",
                   DisplayName = user.FirstName,
                   EmailStatus = EmailQueue.STATUS_PENDING,
                   UserId = user.RecId
               };
               emailService.Save(emailObj);
           });
        }
        public Task<bool> ResetPassword(string userId, string code, string newPassword)
        {
            if (!string.IsNullOrEmpty(code))
            {
                code = Core.Helper.Util.Base64Decode(code);
            }
            return userRepository.ResetPassword(userId, code, newPassword);
        }
        public Task<bool> ConfirmEmail(string userId, string code)
        {
            return userRepository.ConfirmEmail(userId, code);
        }

        public Task<bool> UnsubscribeUser(string id)
        {
            return userRepository.UnsubscribeUser(id);
        }

        public Task<bool> SendMailToSubscriber(TrainingModel trainingModel, TrainingImageModel trainingImage)
        {
            try
            {
                var userList = userRepository.GetAllSubscribers();
                var trainer = trainerService.GetTrainerById(trainingModel.TrainerId);
                var keys = iConfigManager.GetSection<AppKeys>();
                var trainingAddress = addressService.GetAddressById(trainingModel.AddressId);
                var user = GetUserById(trainer.UserId).Result;
                if (user != null)
                {
                    user.Image = Core.Helper.Util.GetDocPreview(user.Image, keys.ApiUrl);
                }
                user.Address = trainingAddress;
                if (trainingImage.Header != null)
                {
                    trainingImage.HeaderUrl = Core.Helper.Util.GetDocPreview(trainingImage.Header, keys.ApiUrl);
                }
                else
                {
                    trainingImage.HeaderUrl = "" + keys.ApiUrl + "api/Document/p/c43c2d66-715f-48c1-b0ff-3cbba68d9bd5";
                }

                foreach (var item in userList)
                {
                    emailService.Save(new EmailModel
                    {
                        ToEmail = item.Email,
                        Body = GetMailBody(Body.TrainingNotification, trainingModel, trainer, trainingImage, user, item.Id),
                        Subject = (trainingModel.HostedBy == 1 ? "Individual " : "Organisation ") + ": " + trainingModel.Name
                        + " at " + string.Format("{0:ddd, MMM dd, yyyy | hh:mm tt}", trainingModel.StartDate)+" IST",
                        EmailStatus = EmailQueue.STATUS_PENDING,
                        UserId = trainingModel.UserId
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Task.FromResult(true);
        }
        public string GetMailBody(Body body, TrainingModel training, TrainerModel trainer, TrainingImageModel trainingImage, UserModel user, string unsubscriberId)
        {
            var keys = iConfigManager.GetSection<AppKeys>();
            string mailBody = string.Empty;
            string html1 = string.Empty;
            var fullPath = string.Format(@"{0}\Template\Email\{1}.html", System.IO.Directory.GetCurrentDirectory(), body.ToString());
            //var fullPath = string.Format(@"F:\Mohit\SCLD\SCLD\SCLD.Api\Template\Email\{0}.html", body.ToString());
            mailBody = System.IO.File.ReadAllText(fullPath);
            mailBody = mailBody.Replace("{TrainingName}", training.Name);
            mailBody = mailBody.Replace("{Description}", training.Description);
            mailBody = mailBody.Replace("{TrainingStartDate}", string.Format("{0:ddd, MMM dd, yyyy | hh:mm tt}", training.StartDate));
            mailBody = mailBody.Replace("{TrainingEndDate}", string.Format("{0:ddd, MMM dd, yyyy | hh:mm tt}", training.EndDate));
            mailBody = mailBody.Replace("{HeaderUrl}", trainingImage.HeaderUrl);
            mailBody = mailBody.Replace("{OrganiserProfileUrl}", user.Image);
            mailBody = mailBody.Replace("{OrganiserName}", user.FirstName + " " + user.LastName);
            mailBody = mailBody.Replace("{TrainingUrl}", "" + keys.ApiUrl + "t/" + training.Url + "?refCode=EMail");
            mailBody = mailBody.Replace("{UnsubscibeCallbackUrl}", Core.Helper.Util.UserUnsubscibeUrl + "?Id=" + unsubscriberId);
            if (user.Address != null)
            {
                html1 = "<p style='margin:5px 0; color:#666; font-size: 14px; text-align: left;'><strong style='color: #000;'> Classroom </strong></p><p style = 'margin:5px 0; color:#666; font-size: 14px; text-align: left;'>" + user.Address.Address1 + "," + user.Address.Address2 + "," + user.Address.Address3 + "," + user.Address.City + "," + user.Address.StateObj + "</p>";
            }
            string html2 = " <p style='margin:5px 0; color:#666; font-size: 14px; text-align: left;'><strong style='color: #000;'>Online</strong></p><p style='margin:5px 0; color:#666; font-size: 14px; text-align: left;'><a style='text-decoration: none; color: #5c74f1; font-size: 14px; cursor: pointer; border-bottom: 1px #5c74f1 solid;' href='" + keys.ApiUrl + "t/" + training.Url + "?refCode=EMail'>Click here </a> for details</p>";
            mailBody = mailBody.Replace("{TrainingType}", training.ModeType == 1 ? html1 : html2);

            return mailBody;
        }
    }

}
