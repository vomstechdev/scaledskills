﻿using SCLD.Core.Infrastructure;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCLD.Service.Logic
{
    internal class PurchasableService : Purchasable, IServicePurchasable
    {
        public PurchasableService(IConfigManager configManager) : base(configManager)
        {

        }

        void IServicePurchasable.CancleAllPendingOrder()
        {
            var date = DateTime.Now.NowDate().AddHours(-1);
            var repo = iConfigManager.GetService<Core.Data.IRepository<Core.Models.DataModels.Order>>();
            
            var orderList = repo.Table.Where(x => !(x.OrderStatus == Core.Models.Enums.OrderStatus.Complete || x.OrderStatus == Core.Models.Enums.OrderStatus.Cancel)
            && x.CreateDate < date).Select(x=>new { x.OrderAppId, x.Reference }) .ToList();
            orderList.ForEach(x =>
            {
                this.CancleOrder(x.OrderAppId, this.GetByType(x.Reference));
            });

        }
    }
}
