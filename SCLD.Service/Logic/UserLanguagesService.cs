﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class UserLanguagesService : BaseService, IUserLanguagesService
    {
        public readonly IRepository<UserLanguage> UserLanguagesRepository;
        public UserLanguagesService(IRepository<UserLanguage> _UserLanguagesRepository)
        {
            UserLanguagesRepository = _UserLanguagesRepository;
        }
        public UserLanguageModel GetUserLanguagesById(object id)
        {
            return UserLanguagesRepository.GetById(id)?.ToModel<UserLanguageModel>();
        }
        public IEnumerable<UserLanguage> GetUserLanguagesByUserId(long id)
        {
            return UserLanguagesRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertUserLanguages(UserLanguageModel model)
        {
            var entity = model.ToInsertBindEntity<UserLanguage>();
            UserLanguagesRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateUserLanguages(UserLanguageModel model)
        {
            var dbObj = UserLanguagesRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            UserLanguagesRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
