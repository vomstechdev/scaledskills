﻿using SCLD.Core.Data;
using SCLD.Core.Infrastructure;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
	class UserSettingService : BaseService, IUserSettingService
	{
		public readonly IRepository<UserSetting> UserSettingRepository;
		public readonly IConfigManager configManager;
		public UserSettingService(IRepository<UserSetting> _UserSettingRepository,
			IConfigManager _configManager
			)
		{
			UserSettingRepository = _UserSettingRepository;
			configManager = _configManager;
		}
		public UserSettingModel GetUserSettingById(object id)
		{
			return UserSettingRepository.GetById(id)?.ToModel<UserSettingModel>();
		}
		public UserSettingModel GetUserSettingByUserId(long id)
		{
			var instance = UserSettingRepository.Table.Where(x => x.UserId == id).FirstOrDefault();
			var model = instance?.ToModel<UserSettingModel>();
			if (configManager != null)
			{
				var keys = configManager.GetSection<AppKeys>();
				model.PublicUrl = keys.WebUrl;
			}
			return model;
		}
		public long InsertUserSetting(UserSettingModel model)
		{
			var entity = model.ToInsertBindEntity<UserSetting>();
			UserSettingRepository.Insert(entity);
			return entity.Id;
		}
		public long UpdateUserSetting(UserSettingModel model)
		{
			var dbObj = UserSettingRepository.GetById(model.Id);
			var updateObj = model.ToUpdateBindEntity(dbObj);
			UserSettingRepository.Update(dbObj);
			return dbObj.Id;
		}
		public long UpdateProfileUserSetting(UserProfileUserSetting model)
		{
			var dbObj = UserSettingRepository.GetById(model.Id);
			var updateObj = model.ToUpdateBindEntity(dbObj);
			UserSettingRepository.Update(dbObj);
			return dbObj.Id;
		}
		public long UpdateInterAffiliatePartner(long userId, bool isInterAffiliatePartner, string referralID)
		{
			var dbObj = UserSettingRepository.Table.FirstOrDefault(x => x.UserId == userId);
			long id = 0;
			if (dbObj == null)
			{
				id = InsertUserSetting(
					new UserSettingModel
					{
						UserId = userId,
						IsInterAffiliatePartner = isInterAffiliatePartner,
						ReferralID = referralID
					});
			}
			else
			{
				dbObj.IsInterAffiliatePartner = isInterAffiliatePartner;
				dbObj.ModifiedDate = DateTime.Now.NowDate();
				dbObj.ReferralID = string.IsNullOrWhiteSpace(dbObj.ReferralID) ? dbObj.ReferralID: referralID;
				UserSettingRepository.Update(dbObj);
				id = dbObj.Id;
			}
			return id;
		}

	}
}
