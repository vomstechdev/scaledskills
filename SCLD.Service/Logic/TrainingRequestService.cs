﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
	class TrainingRequestService : BaseService, ITrainingRequestService
	{
		public readonly IRepository<TrainingRequest> trainerRequestRepository;
		public TrainingRequestService(IRepository<TrainingRequest> _trainerRequestRepository)
		{
			trainerRequestRepository = _trainerRequestRepository;
		}
		public TrainingRequestModel GetTrainingRequestById(object id)
		{
			return trainerRequestRepository.GetById(id)?.ToModel<TrainingRequestModel>();
		}

		public IEnumerable<TrainingRequest> GetTrainingRequestByUserId(long id)
		{
			return trainerRequestRepository.Table.Where(x => x.UserId == id);
		}

		public long InsertTrainingRequest(TrainingRequestModel model)
		{
			var entity = model.ToInsertBindEntity<TrainingRequest>();
			trainerRequestRepository.Insert(entity);
			return entity.Id;
		}

		public long UpdateTrainingRequest(TrainingRequestModel model)
		{
			var dbObj = trainerRequestRepository.GetById(model.Id);
			var updateObj = model.ToUpdateBindEntity(dbObj);
			trainerRequestRepository.Update(dbObj);
			return dbObj.Id;
		}
	}
}
