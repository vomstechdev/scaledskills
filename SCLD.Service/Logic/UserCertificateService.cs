﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class UserCertificateService : BaseService, IUserCertificateService
    {
        public readonly IRepository<UserCertificate> UserCertificateRepository;
        public UserCertificateService(IRepository<UserCertificate> _UserCertificateRepository)
        {
            UserCertificateRepository = _UserCertificateRepository;
        }
        public UserCertificateModel GetUserCertificateById(object id)
        {
            return UserCertificateRepository.GetById(id)?.ToModel<UserCertificateModel>();
        }
        public IEnumerable<UserCertificate> GetUserCertificateByUserId(long id)
        {
            return UserCertificateRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertUserCertificate(UserCertificateModel model)
        {
            var entity = model.ToInsertBindEntity<UserCertificate>();
            UserCertificateRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateUserCertificate(UserCertificateModel model)
        {
            var dbObj = UserCertificateRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            UserCertificateRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
