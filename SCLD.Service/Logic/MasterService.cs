﻿using SCLD.Core.Data;
using SCLD.Core.Models;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
	 class MasterService : BaseService, IMasterService
	{
		public readonly IRepository<MasterData> MasterRepository;
        public readonly IEmailService EmailService;
        IRepository<Contact> contactRepository;
        public MasterService(IRepository<MasterData> _MasterRepository,
            IEmailService _emailService,
            IRepository<Contact> _contactRepository
            )
		{
			MasterRepository = _MasterRepository;
            EmailService =  _emailService;
            contactRepository = _contactRepository;
        }
		public IEnumerable<SelectModelItem> GetMasterData(string key)
		{
			var result = MasterRepository.Table.Where(x => x.Status && x.Key == key)
				.AsEnumerable().Select(x => x.ToModel<SelectModelItem>());
			return result;
		}
        public IEnumerable<SelectItem> GetMasterSettingsKey(string key)
        {
            var data = MasterRepository.GetFromSql<Core.Models.SelectItem>($"Exec GetSettingItem '{key}'").ToList();
            return data;
        }
        public IEnumerable<MultiSelectItems> GetMasterData(params string[] keys)
		{
			var result = MasterRepository.Table.Where(x => x.Status && keys.Any(z=>z.Equals(x.Key)))
				.AsEnumerable().Select(x =>new { x.Key, item = x.ToModel<SelectModelItem>() });
			return result.GroupBy(x => x.Key).Select((x) => new MultiSelectItems {Key = x.Key, Items = x.Select(y=>y.item)});
		}
		public IEnumerable<SelectModelItem> GetMasterData(string key, string preSelect)
		{
			var result = GetMasterData(key);
			if (string.IsNullOrWhiteSpace(preSelect))
			{
				var data = new List<SelectModelItem>() { new SelectModelItem { Text = preSelect, Value = AppKeys.defaultSelectValue } };
				data.AddRange(result);
				result = data;
			}
			return result;
		}
		public IEnumerable<SelectModelItem> GetMasterData(string key, long? parentId)
		{
			var result = MasterRepository.Table.Where(x => x.Status && x.Key == key && (parentId.HasValue && x.ParentId == parentId))
			.AsEnumerable().Select(x => x.ToModel<SelectModelItem>());
			return result;
		}
		public IEnumerable<SelectModelItem> GetMasterData(string key, long? parentId, string preSelect)
		{

			var result = GetMasterData(key, parentId);
			if (string.IsNullOrWhiteSpace(preSelect))
			{
				var data = new List<SelectModelItem>() { new SelectModelItem { Text = preSelect, Value = AppKeys.defaultSelectValue } };
				data.AddRange(result);
				result = data;
			}
			return result;
		}
        public void SetEmail(Core.Models.ViewModels.EmailModel emailModel)
        {
            EmailService.Save(emailModel);
        }
        public void AddContact(Core.Models.ViewModels.ContactModel contactModel)
        {
            var entity = contactModel.ToEntity<Contact>();
            entity.ContactStatus = Core.Models.Enums.ContactStatus.Open;
            contactRepository.Insert(contactModel.ToEntity<Contact>());
        }
    }
}
