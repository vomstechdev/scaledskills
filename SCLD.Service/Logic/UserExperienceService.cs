﻿using SCLD.Core.Data;
using SCLD.Core.Models.DataModels;
using SCLD.Core.Models.ViewModels;
using SCLD.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCLD.Service.Logic
{
    class UserExperienceService : BaseService, IUserExperienceService
    {
        public readonly IRepository<UserExperience> UserExperienceRepository;
        public UserExperienceService(IRepository<UserExperience> _UserExperienceRepository)
        {
            UserExperienceRepository = _UserExperienceRepository;
        }
        public UserExperienceModel GetUserExperienceById(object id)
        {
            return UserExperienceRepository.GetById(id)?.ToModel<UserExperienceModel>();
        }
        public IEnumerable<UserExperience> GetUserExperienceByUserId(long id)
        {
            return UserExperienceRepository.Table.Where(x => x.UserId == id);
        }
        public long InsertUserExperience(UserExperienceModel model)
        {
            var entity = model.ToInsertBindEntity<UserExperience>();
            UserExperienceRepository.Insert(entity);
            return entity.Id;
        }
        public long UpdateUserExperience(UserExperienceModel model)
        {
            var dbObj = UserExperienceRepository.GetById(model.Id);
            var updateObj = model.ToUpdateBindEntity(dbObj);
            UserExperienceRepository.Update(dbObj);
            return dbObj.Id;
        }
    }
}
